/*********************************************************************
 *
 * File:     SignalHandling.c
 *
 * Project:  Hexapito (Hexapode control program)
 *
 * Description: Code to implement Signal management under linux
 *
 * Author(s):  Paolo Mangiagalli
 *
 * Original: February 26 1998
 *
 *
 *
 * (c) 1998 by European Synchrotron Radiation Facility,
 *                     Grenoble, France
 *
 * Version: $Revision: 2.3 $
 * by:      $Author: perez $
 * date:    $Date: 2012/09/24 09:22:53 $
 *
 *          $Log: SignalHandling.c,v $
 *          Revision 2.3  2012/09/24 09:22:53  perez
 *          Add support of MIDPOINT reference for ICEPAP
 *
 *          Revision 2.2  2011/05/23 07:59:15  perez
 *          Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 *          Revision 2.1  2011/03/28 09:04:33  perez
 *          Add ICEPAPvelocity resource
 *
 *          Revision 2.0  2010/09/06 07:44:12  perez
 *          Add ICEPAP motor support
 *
 *          Revision 1.37  2010/06/22 13:04:50  perez
 *          Fix bug of ch numbering + high debug
 *
 *          Revision 1.36  2010/06/17 06:53:01  perez
 *          Better fix bug of channels not as first six ones
 *
 *          Revision 1.35  2009/06/18 11:51:22  perez
 *          Fix bug of channels not as first six ones
 *
 *          Revision 1.34  2009/05/25 12:37:36  perez
 *          Fix bug in VPAP retries to avoid slowing down DS
 *
 *          Revision 1.31  2008/01/17 10:17:25  rey
 *          Fix bug with usleep
 *
 *          Revision 1.30  2006/05/22 14:50:43  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.29  2006/05/22 14:47:26  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.28  2006/05/22 14:41:40  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.27  2006/05/22 14:39:50  rey
 *          Bug when killing hexapode (hexapito not dying) solved
 *
 *          Revision 1.23  2006/03/16 14:16:04  rey
 *          Bug with CheckAll return missing solved
 *
 * Revision 1.22  2004/03/09  16:29:30  16:29:30  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:34  16:36:34  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 *          Revision 1.0  1999/05/12 06:47:48  dserver
 *          Locked by PM
 *
 *********************************************************************/

#include <SignalHandling.h>


/*****************************************************************************
 *  Function: s_maskall      
 *
 *  Description:  mask all signals
 *
 *         In:    -
 *
 *         Out:   -
 *  Error code(s): 
 *
 *****************************************************************************/

void s_maskall(){
  sigset_t set;

  sigfillset(&set);
  sigprocmask(SIG_BLOCK,&set,NULL);
}





/*****************************************************************************
 *  Function: s_unmaskall      
 *
 *  Description:  unmask all signals
 *
 *         In:    -
 *
 *         Out:   -
 *  Error code(s): 
 *
 *****************************************************************************/

void s_unmaskall(){
  sigset_t set;

  sigfillset(&set); 
  sigprocmask(SIG_UNBLOCK,&set,NULL);
}


/*****************************************************************************
 *  Function: s_install      
 *
 *  Description:  Install a signal handler
 *
 *         In:    Signal to manage, pointer to the signal handler 
 *
 *         Out:   struct sigaction of the old signal handler
 *  Error code(s): 
 *
 *****************************************************************************/

struct sigaction s_install(signal, signal_handler)
int signal;
void(*signal_handler)();
{
  struct sigaction act,oldact;
  
  act.sa_handler = signal_handler;
  act.sa_flags = 0;
  sigemptyset(&act.sa_mask);
  if(sigaction(signal,&act,&oldact)) {
	 perror("sigaction"); /* debug */
	 printf("Merde\n");
  }
  return(oldact);
}


/*****************************************************************************
 *  Function: s_install      
 *
 *  Description:  Install a signal handler
 *
 *         In:    Signal to manage, pointer to the old signal handler 
 *
 *         Out:   -
 *  Error code(s): 
 *
 *****************************************************************************/


void s_restore(signal,oldact)
int signal;
struct sigaction oldact;
{
  if(sigaction(signal,&oldact,NULL)) perror("sigaction"); /* debug */
}
