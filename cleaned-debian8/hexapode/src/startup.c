/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/src/RCS/startup.c,v 2.3 2012/09/24 09:22:53 perez Rel $";*/
/*********************************************************************
 *
 * File:  startup.c
 *
 * Project:  Hexapode Device Server
 *
 * Description:  Startup procedure for HexapodeClass. The
 *    startup procedure is the first procedure called
 *    from main() when the device server starts up.
 *    All toplevel devices to be created for the device 
 *    server should be done in startup(). The startup 
 *    should make use of the database to determine which 
 *    devices it should create. Initialisation of devices
 *    is normally done from startup().
 *
 * Author(s);  Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:   March 11 1993  
 *
 * $Log: startup.c,v $
 * Revision 2.3  2012/09/24 09:22:53  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:15  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:12  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.34  2009/05/25 12:37:36  perez
 * Fix bug in VPAP retries to avoid slowing down DS
 *
 * Revision 1.31  2008/01/17 10:17:25  rey
 * Fix bug with usleep
 *
 * Revision 1.30  2006/05/22 14:50:43  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.29  2006/05/22 14:47:26  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.28  2006/05/22 14:41:40  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.27  2006/05/22 14:39:50  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.26  2006/05/22 14:37:55  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.23  2006/03/16 14:11:24  rey
 * CheckAll no return. bug solved
 *
 * Revision 1.22  2004/03/09  16:29:21  16:29:21  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:10  16:36:10  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:12  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.13  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.12  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.11  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.10  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.9  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.8  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.7  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.6  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.5  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.4  1993/10/26  07:53:43  rey
 * Version at 26 October
 *
 * Revision 1.3  1993/10/26  07:51:36  rey
 * Version del 26 Octubre.
 *
 * Revision 1.2  1993/10/16  20:12:41  rey
 * A domingo 16
 *
 * Revision 1.1  93/10/11  17:47:17  17:47:17  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 * Revision 1.2  1993/10/03  14:48:41  rey
 * Antes de empezar a marranear
 *
 * Revision 1.1  1993/09/30  12:59:20  rey
 * Initial revision
 *
  
 * Copyright (c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *********************************************************************/
#ifdef _UCC
#include <module.h>
#endif

#include <signal.h>
#include <Admin.h>
#include <API.h>
#include <DevServer.h>
#include <DevErrors.h>
#include <DevServerP.h>

#include <HexapodeP.h>
#include <Hexapode.h>

#define DEBUG_LVL1

#define CLASS_PREFIX "CLASS/"
#define MAX_NO_OF_DEVICES 6

extern long  no_dev;


/************************************************************************
 *
 * Function   :  long startup()
 * 
 * Description:  create, initialise and export all of the objects
 *               associated with this server
 * 
 * Arg(s) In  :  svr_name
 * 
 * Arg(s) Out :  error:      error return code
 * 
 * Return(s)  :  minus one on failure, zero otherwise
 * 
 * *************************************************************************/
long startup(svr_name, error)
char *svr_name;
DevLong *error;
{
   Hexapode  ds_list[MAX_NO_OF_DEVICES];
   long      i,
             status,
             n_exported;
   short     iret;

/*
 * pointer to list of devices returned by database.
 */
   char   **dev_list;
   unsigned int      n_devices;

#ifdef DEBUG_LVL1
   printf("<STARTUP> Startup-ing -> %s\n", svr_name);
#endif

    if ( (hexapodeClass->hexapode_class.class_svr_name = (char *)
            malloc( strlen(svr_name) + sizeof(CLASS_PREFIX) + 1)) == 0 ){
        *error = DevErr_InsufficientMemory;
        return (DS_NOTOK);
    }
    sprintf(hexapodeClass->hexapode_class.class_svr_name, "%s%s", CLASS_PREFIX, svr_name);


   if (db_getdevlist(svr_name,&dev_list,&n_devices,error))
   {
       printf("startup(): db_getdevlist() failed, error %d\n",*error);
       return(-1);
   }

#ifdef DEBUG_LVL1
   printf("\t<STARTUP> the following devices were found in static database:\n");

   no_dev = n_devices;

   for (i=0;i<n_devices;i++)
   {
       printf("\t\t - %s\n",dev_list[i]);
   }
#endif

/*
 * create, initialise and export all devices served by this server
 */
   
   for (i=0, n_exported = 0; i < n_devices; i++)
   {
#ifdef DEBUG_LVL1
       printf("\t<STARTUP> Object: %s\n", dev_list[i]);
#endif
       fflush(stdout);

       if (ds__create(dev_list[i], hexapodeClass, &(ds_list[i]),error) != 0)
       {
           printf("<STARTUP> create %s failed.",dev_list[i]);
           continue;
       }
#ifdef DEBUG_LVL1
       else printf("\t<STARTUP> - %s created\n", dev_list[i]);
#endif

     /* 
      * initialise the newly created hexapode 
      */
  
     if(ds__method_finder(ds_list[i],DevMethodInitialise)(ds_list[i],error)!= 0)
       {
           printf("<STARTUP> initialise failed, ");
           continue;
       }
#ifdef DEBUG_LVL1
       else printf("\t<STARTUP> - %s initialised\n", dev_list[i]);
#endif

     /*  
      * Now export it to the outside world.
      */
 
       if (ds__method_finder(ds_list[i],DevMethodDevExport)(dev_list[i],ds_list[i],error) != 0)
       {
            printf("<STARTUP> export failed, ");
            printf("<STARTUP> %s", dev_error_str(*error));
            continue;
       }
#ifdef DEBUG_LVL1
       else printf("\t<STARTUP> - %s exported\n", dev_list[i]);
#endif
		n_exported++;
   }

   switch(n_exported)
   {
       case 0:  
           printf("<STARTUP> No devices exported - Hexapode server exiting\n");
           *error = 0;
           iret = -1;
           break;

       case 1:  
#ifdef DEBUG_LVL1
           printf("<STARTUP> Hexapode server running with 1 device exported\n\n");
#endif
           *error = 0;
           iret = 0;
           break;

       default:
#ifdef DEBUG_LVL1
           printf("<STARTUP> Hexapode server running %d devices exported\n\n",
                   n_exported);
#endif
           *error = 0;
           iret = 0;
    }
    return(iret);
}

