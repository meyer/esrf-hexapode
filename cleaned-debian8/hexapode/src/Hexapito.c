/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/src/RCS/Hexapito.c,v 2.3 2012/09/24 09:22:53 perez Rel $";*/
/*********************************************************************
 *
 * File:         Hexapito.c
 *
 * Project:      Hexapito (Hexapode control program)
 *
 * Description:  Main for subprocess.
 *
 * Author(s):    Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:     March 11 1993
 *
 * $Log: Hexapito.c,v $
 * Revision 2.3  2012/09/24 09:22:53  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:15  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:12  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.34  2009/05/25 12:37:36  perez
 * Fix bug in VPAP retries to avoid slowing down DS
 *
 * Revision 1.31  2008/01/17 10:17:25  rey
 * Fix bug with usleep
 *
 * Revision 1.30  2006/05/22 14:50:43  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.29  2006/05/22 14:47:26  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.28  2006/05/22 14:41:40  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.27  2006/05/22 14:39:50  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.26  2006/05/22 14:37:55  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.25  2006/05/22 14:37:02  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.23  2006/03/16 14:11:24  rey
 * CheckAll no return. bug solved
 *
 * Revision 1.22  2004/03/09  16:29:22  16:29:22  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:12  16:36:12  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:12  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.13  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.12  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.11  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.10  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.9  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.8  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.7  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.6  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.5  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.4  1993/10/26  07:53:43  rey
 * Version at 26 October
 *
 * Revision 1.3  1993/10/26  07:51:36  rey
 * Version del 26 Octubre.
 *
 * Revision 1.1  93/10/11  17:47:08  17:47:08  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 * Revision 1.2  1993/10/03  14:48:41  rey
 * Antes de empezar a marranear
 *
 * Revision 1.1  1993/09/30  12:59:20  rey
 * Initial revision
 *
 *
 * Copyright(c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 *********************************************************************/

#define DEBUG_LVL1
#define DEBUG_LVL2
#define DEBUG_LVL3
/*
#define DEBUG_LVL1
#define DEBUG_LVL2
#define DEBUG_LVL3
*/

#ifdef DEBUG_LVL1
#     define MAIN_LOOP_SLEEP 15 
#else
#     define MAIN_LOOP_SLEEP 20          /* seconds */
#endif

#ifdef linux
#     define MINIMAL_LOOP_SLEEP 100000  /* microseconds */
#endif

#ifdef linux
#define MICROSLEEP
#endif
#define PR_MICROSLEEP 100

#define HANDSHAKING_SLEEP 5
#define PR_KILLSLEEP 4


/*
 * Includes.
 */

#ifdef linux
#    include <SharedMemory.h>
#    include <SignalHandling.h>
#    include <wait.h>
#endif

#include <signal.h>
#include <math.h>
#include <ctype.h>
#include <DevServer.h>
#include <DevServerP.h>
#include <Hexapode.h>
#include <HexapodeP.h>

#define MAX_FILE_LINE_LENGTH  101
#define FH_Header          "# Hexapode Backup file   (lengths in meters)\n#"
/*
#define FH_SavedAt         "\n# File saved at: "
*/
#define FH_Description     "\n# Hexapode description: "
#define FH_Velocity        "\n# Velocity: "
#define FH_Last_Reset      "\n# Last reset: "
#define FH_HardReset                           "HARD RESET"
#define FH_SoftReset                           "SOFT RESET"
#define FH_CurrentLengths  "\n# Current actuator lengths (1 to 6):"
#define FH_MovingFrom      "\n# Moving actuators (1 to 6) from:"
#define FH_MovingTo        "\n# to:"
#define FH_EndOfFile       "\n# End of file\n"
#define FH_LengthFormat    "\n%12.10f"

/* 
 * Declaration of functions and variables defined in Auxiliary.c
 */
/*
extern void printPos();
extern void printLengths();
*/
/*
 * Protocol related declarations
 */

/* begmod PM */
#ifdef linux
void sigRequest();
void sigExecute();
void sigCompleted();
void sigQuit();
#endif
/* endmod PM */

/* 
 * Declaration of functions and variables defined in Geometry.c
 */
extern long CheckAll();
extern long SetActuatorTrajectory();
extern long SetTableTrajectory();
extern long SetRotationTrajectory();
extern long SetTraslationTrajectory();
/*
//extern long UpdateRefSystem();
//extern long LToPosition();
//extern void NormalVectorExtraction();
//extern void UpdateJointCoords();
//extern long CheckLengths();
//extern long CheckTilt();
//extern long CheckPosition();
//extern long CheckTrajectory();
//extern long ActuatorTrajectory();
//extern long TableTrajectory();
//extern long RotationTrajectory();
//extern long TraslationTrajectory();
*/
extern long  (*Trajectory)();
extern double  Delta_t;

/*
 * Internal declarations.
 */
long  LinkSharedMemory();
long  CheckMovement();
long  ContinueMovement();
long  ExecuteCommand();
long  SavePosition();
long  CheckCurrentPosition();
long  CheckFault();
long  CheckCommand();
long  GlobalReset();
long  ContinueHomeSearch();
long  ContinueHardReset();
long  ResetEnd();
long  SaveLengthFile();
long  ReadLengthFile();
long  DeleteFile();

void  UpdateDelta();


/*
 *  Global variables.
 */
int     ProcessState; 
int     Moving        = 0;
int     SearchingHome = 0;
int     Resetting     = 0;
double  Next_t;
double  NextLength[6];
double  ActuatorDelta[6];
long    GlobalStatus;


/* begmod PM */
  /*
   * related with Hydraulic hexapode
   */
#ifdef linux
short stop_pid;
int   stop_status;
#endif
/* endmod PM */

/*
 * Shared memory pointers.
 */
SharedData    *SD;
GeometryData  *GD;
ActuatorData  *AD;

/*
 * Interprocess variables.
 */
short parentPid;
int   handshaking;

/* begmod PM */
#ifdef linux
char *sgm_ptr;
key_t sgm_key;
#endif
/* endmod PM */



/* begmod PM */
/*
 * in Linux signal handlers receive no param. A different function is used
 * for each  of them. With OS9 sighand handles them all.
 */

void sigQuit() {
   printf("<HEXAPITO> SigQuit Interrupt Handler\n");

   if(ProcessState == HEXAP_RUNNING) {
       ProcessState = HEXAP_NOT_RUNNING;
   } else {
       printf("<HEXAPITO> Killed\n"); /* it`s perror safe ? */
       exit(0);
   }

   /* for hydraulic hexapode stops feedback process too */

   if ( (AD->MotorData.MotorType == HYDRAULIC) && 
        (SD->MovementMode == NORMAL_MODE) ) { 
       kill(AD->MotorData.MotorPrivate.hyd.feedback_pid,SIGINT);
       stop_pid = wait(&stop_status);

       if (stop_pid == -1)
           printf("<HEXAPITO> Panic killing hxfeedback\n");

       if ((WIFEXITED(stop_status) != 0 ) && WEXITSTATUS(stop_status) != 0) {
           printf("<HEXAPITO> Hxfeedback exited with code %d\n",
                    WEXITSTATUS(stop_status));
       }

       if (WIFSIGNALED(stop_status)) {
          printf("<HEXAPITO> Hxfeedback died with signal %d\n",
                    WTERMSIG(stop_status));
       }
   }

}

void sigRequest() {
   handshaking = COMMAND_REQUEST;
}

void sigExecute() {
   handshaking = COMMAND_EXECUTE;
}

void sigCompleted() {
   handshaking = COMMAND_COMPLETED;
}

/* endmod PM */



/***************************************************************************
 *  Function:     LinkSharedMemory()
 *  
 *  Description:  Signal handler for subprocess.
 *
 *  In:           name       Shared memory module name
 *
 *  Out:         *ppointer   Pointer to shared memory
 *
 ***************************************************************************/
long  LinkSharedMemory(key, ppointer)

key_t *key;

void  **ppointer;
{
   char   *ptmodule;

#ifdef DEBUG_LVL2 
   printf("<HEXAPITO> LinkSharedMemory\n");
#endif

#ifdef linux
   if ((ptmodule = (char *)modlink(key)) == (char *)(-1))
       return(NOT_OK);
   sgm_ptr = ptmodule; /* global variable for base address of the memory segment */ 
   *ppointer = (void *) (ptmodule);
   return(OK);
#endif

#ifndef linux
   return(NOT_OK);
#endif

}


/***************************************************************************
 *  Function:     UnlinkSharedMemory()
 *  
 *  Description:  
 *
 *  In:           name       Shared memory module name (os9) or sgm_ptr for linux
 *
 ***************************************************************************/
long  UnlinkSharedMemory(name)
char  *name;
{
#ifdef DEBUG_LVL2 
   printf("<HEXAPITO> UnlinkSharedMemory\n");
#endif
    
    modunlink(name);

   return(OK);
}


/***************************************************************************
 *  Function:     main()
 *  
 *  Description:  
 *
 *  In:          
 *
 ***************************************************************************/
int main(argc,argv)

unsigned int argc;
char       **argv;
{

   long error;
#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> I am alive\n");
#endif

   /*
    * First link to shared memory. 
    *      shared memory name: argv[1].
    */
   if (argc != 2 ) {
      printf("<HEXAPITO> Wrong initializatin arguments. Exiting ...\n");
      exit(0);
   }

   sscanf( argv[1], "%d", &sgm_key);
   if (LinkSharedMemory(sgm_key, &SD) != OK){

      printf("<HEXAPITO> Cannot link shared memory. Exiting ...\n");
      exit(0);
   }
   /*
    * Assign pointers.
    */
   GD = &(SD->GData);
   AD = &(SD->AData);
   
   /*
    * Get ready to communicate.
    */
   parentPid = SD->serverpid;

   s_install(COMMAND_EXECUTE,   sigExecute);
   s_install(COMMAND_REQUEST,   sigRequest);
   s_install(COMMAND_COMPLETED, sigCompleted);
   s_install(SIGQUIT,           sigQuit);
   s_install(SIGTERM,           sigQuit); 
   s_install(SIGINT,            sigQuit);

   handshaking = NO_COMMAND;
   SD->Error   = 0;
   SD->State   = HXPD_FAULT; 

   /* ActuatorsInit(SIMULATION_MODE, &GD->MechResolution); */
   ActuatorsInit(SD->MovementMode, &GD->MechResolution);

   /*
    * I'm ok and ready and I let the world know.
    */

#ifdef MICROSLEEP
   usleep(PR_MICROSLEEP);
#endif
   kill(parentPid, COMMAND_SUCCESFUL);
   /*sleep(PR_KILLSLEEP); */

   ProcessState = HEXAP_RUNNING;

   /*
    * Main loop.
    */
#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> Entering main loop\n");
#endif

   do {
      if (SD->State == HXPD_READY || SD->State == HXPD_FAULT) {
         sleep(MAIN_LOOP_SLEEP);
      }
      else {                                /* HXPD_MOVING */
          usleep(MINIMAL_LOOP_SLEEP);
      }

      /*
       * 
       */
      if (SD->State != HXPD_FAULT){
         if (CheckMovement() != OK ||
             (!Resetting && !SearchingHome && CheckCurrentPosition() != OK)){
            SD->State = HXPD_FAULT;
            SD->LastError = SD->Error;
         } else if (!Moving)
            SD->State = HXPD_READY;
      } else {
         CheckFault();
       }

       /*
        * Did a command arrive?
        */

       if (handshaking == COMMAND_REQUEST){
           /*
            * Command accepted.
            */
#ifdef MICROSLEEP
           usleep(PR_MICROSLEEP);
#endif
           kill(parentPid, COMMAND_ACCEPTED);
           sleep(PR_KILLSLEEP); 

           /*if (handshaking != COMMAND_EXECUTE)
                 sleep(HANDSHAKING_SLEEP);
           */

           /*
            * If COMMAND_EXECUTE the main process has written its data.
            */ 

           if (handshaking == COMMAND_EXECUTE){

               if (CheckCommand() == OK && ExecuteCommand() == OK) {
#ifdef MICROSLEEP
                    usleep(PR_MICROSLEEP);
#endif
                    kill(parentPid, COMMAND_SUCCESFUL);
                    sleep(PR_KILLSLEEP);
               } else {
#ifdef MICROSLEEP
                    usleep(PR_MICROSLEEP);
#endif
                    kill(parentPid, COMMAND_ERROR);
                    sleep(PR_KILLSLEEP);
               } 

               /* check if Hexapode already responded */
  /*
               if (handshaking != COMMAND_COMPLETED)
                    sleep(HANDSHAKING_SLEEP);
*/

               /* 
                * The main process has read the data.
                */

               if (handshaking == COMMAND_COMPLETED) {
#ifdef MICROSLEEP
                   usleep(PR_MICROSLEEP);
#endif
                   kill(parentPid, COMMAND_END_ACKN);
                   /* sleep(PR_KILLSLEEP);*/
               } else {

#ifdef linux
                  printf("Time out, server not responding 1\n");
#endif
                }



           } else 
#ifdef linux
                  printf("Time out, server not responding 2\n");
#endif
       }
      /*  
       * End of a command execution.
       */
       handshaking = NO_COMMAND;

    } while(ProcessState == HEXAP_RUNNING);

    if (SD->State == HXPD_MOVING && StopActuators() == OK) 
       if(!Resetting) 
          if (AD->MechanicalRef != NONE)
              SaveLengthFile(SD->BackupFile, SD->ActuatorLength, NULL);

#ifdef DEBUG_LVL1 
    printf("<HEXAPITO> Exiting loop and dying\n");
#endif
    
    UnlinkSharedMemory(sgm_ptr);

    exit(0);
}

/*************************************************************************
 *
 * Function:    CheckCurrentPosition()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 **************************************************************************/
long  CheckCurrentPosition()
{
#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> CheckCurrentPosition\n");
#endif

    if (SD->State != HXPD_READY && SD->State != HXPD_MOVING){
        SD->Error = DevErr_InternalBug;
        return(NOT_OK);
    }

    if (CheckAll(SD->ActuatorLength, SD->Position) != OK) {
        if (StopActuators() == OK)
           SaveLengthFile(SD->BackupFile, SD->ActuatorLength, NULL);
        return(NOT_OK);
    } else
        return(OK);
}

/*************************************************************************
 *
 * Function:    CheckMovement()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 **************************************************************************/
long  CheckMovement()
{
   int  i;

#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> CheckMovement\n");
#endif
   /* 
    * Reads status and position.
    */
   if (CheckActuators(&GlobalStatus) != OK)
      return(NOT_OK);

#ifdef DEBUG_LVL2
    printf("<HEXAPITO> Status: %s\n",
                               ValueToLabel(GlobalStatus, &actuatorStateList));
#endif

   if (GlobalStatus == DEV_FAULT)
        return(NOT_OK);

   /*
    * Compares GlobalStatus and last State and decides what to do.
    */
 
    switch(SD->State) {

        case HXPD_READY:
            if (GlobalStatus == DEV_READY)
                return(OK);
            if (GlobalStatus == DEV_LIMIT){
                SD->Error = DevErr_LimitSwitchReached;
                return(NOT_OK);
            } else if (GlobalStatus == DEV_MOVING){
                SD->Error = DevErr_UnexpectedMovement;
                return(NOT_OK);
            }
            break;

        case HXPD_MOVING:
            if (Resetting)
                return(ContinueHardReset());
            else if (SearchingHome)
                return(ContinueHomeSearch(NORMAL_SEARCH));
            else if (Moving && GlobalStatus == DEV_MOVING)
                return(OK);
            else if (Moving && GlobalStatus == DEV_READY)
                return(ContinueMovement());
            else if (Moving && GlobalStatus == DEV_LIMIT){
                if(StopActuators() == OK)
                   Moving = 0;
                SD->Error = DevErr_LimitSwitchReached;
                return(NOT_OK);
            } else 
                StopActuators();
            
            break;

    }
    SD->Error = DevErr_InternalBug;
    return(NOT_OK);
}

/*************************************************************************
 *
 * Function:    CheckFault()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 **************************************************************************/
long  CheckFault()
{
   long   status;
   int    i;

#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> CheckFault\n");
#endif

   if(CheckActuators(&GlobalStatus) != OK)
      return(OK);

   status = 0;
   for (i = 0; i < 6; i++)
      status |= SD->ActuatorStatus[i];

   if (status & DEV_MOVING || status & DEV_SEARCHING)
      return(StopActuators());
   else
      return(OK);
}

/*************************************************************************
 *
 * Function:    CheckCommand()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 **************************************************************************/
long  CheckCommand()
{
   long   ret = OK;

#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> CheckCommand: %d  State: %d\n", SD->Command, SD->State);
#endif

   if (SD->Command == DEV_GET_INFO ||
       SD->Command == DEV_ABORT_MOVEMENT)
      return(OK);

   switch(SD->State){
      case HXPD_READY:
         /*
          *  In the HXPD_READY all commands are allowed.
          */
         break;

      case HXPD_MOVING:
         /*
          *  In the HXPD_MOVING state only the commands
          *  DEV_GET_INFO and DEV_ABORT_MOVEMENT are allowed.
          */
         ret = NOT_OK;
         break;

      case HXPD_FAULT:
         /*
          *  In the HXPD_FAULT state only the commands that include a Reset
          *  are allowed and DEV_ABORT_MOVEMENT .
          */
         switch (SD->Command){
/* begmod PM */
            case DEV_SEARCH_HOME:
                if (SD->LastError == DevErr_NotHomed) 
                      break;
                else
                      ret = NOT_OK;
                      break;
/* endmod PM */

            case DEV_RESET:
            case DEV_SET_ACT_LENGTH:
            case DEV_SET_MODE:
               break;

            default:
               ret = NOT_OK;
         }
         break;

      default:
         StopActuators();
         ret = NOT_OK;
   }
   if (ret != OK)
      SD->Error = DevErr_NonAcceptableCommand;

   return(ret);
}

/*************************************************************************
 *
 * Function:    ExecuteCommand()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 **************************************************************************/
long  ExecuteCommand()
{
#define Movement_START                       \
           Moving = 0;                       \
           if (ContinueMovement() != OK){    \
              if (Moving){                   \
                  SD->State = HXPD_FAULT;    \
                  SD->LastError = SD->Error; \
              } else                         \
                  SD->State = HXPD_READY;    \
              StopActuators();               \
              Moving = 0;                    \
              return(NOT_OK);                \
           } else                            \
              SD->State = HXPD_MOVING

#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> ExecuteCommand %d\n", SD->Command);
#endif
    switch(SD->Command)
    {
         case DEV_GET_INFO:
              break;

         case DEV_SEARCH_HOME:
              if (!AD->CleverHomeSearch){
                 SD->Error = DevErr_NonCompatibleFunction;
                 return(NOT_OK);
              }
              SearchingHome = 0;
              if (ContinueHomeSearch(NORMAL_SEARCH) != OK){
                  if (Moving){
                      SD->State = HXPD_FAULT;
                      SD->LastError = SD->Error;
                  } else
                      SD->State = HXPD_READY;
                  return(NOT_OK);

              } else if (Moving)
                 SD->State = HXPD_MOVING;
              break;

         case DEV_SET_REF_SYSTEM:
              if (SetReferenceSystem(SD->NewRefSystem) != OK )
                  return(NOT_OK);
              break;

         case DEV_SET_REF_POSITION:
              if (SetReferencePosition(SD->OldPosition, 
                                       SD->NewPosition) != OK)
                   return(NOT_OK);
              break;

         case DEV_SET_REF_ORIGIN:
              if (SetReferenceOrigin(SD->Position,
                                     SD->NewReferenceOrigin) != OK)
                   return(NOT_OK);
              break;

         case DEV_SET_REF_AXIS:
              if (SetReferenceAxis(SD->Position,
                                   SD->OldDirection,
                                   SD->NewDirection) != OK)
                  return(NOT_OK);
              break;

         case DEV_DEF_REFERENCE:
              if (SetDefaultReference() != OK) 
                   return(NOT_OK);
              break;

         case DEV_CHECK_POSITION:
              if (CheckIfValidPosition(SD->FinalPosition,
                                       SD->FinalActuatorLength) != OK) 
                   return(NOT_OK);
              break;

         case DEV_MOVE_ACTUATORS:
              if (SetActuatorTrajectory(SD->ActuatorLength,
                                        SD->FinalActuatorLength) != OK)
                   return(NOT_OK);
              Movement_START;
              break;

         case DEV_MOVE_TABLE:
              if (SetTableTrajectory(SD->Position,
                                     SD->FinalPosition) != OK)
                  return(NOT_OK);
              Movement_START;
              break;

         case DEV_ROTATE_TABLE:
              if (SetRotationTrajectory(SD->Position,
                                        SD->RotationOrigin, 
                                        SD->RotationAxis,
                                        SD->RotationAngle) != OK)
                   return(NOT_OK);
              Movement_START;
              break;

         case DEV_TRANSLATE_TABLE:
              if (SetTraslationTrajectory(SD->Position,
                                          SD->TranslationVector) != OK)
                   return(NOT_OK);
              Movement_START;
              break;

         case DEV_ABORT_MOVEMENT:
              if (StopActuators() != OK) {
                   SD->State = HXPD_FAULT;
                   SD->LastError = SD->Error;
                   return(NOT_OK);
              }

              if (Resetting && !AD->CleverHomeSearch){
                   SD->State = HXPD_FAULT;
                   SD->LastError = DevErr_BlindResetInterrupted;
              } else if (SD->State == HXPD_MOVING){
                   if (CheckActuators(&GlobalStatus) == OK){
                      if (GlobalStatus == DEV_READY){
                         if (AD->MechanicalRef != NONE)
                           if(SaveLengthFile(SD->BackupFile, SD->ActuatorLength,
                                                                  NULL) == OK){
                            SD->State = HXPD_READY;
                            break; 
                         }
                      } else
                         SD->Error = DevErr_UnexpectedMovement;
                   }
                   SD->State = HXPD_FAULT;
                   return(NOT_OK);
              }
              /*    If previous state was HXPD_READY or         *
               *    HXPD_FAULT don't do anything                */
              break;

         case DEV_SET_ACT_LENGTH:
              CopyVector(6, SD->NewActuatorLength, SD->ActuatorLength);
              SD->LastResetMode = SOFT_RESET;
              if (AD->MechanicalRef != NONE)
                 if (SaveLengthFile(SD->BackupFile, SD->ActuatorLength, NULL) != OK)
                     return(NOT_OK);
              /*
               * Execute Soft Reset after changing the length file.
               */
              if (GlobalReset(SOFT_RESET) != OK){
                  SD->State = HXPD_FAULT;
                  SD->LastError = SD->Error;
                  return(NOT_OK);
              }
              SD->State = HXPD_READY;
              break;

         case DEV_SET_VELOCITY:
              if (SD->NewVelocity < MIN_VELOCITY || SD->NewVelocity > 1.){
                  SD->Error = DevErr_BadVelocity;
                  return(NOT_OK);
              }
              AD->Velocity = SD->NewVelocity;
              break;

         case DEV_SET_RESOLUTION:
              if (SD->NewMovementResolution == 0)
                  GD->MovementResolution = SD->MaxMovementResolution;
              else if (SD->NewMovementResolution < 100 * GD->MechResolution ||
                  SD->NewMovementResolution > SD->MaxMovementResolution){
                  SD->Error = DevErr_BadMovementResolution;
                  return(NOT_OK);
              }
              GD->MovementResolution = SD->NewMovementResolution;
              break;

         case DEV_SET_MODE:
              if ((SD->NewMovementMode != NORMAL_MODE) &&
                                     (SD->NewMovementMode != SIMULATION_MODE)){
                  SD->Error = DevErr_WrongMovementMode;
                  return(NOT_OK);
              }

/* begmod PM (hydraulic only)*/
#ifdef linux
              if ( (SD->MovementMode == NORMAL_MODE) &&  
                                     (AD->MotorData.MotorType == HYDRAULIC)) {
                /* 
                 * if hydraulic controller kill feedback process 
                 */
#ifdef DEBUG_LVL1 
                printf("<HEXAPITO> Kill feedback process pid = %d\n",
                               AD->MotorData.MotorPrivate.hyd.feedback_pid);
#endif
                kill(AD->MotorData.MotorPrivate.hyd.feedback_pid,SIGINT);
                stop_pid = wait(&stop_status);
                if (stop_pid == -1)
                     printf("<HEXAPITO> Panic killing hxfeedback\n");
                if((WIFEXITED(stop_status) != 0 ) && WEXITSTATUS(stop_status) != 0) {
                    printf("<HEXAPITO> Hxfeedback exited with code %d\n",
                         WEXITSTATUS(stop_status));
                }

                if(WIFSIGNALED(stop_status)) {
                    printf("<HEXAPITO> Hxfeedback died with signal %d\n", \
                         WTERMSIG(stop_status));
                }
              }
#endif

/* endmod PM */

              /*
               * Execute Soft Reset after changing movement mode.
               */
              SD->MovementMode = SD->NewMovementMode;
              if (GlobalReset(SOFT_RESET) != OK){
                  SD->State = HXPD_FAULT;
                  SD->LastError = SD->Error;
                  return(NOT_OK);
              }
              SD->State = HXPD_READY;
              break;

         case DEV_RESET:
/* begmod PM (hydraulic only)*/
#ifdef linux
              if ( (SD->MovementMode == NORMAL_MODE) &&  
                       (AD->MotorData.MotorType == HYDRAULIC)) {  
              /* 
               * if hydraulic controller kill feedback process 
               */
#ifdef DEBUG_LVL1 
                   printf("<HEXAPITO> Kill feedback process pid = %d\n",
                             AD->MotorData.MotorPrivate.hyd.feedback_pid);
#endif
                   kill(AD->MotorData.MotorPrivate.hyd.feedback_pid,SIGINT);
                   stop_pid = wait(&stop_status);
                   if (stop_pid == -1)
                      printf("<HEXAPITO> Panic killing hxfeedback\n");
                   if((WIFEXITED(stop_status) != 0 ) && WEXITSTATUS(stop_status) != 0) {
                      printf("<HEXAPITO> Hxfeedback exited with code %d\n",
                          WEXITSTATUS(stop_status));
                   }
                   if(WIFSIGNALED(stop_status)) {
                      printf("<HEXAPITO> Hxfeedback died with signal %d\n",
                          WTERMSIG(stop_status));
                   }
               }
#endif
/* endmod PM */

              if (GlobalReset(SD->ResetMode) != OK){
                  SD->State = HXPD_FAULT;
                  SD->LastError = SD->Error;
                  return(NOT_OK);
              } else if (Moving)
                 SD->State = HXPD_MOVING;
              else
                 SD->State = HXPD_READY;
              break;

         case DEV_QUIT:
              ProcessState = HEXAP_NOT_RUNNING;
              break;

         default:
              SD->Error = DevErr_WrongInternalCommand;        
              return(NOT_OK);
              break;
    }
    return(OK);
}

/*************************************************************************
 *
 * Function:    ContinueMovement()
 *
 * Description: Send the next step in the trajectory.
 * 
 * Variables involved:
 *
 * Error(s):
 *
 **************************************************************************/
long  ContinueMovement()
{
   static  short Finished;
   int     i;
 
#ifdef DEBUG_LVL1
   printf("<HEXAPITO> ContinueMovement %f,delta: %f\n",Next_t,Delta_t);
#endif

   if (!Moving){
      if (CheckActuators(&GlobalStatus) != OK)
         return(NOT_OK);

      if (GlobalStatus != DEV_READY){
            if (GlobalStatus == DEV_MOVING)
               SD->Error = DevErr_UnexpectedMovement;
         return(NOT_OK);
      }

      Trajectory(Next_t = (double)0.0, NextLength);
      Moving = 1;
      Resetting = 0;
      SearchingHome = 0;
      Finished = 0;
   }

/*
   printf("ContinueMovement: Moving motors to:\n");
   for (i = 0; i < 6; i++) {
      printf("Leg%d - %f\n",i+1,NextLength[i]);
   }
*/
   for (i = 0; i < 6; i++) {
      if(fabs(SD->ActuatorLength[i] - NextLength[i]) > GD->MechResolution)
         return(MoveActuatorsTo(NextLength));
   }
  
   if (AD->MechanicalRef != NONE) 
      if (SaveLengthFile(SD->BackupFile, SD->ActuatorLength, NULL) != OK)
          return(NOT_OK);

   if (Finished){
      Moving = 0;
      return(OK);
   }

   if (Next_t < 1) {
      Next_t += Delta_t;
      if (Trajectory(Next_t, NextLength) != OK)
         return(NOT_OK);

      UpdateDelta();

     
      if (Next_t >= 1 && AD->Backlash != 0){
         for (i = 0; i < 6; i++)
            if ((ActuatorDelta[i] / AD->Backlash) < 1)
               NextLength[i] -= AD->Backlash;
      }

      if (AD->MechanicalRef != NONE) 
          if (SaveLengthFile(SD->TmpBackupFile, SD->ActuatorLength, NextLength) 
                                                                         != OK)
              return(NOT_OK);

      return(MoveActuatorsTo(NextLength));
   } 
 
   /*
    * If Next_t >= 1. Movement is finished.
    */
    /*
     * These final Trajectory() and MoveActuatorsTo() calls seem to be redundant
     * and inecessary. To be eliminated in next version. (PF)
     */ 
   Finished = 1;
   if (Trajectory(1., NextLength) != OK)
      return(NOT_OK);

   if (AD->MechanicalRef != NONE) 
      if (SaveLengthFile(SD->TmpBackupFile, SD->ActuatorLength, NextLength) != OK)
          return(NOT_OK);

   UpdateDelta();
   return(MoveActuatorsTo(NextLength));
}


void UpdateDelta()
{
   double  delta;
   int     i;

   for (i = 0; i < 6; i++){
      delta = NextLength[i] - SD->ActuatorLength[i];
      if (fabs(delta) > GD->MechResolution){
         if ((ActuatorDelta[i] / delta) > 0)
            ActuatorDelta[i] += delta;
         else
            ActuatorDelta[i] = delta;
      }
   }
}

/*************************************************************************
 *
 * Function:    CheckSD()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 *************************************************************************/
long  CheckSD()
{
   int  i;

#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> CheckSD\n");
#endif

   /*
    *  Check and set resolution variables.
    *  (GD->MechResolution and SD->MaxMovementResolution)
    */
   if (SD->MaxMovementResolution > 100 * GD->MechResolution)
      GD->MovementResolution = SD->MaxMovementResolution;
   else {
      SD->Error = DevErr_BadMovementResolution;
      return(NOT_OK);
   }

   /*
    *  Check that max deviations are positive.
    */
   if (GD->MaxTiltAngle <= 0){
      SD->Error = DevErr_BadMovementLimits;
      return(NOT_OK);
   }
   for (i = 0; i < 6; i++) 
      if (GD->MaxIncrement[i] <= 0){
         SD->Error = DevErr_BadMovementLimits;
         return(NOT_OK);
   }

   /*
    *  Check the Mechanical Reference type.
    */
   switch(AD->MechanicalRef){
      case NONE:
      case ABSOLUTE:
      case LIMIT:
      case MIDPOINT:
         break;

      default:
         SD->Error = DevErr_WrongHomeType;
         return(NOT_OK);
   }

   return(OK);
}

/*************************************************************************
 *
 * Function:    GlobalReset()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 *************************************************************************/
long  GlobalReset(reset_mode)
long	reset_mode;
{
   int	i;

#ifdef DEBUG_LVL3 
   printf("<HEXAPITO> GlobalReset: %d\n", reset_mode);
   printf("    Movement mode     : %d\n", SD->MovementMode);
#endif

   SD->LastError = SD->Error = DevErr_HxpdNoError;

   if (reset_mode != HARD_RESET && reset_mode != SOFT_RESET){
       SD->Error = DevErr_WrongResetMode;
       return(NOT_OK);
   }

   /*
    * Initialises Hardware
    */
   if (ActuatorsInit(SD->MovementMode, &GD->MechResolution) != OK) {
#ifdef DEBUG_LVL3 
   printf("<HEXAPITO> GlobalReset: ActuatorsInit returns an error\n");
#endif
       return(NOT_OK);
   }

   /*
    * Checks shared memory contents.
    */
   if (CheckSD() != OK) {
#ifdef DEBUG_LVL3 
   printf("<HEXAPITO> GlobalReset: CheckSD returns an error\n");
#endif
      return(NOT_OK);
   }

   /*
    * Initialises geometrical parameters
    */
   if (GeometryInit() != OK) {
#ifdef DEBUG_LVL3 
   printf("<HEXAPITO> GlobalReset: GeometryInit returns an error\n");
#endif
      return(NOT_OK);
   }

   Moving  = 0;
   Resetting = 0;
   SearchingHome = 0;

   if (AD->MechanicalRef == ABSOLUTE && SD->MovementMode != SIMULATION_MODE){
      if (UpdateActuatorLengths() != OK) {
#ifdef DEBUG_LVL3 
   printf("<HEXAPITO> GlobalReset UpdateActLengths returns an error\n");
#endif
         return(NOT_OK);
      }
#ifdef DEBUG_LVL3 
   printf("    updatelengths: OK\n");
#endif

   } else
      switch (reset_mode){
         case SOFT_RESET:

            if (SD->MovementMode == SIMULATION_MODE){
               CopyVector(6, GD->NominalLength, SD->ActuatorLength);
               break;

/* begmod PM */
            } else if (AD->MechanicalRef != NONE) {
                 if (ReadLengthFile(SD->BackupFile, SD->ActuatorLength) == OK)
                     break;
                 else if (ReadLengthFile(SD->TmpBackupFile, SD->ActuatorLength) == OK)
                     break;
                 else
                     return(NOT_OK);
            } else 
          /* if AD->MechanicalRef == NONE SD-> ActuatorLength is setted in MotorInit */ 
                break;

/* endmod PM */

          case HARD_RESET:
             Resetting = 0;
             return(ContinueHardReset());

          default:
             SD->Error = DevErr_InternalBug;
             return(NOT_OK);
      }

   return(ResetEnd());
}

/*************************************************************************
 *
 * Function:    ContinueHomeSearch()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 *************************************************************************/
long  ContinueHomeSearch(type)
short  type;
{
#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> ContinueHomeSearch\n");
#endif

   if (!SearchingHome){
      if (CheckActuators(&GlobalStatus) != OK || GlobalStatus == DEV_FAULT)
         return(NOT_OK);

      switch(AD->MechanicalRef){
         case NONE:
            SD->Error = DevErr_NonCompatibleFunction;
            return(NOT_OK);

         case ABSOLUTE:
            if (UpdateActuatorLengths() != OK)
               return(NOT_OK);
            break;

         case LIMIT:
         case MIDPOINT:
            Moving = 1;
            if(SearchHomeSwitch(&SearchingHome, type) != OK)
               return(NOT_OK);

            DeleteFile(SD->BackupFile);
            DeleteFile(SD->TmpBackupFile);

            break;

         default:
            SD->Error = DevErr_InternalBug;
            return(NOT_OK);
      }

   } else {

      if (SearchHomeSwitch(&SearchingHome, type) != OK)
         return(NOT_OK);

   } 
   if (!SearchingHome){
      Moving = 0;
      switch(type){
         case NORMAL_SEARCH:
/* begmod PM */
 /* Redundant - No search hoem for MechanicalRef = NONE */
            if (AD->MechanicalRef != NONE)
/* endmod PM */

               if(SaveLengthFile(SD->BackupFile, SD->ActuatorLength, NULL) != OK)
                   return(NOT_OK);
            break;

         case RESET_SEARCH:
            break;

         default:
            SD->Error = DevErr_InternalBug;
            return(NOT_OK); 
      }
   }

   return(OK);
}

/*************************************************************************
 *
 * Function:    ContinueHardReset()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 *************************************************************************/
long  ContinueHardReset()
{
   int  i;
   long ret;

#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> ContinueHardReset\n");
#endif

   if(!Resetting){
      Resetting = 1;
      SearchingHome = 0;
      AD->Velocity = 1.;
      if (ContinueHomeSearch(RESET_SEARCH) != OK)
         return(NOT_OK);

   } else {
      if (SearchingHome){
         if (ContinueHomeSearch(RESET_SEARCH) != OK)
            return(NOT_OK);

         if (!SearchingHome){
            Moving = 1;
            for (i = 0; i < 6; i++){
               NextLength[i] = GD->NominalLength[i];
               if (AD->Backlash != 0 &&
                   (NextLength[i] - SD->ActuatorLength[i]) / AD->Backlash < 1)
                  NextLength[i] -= AD->Backlash;
            }
            UpdateDelta();
            ret = MoveActuatorsTo(NextLength);

/*
 * Note VR 15/01/2004 :
 * Sleep one second before reading first position due to error reading
 * VPAP driver after this phase
 * Problem appeared on ID28 after upgrading control workstation to
 * faster CPU and network. Not fully understood.
 */

/* 
 * Note MP 08/03/2004 :
 * Same problem appeared on ID16 but fixed only with a two seconds delay
 */

#ifdef DEBUG_LVL1
            printf("Ok. I am now in the way to nominal !!!!!!!!!!!!!!!!!\n");
            printf("DoooooDoooo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
#endif
            sleep(2);
            return(ret);
         }
      } else {
         if (CheckActuators(&GlobalStatus) == OK){
            switch (GlobalStatus){
               case DEV_MOVING:
                  return(OK);

               case DEV_READY:
                  for (i = 0; i < 6; i++)
                     NextLength[i] = GD->NominalLength[i];
                  
                  for (i = 0; i < 6; i++)
                     if (fabs(SD->ActuatorLength[i] - NextLength[i])
                                                      >= GD->MechResolution){
                        UpdateDelta();
                        return(MoveActuatorsTo(NextLength));
                     }

                  Moving = 0;
                  Resetting = 0;

                  return(ResetEnd());

               case DEV_LIMIT:
                  break;
            }
         }
      }
   }
   return(OK);
}

/*************************************************************************
 *
 * Function:    ResetEnd()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 *************************************************************************/
long  ResetEnd()
{
#ifdef DEBUG_LVL3 
   printf("<HEXAPITO> ResetEnd\n");
#endif

   if (SetLengths(SD->ActuatorLength) != OK)
      return(NOT_OK);

   if (CheckAll(SD->ActuatorLength, SD->Position) != OK)
      return(NOT_OK);

   if (AD->MechanicalRef != NONE)
      if (SaveLengthFile(SD->BackupFile, SD->ActuatorLength, NULL) != OK)
          return(NOT_OK);

   return(OK);
}

/*************************************************************************
 *
 * Function:    SaveLengthFile()
 *
 * Description: 
 * 
 * Variables involved:
 *
 * Error(s):
 *
 *************************************************************************/
long  SaveLengthFile(file, length0, length1)
char		*file;
double	*length0;
double	*length1;
{
     int i;
     time_t        now;
     int           path;

     FILE         *fp;

#ifdef DEBUG_LVL5 
   printf("<HEXAPITO> SaveLengthFile: '%s'\n", file);
#endif

    if (SD->MovementMode == SIMULATION_MODE)
        return(OK);

    /*
     * Check whether or not the file exists.
     */

    if((fp=fopen(file,"a+")) != NULL) /* OK i can open the file */
       fclose(fp);

    else {
       if (file == SD->BackupFile){
          DeleteFile(SD->TmpBackupFile);
          if (SaveLengthFile(SD->TmpBackupFile, length0, length0) != OK)
             return(NOT_OK);
       } else if (file == SD->TmpBackupFile){
          SD->Error = DevErr_FileAlreadyExists;
          return(NOT_OK);
       } else {
          SD->Error = DevErr_InternalBug;
          return(NOT_OK);
       } 
    }

    for (i = 0; i < 6; i++) {
        if (length0[i] == 0) {
#ifdef DEBUG_LVL5
           printf("Saving file with 0 lenghts?  No!\n");
#endif
           return(NOT_OK);
        }
    }

    /*
     * Open file.
     */
    if ((fp= fopen(file,"w")) == NULL) {
        SD->Error = DevErr_OpeningFile;
		return(NOT_OK);
    } 

    fprintf(fp, FH_Header);

/*
    time(&now);
    fprintf(fp, FH_SavedAt);
    fprintf(fp,"%s", ctime(&now));
*/
    fprintf(fp, FH_Description);
    fprintf(fp, "\"%s\"", SD->Description);

    fprintf(fp, FH_Last_Reset);
    if (SD->LastResetMode == SOFT_RESET)
        fprintf(fp, FH_SoftReset);
    else if (SD->LastResetMode == HARD_RESET)
        fprintf(fp, FH_HardReset);

    fprintf(fp, FH_Velocity);
    fprintf(fp, "%5.3f", AD->Velocity);

    if (file == SD->BackupFile){
        fprintf(fp, FH_CurrentLengths);
        for (i = 0; i < 6; i++)
            fprintf(fp, FH_LengthFormat, length0[i]);
    } else if (file == SD->TmpBackupFile){
        fprintf(fp, FH_MovingFrom);
        for (i = 0; i < 6; i++)
            fprintf(fp, FH_LengthFormat, length0[i]);
        fprintf(fp, FH_MovingTo);
        for (i = 0; i < 6; i++)
            fprintf(fp, FH_LengthFormat, length1[i]);
    } 

    fprintf(fp, FH_EndOfFile);
    fclose(fp);

    if (file == SD->BackupFile)
       DeleteFile(SD->TmpBackupFile);
    else
       DeleteFile(SD->BackupFile);

    return(OK);
}

/*****************************************************************************
 *  Function:      ReadLengthFile()
 *
 *  Description:  
 * 
 *  Out:     none
 *
 *  Error code(s): 
 *
 *****************************************************************************/
long ReadLengthFile(file, length)
char		*file;
double	 length[6];
{
    FILE   *fp;
    char    buf[MAX_FILE_LINE_LENGTH + 1];
    char   *line;

    double new_length;
    double var[12];
    int    i = 0;
    int    last_reset = SOFT_RESET;
    float  velocity = 1.;

#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> ReadLengthFile: '%s'\n", file);
#endif
    /*
     * Open file.
     */
     if ((fp= fopen(file,"r")) == NULL) {
         SD->Error = DevErr_OpeningFile;
         return(NOT_OK);
     }

    while ( fgets(line = buf, MAX_FILE_LINE_LENGTH, fp) != NULL )
    {
       while (isspace(*line)) line++;
       if (*line == '\0')
           continue;

       if (strcmp(FH_Last_Reset, line) == 0){
           line += strlen(FH_Last_Reset);
           while (isspace(*line)) line++;
           if (strcmp(line, FH_HardReset) == 0){
               last_reset = SOFT_RESET;
               continue;
           }
       }

       if (strcmp(FH_Velocity, line) == 0){
           line += strlen(FH_Velocity);
           while (isspace(*line)) line++;
           sscanf(line, "%f", &velocity);
           if (velocity < MIN_VELOCITY || velocity > 1)
               velocity = 1.;
       }

       if (line[0] == '#')
           continue;

       if (sscanf(line,"%lf",&new_length) == 1){
            if (i < 12)
                var[i] = new_length;
            else {
                SD->Error = DevErr_FileFormat;
                return(NOT_OK);
            }

#ifdef DEBUG_LVL1
            if (i == 0 && file == SD->BackupFile) printf("Actuator lengths:\n");
            if (i == 0 && file == SD->TmpBackupFile) printf("Moving from:\n");
            if (i == 6 && file == SD->TmpBackupFile) printf("Moving to:\n");
            printf("\t\t(%d) %f\n", i%6 + 1, var[i]);
#endif
            i++;
       }
    }
    fclose(fp);

    if (file == SD->BackupFile){
        if (i != 6) {
            SD->Error = DevErr_FileFormat;
            return (NOT_OK);
        } else {
            CopyVector(6, var, length);
            SD->LastResetMode = last_reset;
            AD->Velocity = velocity;
            return(OK);
        }
    }

    if (file == SD->TmpBackupFile){
        if (i != 12) {
            SD->Error = DevErr_FileFormat;
            return (NOT_OK);
        } else {
            for (i = 0; i < 6; i++){
                 if (fabs(var[i] - var[i + 6]) / 2. > SD->LengthUncertainty){
                     SD->Error = DevErr_UncertaintyTooBig;
                     return (NOT_OK);
                 }
            }
            for (i = 0; i < 6; i++)
                 length[i] = (var[i] + var[i + 6]) / 2.;
            SD->LastResetMode = SOFT_RESET;
            AD->Velocity = velocity;
            return(OK);
        }
    }
    SD->Error = DevErr_InternalBug;
    return(NOT_OK);
}

/*****************************************************************************
 *  Function:      DeleteFile()
 *
 *  Description:  
 * 
 *  Out:     none
 *
 *  Error code(s): 
 *
 *****************************************************************************/
long DeleteFile(file)
char		*file;
{
#ifdef DEBUG_LVL1 
   printf("<HEXAPITO> DeleteFile: '%s'\n", file);
#endif

   if (SD->MovementMode == SIMULATION_MODE)
      return(OK);

   if (unlink(file) == 0)
      return(OK);
   else 
      return(NOT_OK);
}



/*****************************************************************************
 *  Function:      _errmsg()
 *
 *  Description:   emulate OS9 system call under Linux
 * 
 *  Out:     none
 *
 *  Error code(s): 
 *
 *****************************************************************************/
#ifdef linux
int _errmsg(int nerr, char *msg)
{
 printf("hexapito: %s",msg);
 return(nerr);
}
#endif
