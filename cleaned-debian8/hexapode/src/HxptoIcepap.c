/*
 * Includes.
 */
#include <Hexapito.h>
#include <libdeep.h>



/*
 * Definitions
 */
#define __DPRINTF(n, ...) if (ID->debug < (n)); else \
  printf("\t<HXPTO_ICEPAP>: "__VA_ARGS__)

#define DEBUGF_1(...)  __DPRINTF(1, __VA_ARGS__)
#define DEBUGF_2(...)  __DPRINTF(2, __VA_ARGS__)
#define DEBUGF_3(...)  __DPRINTF(3, __VA_ARGS__)
#define DEBUGF_4(...)  __DPRINTF(4, __VA_ARGS__)
#define DEBUGF_5(...)  __DPRINTF(5, __VA_ARGS__)
#define DEBUGF_6(...)  __DPRINTF(6, __VA_ARGS__)
#define DEBUGF_7(...)  __DPRINTF(7, __VA_ARGS__)

#define DEBUGF_ERR(...)        printf("<HXPTO_ICEPAP> ERROR: "__VA_ARGS__)

#define ICEPAP_CMD_MAX         2048

#define ICEPAP_STA_READY       (1<< 9)
#define ICEPAP_STA_MOVING      (1<<10)
#define ICEPAP_STA_SETTLING    (1<<11)
#define ICEPAP_STA_UPLIMIT     (1<<18)
#define ICEPAP_STA_DOWNLIMIT   (1<<19)
#define ICEPAP_STA_POWERON     (1<<23)

/*
#define round_float(x) (long)(((x)-(long)(x) < .5) ? (x) : (x) + 1)
*/
#define round_float(x) (long)((x>=0) ? (x+.5) : (x-.5))


/*
 * Extern. 
 */
extern ActuatorData *AD;
extern SharedData   *SD;



/*
 * Declarations.
 */
long IcepapMotorInit       (motorData *, actuatorParameters *);
long IcepapCheckMotors     (long *,long *,double *);
long IcepapReadState       (int, long *);
long IcepapReadPosition    (int, double *);
long IcepapSetMotors       (double *);
long IcepapMoveMotorsTo    (double *, float *);
long IcepapStopMotors      ();
long IcepapSearchHomeSwitch();


MotorDefinition IcepapDefinition = {
      IcepapMotorInit,
      IcepapCheckMotors,
      IcepapSetMotors,
      IcepapMoveMotorsTo,
      IcepapStopMotors,
      IcepapSearchHomeSwitch,
};


deeperror_t _IcepapCmd     (char *, char **);




/*
 * Global resources
 */
icepapData *ID;
char        icepapCmd[ICEPAP_CMD_MAX];
char       *icepapAns;



/*****************************************************************************
 *  Function:      IcepapMotorInit()
 *
 *  Description:   Initializes actuator dependent stuff.
 *
 *****************************************************************************/
long IcepapMotorInit(data, parameters)
motorData          *data;
actuatorParameters *parameters;
{
  int    i;
  int    ret;
 

 /* 
  *Get access to shared memory filled with configuration from res
  */
  ID        = &data->MotorPrivate.icepap;


 /* 
  * Let's consider that the shared memory has been well filled 
  */
  DEBUGF_2("IcepapMotorInit\n");


 /* 
  * Close any previous connnection to ICEPAP controller 
  */
  if(ID->initialized)
  {
   /* TODO MP 10/07/29: should perhaps power off drivers??? dangerous??? */
   DEBUGF_3("closing a previous connection to \"%s\"\n",ID->hostname);
   deepdev_close(ID->dev);
   ID->initialized = 0;
  }


 /* 
  * Initialize connnection to ICEPAP controller 
  */
  DEBUGF_3("opening connection to: %s\n",ID->hostname);
  /* JM
   * if((ID->dev = deepdev_open(ID->hostname)) == DEEPDEV_BAD_HANDLE) 
   */
   if((ID->dev = deepdev_open(ID->hostname)) == BAD_HANDLE)
  {
    SD->Error = DevErr_OpenIcepapDevice;
    DEBUGF_ERR("unable to open connection to \"%s\"\n", \
      ID->hostname);
    return(NOT_OK);
  }
  deepdev_setparam(ID->dev, "debug", (ID->libdebug<=5?ID->libdebug:5));


 /* 
  * Try to power on ICEPAP drivers
  */
  for(i=0,ret=OK;i<6;i++) 
  {
   snprintf(icepapCmd,ICEPAP_CMD_MAX,"#%d:VELOCITY %d", \
    ID->motor[i].addr,ID->Velocity);
   if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
   {
    DEBUGF_ERR("setting velocity on DRIVER: %d\n",ID->motor[i].addr);
    SD->Error = DevErr_PowerIcepap;
    ret=NOT_OK;
   }

   snprintf(icepapCmd,ICEPAP_CMD_MAX,"#%d:POWER ON",ID->motor[i].addr);
   if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
   {
    DEBUGF_ERR("on DRIVER: %d\n",ID->motor[i].addr);

    /* Add some diagnostic help */
    snprintf(icepapCmd,ICEPAP_CMD_MAX,"%d:?ISG ?PWRINFO",ID->motor[i].addr);
    _IcepapCmd(icepapCmd, &icepapAns);
    printf("%s\n",icepapAns);

    SD->Error = DevErr_PowerIcepap;
    ret=NOT_OK;
   }
  }



 /*
  * Internal initialization
  */
  parameters->MotorCleverSearch = 1;
  if(ID->StepsPerMeter <= 0)
  {
    SD->Error = DevErr_BadActuatorParameter;
    return(NOT_OK);
  } 
  parameters->MotorResolution = 1. / ID->StepsPerMeter;


 /* 
  * Normal end 
  */
  DEBUGF_2("leaving IcepapMotorInit\n");
  ID->initialized = 1;
  return(ret);
}




/*****************************************************************************
 *
 *
 */
deeperror_t  _IcepapCmd(char *cmd, char **answ)
{
 deeperror_t  ret;


 DEBUGF_4("===> %s\n", cmd);
 /* JM
  * ret = deepdev_cmd(ID->dev, cmd, answ);
  */
 ret = deepdev_query(ID->dev, cmd, answ);
 
 switch(ret) {
     case DEEPDEV_OK:
       break;
     case DEEPDEV_ERR:
       DEBUGF_ERR("from library: ");
       break;
     case DEEPDEV_COMMERR:
       DEBUGF_ERR("on communication: ");
       break;
     case DEEPDEV_ANSERR:
       DEBUGF_ERR("from device: ");
       break;
     default:
       DEBUGF_ERR("unknown:");
 }
 if(strlen(*answ)) 
 {
   if(ret == DEEPDEV_OK)
     DEBUGF_4("<=== %s\n", *answ);
   else
     printf("%s\n\n\n", *answ);
 }

 return(ret);
}




/*****************************************************************************
 *  Function:      IcepapCheckMotors()
 *
 *  Description:
 *
 *         In:     .
 *
 *        Out:     none.
 *
 *     Remarks:    An DEV_FAULT condition sets the the SD->Error variable
 *                 even although it returns OK. This lets the calling
 *                 function go on checking the six channels.
 *
 *****************************************************************************/
long IcepapCheckMotors(global_status, status, length)
long  *global_status;
long   status[6];
double length[6];
{
 int  i;
 DEBUGF_2("IcepapCheckMotors\n");

 *global_status = 0;

 for(i = 0; i < 6; i++)
 {
   if(IcepapReadState(i, status + i) != OK)
   {
     *global_status = DEV_FAULT;
     return(NOT_OK);
   }

   *global_status |= status[i];

   if(IcepapReadPosition(i, length + i) != OK)
   {
     *global_status = DEV_FAULT;
     return(NOT_OK);
   }
 } /* end of for(i) */

 return(OK);
}






/*****************************************************************************
 *  Function:      IcepapReadState()
 *
 *  Description:   Read status word from vpap.
 *
 *         In:     Index number for leg to be check.
 *
 *        Out:     none.
 *
 *     Remarks:    An DEV_FAULT condition sets the the SD->Error variable
 *                 even although it returns OK. This lets the calling 
 *                 function go on checking the six channels.
 *
 *****************************************************************************/
long IcepapReadState(i, state)
int   i;
long *state;
{
  long status;

  DEBUGF_2("IcepapReadState(%d)\n",i);


 /*
  * Get status from device
  */
  snprintf(icepapCmd,ICEPAP_CMD_MAX,"?_FSTATUS %d",ID->motor[i].addr);
  if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
  {
    *state = DEV_FAULT;
    SD->Error = DevErr_ReadIcepap;
    return (NOT_OK);
  }
  
  if(sscanf(icepapAns,"%x",&status) != 1)
  {
    *state = DEV_FAULT;
    SD->Error = DevErr_ReadIcepap;
    return (NOT_OK);
  }

  DEBUGF_3("leg %d state: 0x%08X\n", i, status);
  *state = 0;



 /*
  * Get limitswitches status
  */
  if((status & ICEPAP_STA_UPLIMIT) && (status & ICEPAP_STA_UPLIMIT))
  {
    *state |= DEV_FAULT;
    SD->Error = DevErr_LimitSwitchDisfunction;
  }

  if(status & ICEPAP_STA_UPLIMIT)
    *state |= DEV_UP_LIMIT;

  if(status & ICEPAP_STA_DOWNLIMIT)
    *state |= DEV_DOWN_LIMIT;


 /*
  * Get moving status
  * NOTE MP 070610: check MOVING and SETTLING status bits
  *   -the MOVING   (10) goes down at the end of the trajectory
  *   -the SETTLING (11) goes down at the end of close loop algorythm
  *
  * NOTE MP 210610: during a HOMING sequence, several motions take place,
  * therefore the MOVING/SETTLING pair can be unset. But the READY remains
  * unset during all the HOMING sequence. To distinguish a non READY axis
  * due to error condition from moving condition, the POWER bit
  * must also be checked
  *   -the READY    (09) goes down on error or moving
  *   -the POWERON  (23) goes down on error
  */
  if((status & ICEPAP_STA_MOVING) || (status & ICEPAP_STA_SETTLING)) 
    *state |= DEV_MOVING;
  else if(!(status & ICEPAP_STA_READY) && (status & ICEPAP_STA_POWERON)) 
    *state |= DEV_MOVING;

  if((status & ICEPAP_STA_READY) && !(*state & DEV_MOVING)) 
    *state |= DEV_READY;




 /*
  * Get home searching status from device
  * NOTE MP 200710: the ?HOMESTAT can not be done here, because 
  * the FOUND condition is not erased by DSP until next homing request.
  * Therefore the ?HOMESTAT must be done only in IcepapSearchHomeSwitch()
  *
  *
  snprintf(icepapCmd,ICEPAP_CMD_MAX,"%d:?HOMESTAT",ID->motor[i].addr);
  if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
  {
    *state = DEV_FAULT;
    SD->Error = DevErr_ReadIcepap;
    return (NOT_OK);
  }
  DEBUGF_3("leg %d home : %s\n", i, icepapAns);
  
  if(!strncmp(icepapAns,"MOVING",strlen("MOVING")))
    *state |= DEV_SEARCHING;

  if(!strncmp(icepapAns,"FOUND",strlen("FOUND")))
    *state |= DEV_HOME;
  */





 /*
  * Blabla
  */
  DEBUGF_3("leg %d state: ", i);
  if(ID->debug >= 3)
  {
   if(*state & DEV_READY)      printf("READY ");
   if(*state & DEV_MOVING)     printf("MOVING ");
   if(*state & DEV_HOME)       printf("HOME ");
   if(*state & DEV_SEARCHING)  printf("SEARCHING ");
   if(*state & DEV_UP_LIMIT)   printf("UP_LIMIT ");
   if(*state & DEV_DOWN_LIMIT) printf("DOWN_LIMIT ");
   printf("\n");
  }


  return (OK);
}






/*****************************************************************************
 *  Function:      IcepapReadPosition()
 *
 *  Description:   Read positions
 *
 *         In:     Index number for leg to be read.
 *
 *        Out:     none.
 *
 *****************************************************************************/
long IcepapReadPosition(i, length)
int     i;
double *length;
{
  long   pos;

  DEBUGF_2("IcepapReadPosition(%d)\n",i);


 /*
  * get position from device
  */
  snprintf(icepapCmd,ICEPAP_CMD_MAX,"?_FPOS %d",ID->motor[i].addr);
  if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
  {
    SD->Error = DevErr_ReadIcepap;
    return (NOT_OK);
  }
  if(sscanf(icepapAns,"%ld",&pos) != 1)
  {
    SD->Error = DevErr_ReadIcepap;
    return (NOT_OK);
  }
  DEBUGF_3("leg %d pos  : %ld(stp)\n",i,pos);



 /*
  * convert position from steps
  */
  *length =  pos / ID->StepsPerMeter;
  DEBUGF_3("leg %d pos  : %f(m)\n",i,*length);



  return (OK);
}






/*****************************************************************************
 *
 *  Function:      IcepapSetMotors()
 *
 *  Description:   Set the lengths of the VPAP controllers
 *                 at a certain value.
 *
 *         In:     New values.
 *
 *****************************************************************************/
long IcepapSetMotors(Length)
double  Length[6];
{
  int    i;
  long   steps;

  DEBUGF_2("IcepapSetMotors\n");

  for(i=0;i<6;i++) 
  {
    /* Convert length from meters to steps */
    steps =  round_float(Length[i] * ID->StepsPerMeter);
    DEBUGF_3("leg %d set to %ld(stp)\n", i, steps);

    /* Force the current position */
    snprintf(icepapCmd,ICEPAP_CMD_MAX,"#%d:POS %ld",ID->motor[i].addr, steps);
    if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
    {
      SD->Error = DevErr_WriteIcepap;
      return (NOT_OK);
    }
  }


  return (OK);
}





/*****************************************************************************
 *  Function:      IcepapMoveMotorsTo()
 *
 *  Description:   Move one leg to a certain position (VPAP).
 *
 *         In:     New lengths in meters.
 *
 *        Out:     none.
 *
 *****************************************************************************/
long IcepapMoveMotorsTo(length, relspeed)
double  length[6];
float   relspeed[6];
{
  int    i;
  int    n;
  long   steps;


  /* 
   * TODO MP 300710: what do we do with speed parameter? 
   * Let's trust icepapcms for the moment
   */


  DEBUGF_2("IcepapMoveMotorsTo\n");
  /* Prepare group motion: TODO MP 11/05/12: we should check that
     the group motions by the current DSP firmware release >= 1.22
     but today all the installed hexapods are with such firmware */
  snprintf(icepapCmd,ICEPAP_CMD_MAX,"#MOVE GROUP");
  for(i=0;i<6;i++) 
  {

    /* Convert length from meters to steps */
    steps =  round_float(length[i] * ID->StepsPerMeter);
    DEBUGF_3("leg %d move to %ld(stp)\n", i, steps);

    /* Prepare absolute movements */
    n = strlen(icepapCmd);
    snprintf(icepapCmd+n,ICEPAP_CMD_MAX-n," %d %ld", \
      ID->motor[i].addr, steps);
  }
  DEBUGF_3("command sent: \"%s\"\n", icepapCmd);

  /* Launch absolute movements */
  if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
  {
    /* In case of trouble cancel all pending movements */
    IcepapStopMotors();
    SD->Error = DevErr_WriteIcepap;
    return (NOT_OK);
  }

  return (OK);
}





/*****************************************************************************
 *  Function:      IcepapStopMotors()
 *
 *  Description:   Inmediately stops the movement of the six legs (VPAP).
 *
 *         In:     none.
 *
 *        Out:     none.
 *
 *****************************************************************************/
long IcepapStopMotors()
{
  int    i;


  DEBUGF_2("IcepapStopMotors\n");
  for(i=0;i<6;i++) 
  {
    DEBUGF_3("leg %d stopped\n",i);

    snprintf(icepapCmd,ICEPAP_CMD_MAX,"#%d:STOP",ID->motor[i].addr);
    if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
    {
      SD->Error = DevErr_WriteIcepap;
      return (NOT_OK);
    }
  }

  return (OK);
}




/*****************************************************************************
 *  Function:      IcepapSearchHomeSwitch()
 *
 *  Description:
 *
 *         In:     i:
 *
 *        Out:     none
 *
 *****************************************************************************/
long IcepapSearchHomeSwitch(searching, type, backlash)
short  *searching;
short   type;
{
  int           i;
  static short  phase;
  float         _delta;


  DEBUGF_2("IcepapSearchHomeSwitch\n");
  DEBUGF_2("searching: %d\n",*searching);
  DEBUGF_2("phase    : %d\n", phase);




 /*
  * Init sequencer
  */
  if (!*searching)
  {
    *searching = 1;
    phase      = 0;
  }




 /*
  * Sequencer
  */
  switch(phase)
  {


   /*
    * Launch homing movements to lim-
    */
    case 0:
      for(i=0;i<6;i++) 
      {
        /* 
         * NOTE MP 210912: by default "home" source is configured as "lim-"
         *                 therefore the command to send is "home -1"
         *                 But new Symetrie hexapods come with level home
         *                 switches. Then the "home" source is "home"
         *                 and the search direction must have been set
         *                 correctly to allow icepap driver to know by
         *                 itself in which direction the motar has to be moved
         *                 Therefore the command to send is "home 0" 
         */
        snprintf(icepapCmd,ICEPAP_CMD_MAX,"#%d:HOME %d",
          ID->motor[i].addr,
          (AD->MechanicalRef==MIDPOINT)?0:-1);
        if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
        {
          SD->Error = DevErr_WriteIcepap;
          return (NOT_OK);
        }
      }
      phase = 1;
      break;



   /*
    * Wait for the limitswitch activation
    */
    case 1:
      /*  Get home searching status from device */
      for(i=0;i<6;i++) 
      {
        snprintf(icepapCmd,ICEPAP_CMD_MAX,"%d:?HOMESTAT",ID->motor[i].addr);
        if(_IcepapCmd(icepapCmd, &icepapAns) != DEEPDEV_OK)
        {
         SD->Error = DevErr_ReadIcepap;
         return (NOT_OK);
        }
        DEBUGF_3("leg %d home : %s\n", i, icepapAns);
  
        if(!strncmp(icepapAns,"MOVING",strlen("MOVING")))
          SD->ActuatorStatus[i] |= DEV_SEARCHING;
      }


      /* Cancel next step in sequencer */
      phase = 2;
      for (i = 0; i < 6; i++)
        if (SD->ActuatorStatus[i] & DEV_SEARCHING)
          phase = 1;
      break;


   /*
    * Move back from the limitswitch
    */
    case 2:
      phase = 3;
      break;


   /*
    * Wait for the end of motions
    */
    case 3:
      phase = 4;
      break;


   /*
    * Search down the limitswitch
    */
    case 4:
      phase = 5;
      break;



   /*
    * Wait for the limitswitch activation
    */
    case 5:
      phase = 6;
      break;



   /*
    * End of homing sequence
    */
    case 6:
      *searching = 0;
      phase      = 0;

      /* TODO MP 300710: could use ?HOMEPOS to be more accurate */
      _delta     = 0.0;
       
      /* NOTE MP 300710: don't understand what this for is */
      for (i = 0; i < 6; i++)
        AD->HomeLengthFound[i] = SD->ActuatorLength[i] - _delta;

      /* Force the current leg position do home postion from res */
      if (type == RESET_SEARCH){
        for (i = 0; i < 6; i++)
          SD->ActuatorLength[i] = AD->HomeLength[i] + _delta;

        return(IcepapSetMotors(SD->ActuatorLength));
      }

      break;

  } /* end of switch(phase) */



  return (OK);
}
