
/***************************************************************
#
#	File:	hxfeedback.c
#
#	Project: Hexapode Device Server
#
#	Description: Feedback process for hydraulic Hexapodes
#
#	Author(s): Paolo Mangiagalli
#
#	Original : July 1998
#
#	Revision:
#
#	Copyright (c) year European Synchrotron Radiation Facility
#		Grenoble, France
#
 * Version: $Revision: 2.3 $
 * by:      $Author: perez $
 * date:    $Date: 2012/09/24 09:22:53 $
 *
 *          $Log: hxfeedback.c,v $
 *          Revision 2.3  2012/09/24 09:22:53  perez
 *          Add support of MIDPOINT reference for ICEPAP
 *
 *          Revision 2.2  2011/05/23 07:59:15  perez
 *          Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 *          Revision 2.1  2011/03/28 09:04:33  perez
 *          Add ICEPAPvelocity resource
 *
 *          Revision 2.0  2010/09/06 07:44:12  perez
 *          Add ICEPAP motor support
 *
 *          Revision 1.37  2010/06/22 13:04:50  perez
 *          Fix bug of ch numbering + high debug
 *
 *          Revision 1.36  2010/06/17 06:53:01  perez
 *          Better fix bug of channels not as first six ones
 *
 *          Revision 1.35  2009/06/18 11:51:22  perez
 *          Fix bug of channels not as first six ones
 *
 *          Revision 1.34  2009/05/25 12:37:36  perez
 *          Fix bug in VPAP retries to avoid slowing down DS
 *
 *          Revision 1.31  2008/01/17 10:17:25  rey
 *          Fix bug with usleep
 *
 *          Revision 1.30  2006/05/22 14:50:43  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.29  2006/05/22 14:47:26  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.28  2006/05/22 14:41:40  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.27  2006/05/22 14:39:50  rey
 *          Bug when killing hexapode (hexapito not dying) solved
 *
 *          Revision 1.23  2006/03/16 14:16:04  rey
 *          Bug with CheckAll return missing solved
 *
 * Revision 1.22  2004/03/09  16:29:33  16:29:33  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:42  16:36:42  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 *          Revision 1.0  1999/05/12 06:47:48  dserver
 *          Locked by PM
 *
***************************************************************/
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sched.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdlib.h>
#include "MessageHandling.h"
#include "SignalHandling.h"


/*#define PRINT_TO_FILE 1*/
#define DEBUG_LEVEL1 1
#define DEBUG_LEVEL2 2 

#define OK 0
#define NOT_OK -1


#define OUT_OF_RANGE 0          /* last x out of range */
#define CONTROL_RANGE_1 1       /* last x in control range but everything else out */
#define CONTROL_RANGE_2 2       /* last x in stable range */
#define CONTROL_RANGE_3 3       /* average x in stable range */
#define STABLE_RANGE 5          /* average+sigma in stable range */

#define TRAVEL_SPEED .00009

#define DMM16_BASE 0x300        /* address of dmm16 card */
#define OMM48_BASE 0x350        /* address of omm48 card */

#define LOOP_PER_USEC 28 /* calibrated for medea3 486 */
#define SLEEP1 20        /* time in usec */
#define SLEEP2 150
#define LOOP_PERIOD 1    /* time in second between readings */ 

#define DMM16_COUNTS 50      /* number of readings for each leg reading */

#define AVRG_POINTS 10          /* number of points for position array */
#define CONTROL_RANGE_WIDTH 500e-6  /* half width of control range (m) */
#define STABLE_RANGE_WIDTH 15e-6    /* half width of stable range (m) */

#define ALL_LEGS -1

int acs,avs,avss;


struct leg_data {
  int status;                   /* leg status */
  int control;                  /* injectors working flag */  
  double setpoint;              /* actual setpoint (m) */
  double offset;                /* actual offset (m) `*/
  int actual;                   /* index of actual reading in lengths array */
  double oldtime;              /* relative time of last reading in seconds */
  double speed;                 /* d_length / d_time for last reading (m/s) */
  double dspeed;                /* difference between actual and last speed reading */
  double position[AVRG_POINTS]; /* buffer for last AVRG_POINTS readings */
  double average;               /* average of last AVRG_POINTS readings */
  double sigma;                 /* standard deviation of last AVRG_POINTS */
  double reference;             /* reference position (m) for LVDT = -5 V */
  double adc2length;            /* conversion factor 1 adc bit -> leg displ (m) */
  double length2dac;
  int reference_advalue;
};

struct leg_data leg[6];

int injectors_register;        /* mask used for block injectors */
int injectors_control;          /* INJECTOR_ON -> deblock injector if out of range */


int hxfeedback;             /* main loop flag */
double actual_position[6];      /* last position */
double actual_time;             /* time of last reading */
double loop_time;               /* time of last main loop iteration */
double actual_speed;
int hxpd_command;




key_t key;
int   qid,
  msgsz;
struct mymsgbuf qbuf;

int is_in_control_range( double setpoint, double position, double sigma );
int is_in_stable_range(double setpoint, double position, double sigma );
void avrg_and_stddev(struct leg_data *leg);
void dmm16_scan(int counts,long *values);
int dmm16_init();
int omm48_write(int address, int value);
int omm48_init();
int read_lengths( double *actual_positions);
void myusleep(int usec);
void calculate_offset(struct leg_data *leg);
void update_leg_value(int leg ,struct leg_data * data);
void switch_injectors(int leg ,int control);
void init_injectors();
void reset_position_array(struct leg_data *data);


void     sigQuit();

/*****************************************************************************
 *
 *  Functions:     sigKill(), sigRequest(), sigExecute(), sigCompleted()
 *
 *  Description:   signal handlers for subprocess handshaking
 * 
 *  Error code(s): 
 *
 *****************************************************************************/
void sigQuit()
{
  printf("\t<hxfeedback> Kill signal received\n");
  hxfeedback = 0;
}


/***************************************************************
#
#	Function: update_leg_value
#
#	Description: Calculate the DAC value for the new position
#                    and call set_length for writing into the hexacontrolleer
#
#       Arg(s) In:  Leg number, leg structure data    
#
#	Arg(s) Out:
#
#	Return: -
#
#***************************************************************/

void update_leg_value(int leg ,struct leg_data *data) {
  double expected_position;
  long ad_value;

  expected_position = data->setpoint + data->offset;
  ad_value = (expected_position - data->reference) / data->length2dac;
  if  ( ad_value < 0 )
    ad_value = 0;
  if (ad_value > 65535)
    ad_value = 65535;

#ifdef DEBUG_LEVEL1
  printf("\t<update_leg_value> Leg %i Pos: %.2f um adc = %d\n",leg+1,expected_position,ad_value);
#endif

  omm48_write(leg+1,ad_value); 
  myusleep(SLEEP2);
  omm48_write(0,0);
  
  
  return;
}




/***************************************************************
#
#	Function: switch_injectors
#
#	Description: switch injectors on or off
#
#	Arg(s) In: int leg 
#                    -> (0-5) for a single leg
#                    ->  -1 all legs
#                  int control -> INJECTORS_OFF || INJECTORS_ON
#	                               
#
#	Arg(s) Out: leg[i].offset is updated
#
#	Return: -
#
#***************************************************************/

void switch_injectors(int leg, int control) {
  int bit;
  if  ( (leg < -1) || (leg > 5)) {
#ifdef DEBUG_LEVEL2
    printf("\t<switch_injectors>Wrong Leg Value\n" );
#endif
    return;
  }
  if (leg == -1) {
    injectors_register = (control == INJECTORS_ON) ? 63 : 0;
#ifdef DEBUG_LEVEL2
  printf("\t<switch_injectors> All Leg %s mask = %d\n",(control == INJECTORS_ON) ? "ON" : "OFF" ,injectors_register);
#endif
  }
  else {
    bit = (1 << (5-leg));
    if (control == INJECTORS_ON) {
      injectors_register |= bit;
    }
    else {
      injectors_register &= ~bit;
    }
#ifdef DEBUG_LEVEL2
  printf("\t<switch_injectors> Leg %i %s mask = %d\n",leg+1,(control == INJECTORS_ON) ? "ON" : "OFF",injectors_register );
#endif
  }

  omm48_write(7,injectors_register);
  myusleep(SLEEP2);
  omm48_write(0,0);
  return;
}

/***************************************************************
#
#	Function: read_lengths
#
#	Description: read lengths from dmm16

#
#	Arg(s) In:
#	                               
#
#	Arg(s) Out: double * actual_position
#
#	Return: -
#
#***************************************************************/

int read_lengths( double *actual_positions) {
  long ad_values[6];
  int i;

  dmm16_scan(DMM16_COUNTS,&ad_values[0]); /* pay attention -> ad_values[0] = leg 6 */
  for(i=0;i<6;i++) {
    *(actual_position + i) = (double) ( ((-1) * ad_values[5-i] + leg[i].reference_advalue) * leg[i].adc2length ) + leg[i].reference; 

#ifdef DEBUG_LEVEL2
    printf("\t<read_length> Leg %d = %2.3f mm %d adc\n",i+1, *(actual_position + i) * 1e3, ad_values[5-i]);
#endif
  }
}




/***************************************************************
#
#	Function: dmm16_scan
#
#	Description: perform n scans across channels 1-6, assumes that 
#                    the card has been setted for channel, range , etc..                
#
#	Arg(s) In:  int counts -  number of scans
#	            unsigned long *values - pointer to a 6
#                          unsigned long values array
#
#	Arg(s) Out  values[6}  average values
#                   
#	Return: -
#
#***************************************************************/

void dmm16_scan(int counts, long *values){
  int lsb,msb;
  int i,
      j,
      loop=1000000;

  short int advalue;
 
  outb(97, DMM16_BASE + 2);    /* set A/D channel range 1-6 */
  myusleep(SLEEP2);
  outb(8, DMM16_BASE +11);     /* set bipolar input +/-10V , gain 1 */
  myusleep(SLEEP2);
  for(j=0;j<6;j++)
    *(values+j) = 0;
  
  for (i=0;i<counts;i++) {
    for(j=0;j<6;j++) {
      outb(1 , DMM16_BASE);        /* start A/D conversion */
      myusleep(SLEEP1);
      while ( loop && (inb(DMM16_BASE + 8) & 128) ) {  /* polling on A/D busy flag 1000 times */
	loop--;
	myusleep(SLEEP1);
      }
      msb = inb(DMM16_BASE + 1);
      myusleep(SLEEP2);
      lsb = inb(DMM16_BASE);
      myusleep(SLEEP2);
      advalue = msb*256+lsb;      /* store the A/D value in a 16 bit signed integer */
      *(values+j) += advalue;     /* add it in a long signed integer */
    }
  }
  for(j=0;j<6;j++) 
    *(values+j) /= counts;	  /* average the final value over the readings */
}

/***************************************************************
#
#	Function: dmm16_init
#
#	Description: hardware init of dmm16
#                                    
#
#	Arg(s) In: 
#	           
#                        
#
#
#	Arg(s) Out: 
#                   
#	Return: - OK if succesful
#
#***************************************************************/

int dmm16_init(){

#ifdef DEBUG_LEVEL1
  printf("\t<dmm16_init>\n");
#endif

  if (ioperm(DMM16_BASE,16,1)){             /* give read/write permissions */
    printf("\t<dmm16_init> Cannot ioperm \n");
    return(NOT_OK);
  }
  
  outb(0, DMM16_BASE + 2);    /* write channel register test */
  myusleep(10);               /* wait about 10 us */
  if (inb(DMM16_BASE + 2) != 0) {
    printf("\t<dmm16_init> Readback channel register failed !\n");
    return(NOT_OK);
  }
  myusleep(SLEEP1);

  outb(255, DMM16_BASE + 2);    /* write channel register test  */
  myusleep(SLEEP2);                 /* wait about 10 us */
  if (inb(DMM16_BASE + 2) != 255) {
    printf("\t<dmm16_init> Readback channel register failed !\n");
    return(NOT_OK);
  }

  outb(8, DMM16_BASE +11);     /* set bipolar input +/-10V , gain 1 */
  myusleep(SLEEP2);
  return(OK);
}


/***************************************************************
#
#	Function: omm48_init
#
#	Description: hardware init of omm48 digital I/O card
#                                    
#
#	Arg(s) In: 
#	           
#	Arg(s) Out: 
#                   
#	Return: - OK if succesful
#
#***************************************************************/

int omm48_init(){


#ifdef DEBUG_LEVEL1
  printf("\t<omm48_init>\n");
#endif

  if (ioperm(OMM48_BASE,16,1)){             /* give read/write permissions */
    printf("\t<omm48_init> Cannot ioperm \n");
    return(NOT_OK);
  }
  
  outb(0x80, OMM48_BASE + 3);    /* set port 1A,1B,1C as output, logic low */
  myusleep(10);                  /* wait about 10 us */
  outb(0x80, OMM48_BASE + 7);    /* set port 2A,2B,2C as output, logic low */
  myusleep(10); 

  if (inb(OMM48_BASE + 3) != 0x80) {
    printf("\t<omm48_init> Readback channel register failed !\n");
    return(NOT_OK);
  }
  return(OK);
}


/***************************************************************
#
#	Function: omm48_write
#
#	Description: write the 24 bit control word to the dac
#                                    
#
#	Arg(s) In: address (port 1A,2A)
#                  value   (LSB  1C,2C; MSB 1B,2B) 
#	Arg(s) Out: 
#                   
#	Return: - OK if succesful
#
#***************************************************************/

int omm48_write(int address, int value) {

    int port_a,port_b,port_c;

#ifdef DEBUG_LEVEL2
    printf("\t<omm_write> address %d  value %d\n",address, value);
#endif

    if ( address < 0  || address > 255 ) {
      printf("\t<omm_write> Wrong address \n");
      return(NOT_OK);
    }
    if ( value < 0  || value > 65535 ) {
      printf("\t<omm_write> Wrong value \n");
      return(NOT_OK);
    }
   
    port_a = address;
    port_c = (int) value / 256;
    port_b = (int) (value - 256 * port_c);
    outb(port_a,OMM48_BASE);
    myusleep(5);
    outb(port_a,OMM48_BASE + 4);
    myusleep(5);
    outb(port_c,OMM48_BASE + 1);
    myusleep(5);
    outb(port_c,OMM48_BASE + 5);
    myusleep(5);
    outb(port_b,OMM48_BASE + 2);
    myusleep(5);
    outb(port_b,OMM48_BASE + 6);
    myusleep(5);
    
    return(OK);
}

/***************************************************************
#
#	Function: is_in_control_range
#
#	Description: Check if a position is in the Control Range
#                    defined as SETPOINT +- CONTROL_RANGE_WIDTH
#

#	Arg(s) In:  double setpoint
#	            double position
#                   double sigma
#
#	Arg(s) Out: 1 if in range, 0 if not
#
#	Return: -
#
#***************************************************************/

int is_in_control_range( double setpoint, double position, double sigma ) {
  if ( position + sigma > setpoint + CONTROL_RANGE_WIDTH ) return (0);
  if ( position - sigma < setpoint - CONTROL_RANGE_WIDTH ) return (0);
  return(1);
}

/***************************************************************
#
#	Function: is_in_sample_range
#
#	Description: Check if a position is in the Sample Range
#                    defined as SETPOINT +- SAMPLE_RANGE_WIDTH
#
#	Arg(s) In:  double setpoint
#	            double position
#                   double sigma
#
#	Arg(s) Out: 1 if in range, 0 if not
#
#	Return: -
#
#***************************************************************/


int is_in_stable_range( double setpoint, double position, double sigma ) {
  if ( position + sigma > setpoint + STABLE_RANGE_WIDTH ) return (0);
  if ( position - sigma < setpoint - STABLE_RANGE_WIDTH ) return (0);
  return(1);
}

/***************************************************************
#
#	Function: avrg_and_stddev
#
#	Description: Calculate average and standard deviation of
#                     the last AVRG_POINTS positions                   
#
#	Arg(s) In:  struct leg_data * leg
#	            
#
#	Arg(s) Out  leg.average
#                   leg.sigma
#	Return: -
#
#***************************************************************/


void avrg_and_stddev(struct leg_data *leg) {  
  int i,j;
  double average=0;
  double standard_deviation=0;
  

  for (i=0; i<AVRG_POINTS; i++) {
    average += leg->position[i];
    standard_deviation += pow(leg->position[i],2);
  }
  leg->average = average / AVRG_POINTS;
  leg->sigma = sqrt( (standard_deviation - AVRG_POINTS * pow(leg->average,2) ) / ( AVRG_POINTS - 1 ) );
}

/***************************************************************
#
#	Function: calculate_offset
#
#	Description: calculate_offste from leg data
#
#	Arg(s) In:
#	                               
#
#	Arg(s) Out: leg[i].offset is updated
#
#	Return: -
#
#***************************************************************/

void calculate_offset( struct leg_data *leg) {
  leg->offset = 0;
}
/***************************************************************
#
#	Function: reset_position_array
#
#	Description: reinitialize the array of previous readings 
#
#
#	Arg(s) In: 
#	        
#
#	Arg(s) Out:
#
#	Return: -
#
#***************************************************************/

void reset_position_array(struct leg_data *data) {
  int j;
  data->position[0] = data->position[data->actual];
  data->actual = 0;
  for(j=1;j<AVRG_POINTS;j++)
    data->position[j]=-9999999.0;
}
      
/***************************************************************
#
#	Function: myusleep
#
#	Description: sleep for a short time with a dummy for loop
#         it assumes that the variable LOOP_PER_USEC has been
#         calibrate on the machine.
#
#	Arg(s) In:  int usec 
#	            
#                   
#
#	Arg(s) Out: 
#
#	Return: -
#
#***************************************************************/

void myusleep(int usec){
  int i;
for(i=0;i< usec*LOOP_PER_USEC; i++);
}



int main(int argc, char** argv) {

  int i,j;
  FILE *fp;


  s_install(SIGQUIT,sigQuit); /* trap quit & term signal */
  s_install(SIGTERM,sigQuit);
  s_install(SIGINT,sigQuit);

  /* program init */
  if (dmm16_init() != OK) {
    printf("\t<hxfeedback> dmm16_init failed\n");
    exit(-1);
  }
  if (omm48_init() != OK) {
    printf("\t<hxfeedback> omm48 init failed\n");
    exit(-1);
  }

  msgsz = sizeof(struct mymsgbuf) - sizeof(long);  
  /* Create unique key via call to ftok() if key is not passed as argument */
  if(argc == 2) {
    sscanf(argv[1],"%d",&key);
  }
  else {
    key = ftok(".", 'm');
  }

  /* Open the queue - create if necessary */
  if((qid = msgget(key, IPC_CREAT|0660)) == -1) {
		printf("\t<hxfeedback> Message queue open failed\n");
		exit(-1);
  }

  printf("\t<hxfeedback> Init OK !\n");
  
  actual_time = ( clock() / (double) CLOCKS_PER_SEC);

  for(i=0;i<6;i++) { /* initialize LVDT parameters */
    leg[i].reference = .439;
  }   
  
  leg[0].length2dac = .37654e-6;
  leg[1].length2dac = .3389e-6;
  leg[2].length2dac = .3490229e-6;  
  leg[3].length2dac = .2502e-6;
  leg[4].length2dac = .2834785e-6;
  leg[5].length2dac = .312999e-6;
  leg[0].adc2length = .75213e-6;
  leg[1].adc2length = .67604e-6;
  leg[2].adc2length = .699027e-6;
  leg[3].adc2length = .5013e-6;
  leg[4].adc2length = .568938e-6;
  leg[5].adc2length = .623877e-6;
  leg[0].reference_advalue = 17;
  leg[1].reference_advalue = 60;
  leg[2].reference_advalue = 31;
  leg[3].reference_advalue = 40;
  leg[4].reference_advalue = 43;
  leg[5].reference_advalue = 89;

  read_lengths(actual_position);

  for(i=0;i<6;i++) {
    leg[i].setpoint = actual_position[i];
    leg[i].actual = 1;
    leg[i].oldtime = actual_time;
    leg[i].speed=0;
    reset_position_array(&leg[i]);
    switch_injectors(i,INJECTORS_OFF);
  }    
 
  hxfeedback = 1;
  injectors_control = INJECTORS_OFF;

#ifdef PRINT_TO_FILE
  fp=fopen("/tmp/pos.out","w");
#endif  

  /* hxfeedback loop */
  while(hxfeedback) {
    
    /* network message code */

    /* read status command ? */
    if( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , STATUS_READ, IPC_NOWAIT) != -1 ) {
#ifdef DEBUG_LEVEL1
      printf("\t<hxfeedback> Status read message received\n");
#endif
      hxpd_command = STATUS_READ;    
    }  
         /* write position command  ? */
    else if( msgrcv(qid,(struct msgbuf *)&qbuf , msgsz, POSITION_WRITE, IPC_NOWAIT) != -1 ) {
#ifdef DEBUG_LEVEL1      
      printf("\t<hxfeedback> Position write message received\n");
#endif      
      for(i=0;i<6;i++) {
#ifdef DEBUG_LEVEL1    
	printf("\t<hxefeedback> new position[%i] = %g\n",i+1,qbuf.value[i]);
#endif      
	leg[i].setpoint = qbuf.value[i];
	leg[i].offset = 0;
	update_leg_value(i,&leg[i]);
      }
      hxpd_command = POSITION_WRITE;

      qbuf.mtype = MESSAGE_ACK; /* send message acknowledgement */
      if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0) == -1 ) 
	printf("\t<hxfeedback> msgsnd failed\n");    
    }

    /* block/unblock injectors command ? */
    else if( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , INJECTORS_CONTROL, IPC_NOWAIT) != -1 ) {      
#ifdef DEBUG_LEVEL1
      printf("\t<hxfeedback> Injector control message received. Control %d\n",qbuf.control);
#endif
      injectors_control = qbuf.control;
      switch_injectors(ALL_LEGS,injectors_control); 
      hxpd_command = INJECTORS_CONTROL;
      
      qbuf.mtype = MESSAGE_ACK; /* send message acknowledgement */
      if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0) == -1 ) 
	printf("\t<hxfeedback> msgsnd failed\n");    
    }  
    
    /* Set adc2length  command ? */
    else if( msgrcv(qid,(struct msgbuf *)&qbuf , msgsz, ADC2LENGTH, IPC_NOWAIT) != -1 ) {
#ifdef DEBUG_LEVEL1      
      printf("\t<hxfeedback> set adc2length message received\n");
#endif      
      for(i=0;i<6;i++) {
	leg[i].adc2length = qbuf.value[i];
	leg[i].offset = 0;
	reset_position_array(&leg[i]);
      }
      hxpd_command = ADC2LENGTH;

      qbuf.mtype = MESSAGE_ACK; /* send message acknowledgement */
      if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0) == -1 ) 
	printf("\t<hxfeedback> msgsnd failed\n");    
    }

    /* Set reference_advalue  command ? */
    else if( msgrcv(qid,(struct msgbuf *)&qbuf , msgsz, REFERENCE_ADVALUE, IPC_NOWAIT) != -1 ) {
#ifdef DEBUG_LEVEL1      
      printf("\t<hxfeedback> set reference_advalue message received\n");
#endif      
      for(i=0;i<6;i++) {
	leg[i].reference_advalue = qbuf.value[i];
	leg[i].offset = 0;
	reset_position_array(&leg[i]);
      }
      hxpd_command = REFERENCE_ADVALUE;

      qbuf.mtype = MESSAGE_ACK; /* send message acknowledgement */
      if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0) == -1 ) 
	printf("\t<hxfeedback> msgsnd failed\n");    
    }

    /* Set length2dac  command ? */
    else if( msgrcv(qid,(struct msgbuf *)&qbuf , msgsz, LENGTH2DAC, IPC_NOWAIT) != -1 ) {
#ifdef DEBUG_LEVEL1      
      printf("\t<hxfeedback> set length2dac message received\n");
#endif      
      for(i=0;i<6;i++) {
	leg[i].length2dac = qbuf.value[i];
	leg[i].offset = 0;
	reset_position_array(&leg[i]);
      }
      hxpd_command = LENGTH2DAC;
    }

    /* Set reference  command ? */
    else if( msgrcv(qid,(struct msgbuf *)&qbuf , msgsz, REFERENCE, IPC_NOWAIT) != -1 ) {
#ifdef DEBUG_LEVEL1      
      printf("\t<hxfeedback> set reference message received\n");
#endif      
      for(i=0;i<6;i++) {
	leg[i].reference = qbuf.value[i];
	leg[i].offset = 0;
	reset_position_array(&leg[i]);
      }
      hxpd_command = REFERENCE;

      qbuf.mtype = MESSAGE_ACK; /* send message acknowledgement */
      if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0) == -1 ) 
	printf("\t<hxfeedback> msgsnd failed\n");    
    }
     




    /* quit command ? */
    else if( msgrcv(qid,(struct msgbuf *)&qbuf , msgsz, QUIT , IPC_NOWAIT) != -1 ) {
#ifdef DEBUG_LEVEL1      
      printf("\t<hxfeedback> Quit hxfeedback\n");
#endif
      hxfeedback = 0;
      hxpd_command = QUIT;
      continue;
    }

    /* no command ! */
    else {
      hxpd_command = NO_MESSAGE;
      loop_time = ( clock() / (double) CLOCKS_PER_SEC);
      if( (loop_time - actual_time) < LOOP_PERIOD ) {
	continue;      /* if no command is available and time since last reading is less than LOOP_PERIOD */
                       /* do not update readings */
      }
    }



    /* read leg positions & status */
    actual_time =( clock() / (double) CLOCKS_PER_SEC);
    read_lengths(actual_position);


  
    
  for(i=0;i<6;i++){

      leg[i].position[leg[i].actual] = actual_position[i]; /* store actual position */
      if ( actual_time > leg[i].oldtime ) {                /* set the slope of last displacement */ 
	if ( leg[i].actual != 0) 
	  actual_speed = ( leg[i].position[leg[i].actual] - leg[i].position[leg[i].actual - 1] )\
			   / (actual_time - leg[i].oldtime);			   
	else 
	  actual_speed = ( leg[i].position[leg[i].actual] - leg[i].position[AVRG_POINTS - 1] )\
			   / (actual_time - leg[i].oldtime);
	leg[i].oldtime = actual_time;                            /* reset old_time   */
	leg[i].dspeed = fabs(leg[i].speed - actual_speed);
	leg[i].speed = actual_speed;
      }
      if ( leg[i].actual < AVRG_POINTS - 1 ) 
	leg[i].actual++;                     /* increment the pointer to actual in position array */
      else 
	leg[i].actual = 0;                   /* reset pointer if last element of position array */

      avrg_and_stddev(&leg[i]);              /* calculate average and sigma of last AVRG_POINTS */  
#ifdef PRINT_TO_FILE
      if(i==0) {
	printf("%f %f %f %f %f\n",actual_time,actual_position[i],leg[i].average,leg[i].speed,leg[i].dspeed);
	fprintf(fp,"%f %f %f %f %f\n",actual_time,actual_position[i],leg[i].average,leg[i].speed,leg[i].dspeed);
      }
#endif
 
    

      if (!is_in_control_range(leg[i].setpoint,actual_position[i] ,0)) {
	leg[i].status = OUT_OF_RANGE;
	continue;
      }

      acs = is_in_stable_range(leg[i].setpoint,actual_position[i] ,0);
      avs = is_in_stable_range(leg[i].setpoint,leg[i].average,0);
      avss = is_in_stable_range(leg[i].setpoint,leg[i].average,leg[i].sigma);

      leg[i].status = CONTROL_RANGE_1;      
      if( avss ) {
	leg[i].status = STABLE_RANGE;
	continue;
      }
      if( avs ) {
	leg[i].status = CONTROL_RANGE_3;
	continue;
      }
      if( acs ) {
	leg[i].status = CONTROL_RANGE_2;
      }

  

    } /* end of legs position and status readings */

  
    /* take appropriate actions according leg's status */ 
    for (i=0; i<6; i++) {              

#ifdef DEBUG_LEVEL2
      printf("\t<hxfeedback> Leg %d status %d \n",i+1,leg[i].status);
#endif

      switch(leg[i].status){
      case OUT_OF_RANGE:
      case CONTROL_RANGE_1:
      case CONTROL_RANGE_2:
	if(leg[i].control == INJECTORS_OFF && injectors_control == INJECTORS_ON) {
	  switch_injectors(i,INJECTORS_ON);
	  leg[i].control = INJECTORS_ON;
	}
	break;
      case STABLE_RANGE:
    	if(leg[i].control == INJECTORS_ON) {
	  switch_injectors(i,INJECTORS_OFF);
	  leg[i].control = INJECTORS_OFF;
	}
	break;  
      }
    }
      /* put results on the network */
    switch ( hxpd_command ) {

    case STATUS_READ:
      for(i=0;i<6;i++) {
	qbuf.status[i]=leg[i].status;
        qbuf.value[i]=actual_position[i];
      }
      qbuf.mtype = STATUS_WRITE;
      if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0) == -1 ) 
	printf("\t<hxfeedback> msgsnd failed\n");
      break;
 
    default:
      break;
    }        
  }	/* end of while(hxfeedback) */
  
#ifdef PRINT_TO_FILE
  fclose(fp);
#endif

  switch_injectors(ALL_LEGS,INJECTORS_OFF); /* switch off injectors */

  if ( msgctl(qid, IPC_RMID, 0) == -1) {
    printf("\t<hxfeedback> Cannot remove queue \n");
    exit(-1);
  }
#ifdef DEBUG_LEVEL1
     printf("\t<hxfeedback> Exiting \n");
#endif
  exit(0);
}
