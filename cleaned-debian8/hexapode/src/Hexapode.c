static char HexaRcsId[] = "$Header: /segfs/dserver/classes/hexapode/src/RCS/Hexapode.c,v 2.3 2012/09/24 09:22:53 perez Rel $";
/*********************************************************************
 *
 * File:         Hexapode.c
 *
 * Project:      Hexapode Device Server
 *
 * Description:  Main device server code.
 *
 * Author(s):    Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:     March 11 1993
 *
 * $Log: Hexapode.c,v $
 * Revision 2.3  2012/09/24 09:22:53  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:15  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:12  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.34  2009/05/25 12:37:36  perez
 * Fix bug in VPAP retries to avoid slowing down DS
 *
 * Revision 1.31  2008/01/17 10:17:25  rey
 * Fix bug with usleep
 *
 * Revision 1.30  2006/05/22 14:50:43  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.29  2006/05/22 14:47:26  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.28  2006/05/22 14:41:40  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.27  2006/05/22 14:39:50  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.26  2006/05/22 14:37:55  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.25  2006/05/22 14:37:02  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.24  2006/05/22 14:26:59  rey
 * dev_counter in process id array corrected avoids bug when stopping hexapode
 *
 * Revision 1.23  2006/03/16 14:11:24  rey
 * CheckAll no return. bug solved
 *
 * Revision 1.22  2004/03/09  16:29:19  16:29:19  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:08  16:36:08  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:12  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.13  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.12  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.11  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.10  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.9  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.8  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.7  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.6  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.5  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.4  1993/10/26  07:53:43  rey
 * Version at 26 October
 *
 * Revision 1.3  1993/10/26  07:51:36  rey
 * Version del 26 Octubre.
 *
 * Revision 1.2  1993/10/16  20:12:41  rey
 * A domingo 16
 *
 * Revision 1.1  93/10/11  17:47:11  17:47:11  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 * Revision 1.2  1993/10/03  14:48:41  rey
 * Antes de empezar a marranear
 *
 * Revision 1.1  1993/09/30  12:59:20  rey
 * Initial revision
 *
 *
 * Copyright(c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 *********************************************************************/

#ifdef linux
#include <SharedMemory.h>
#include <SignalHandling.h>
#include <sys/types.h>
#include <sys/wait.h>
#endif

#include <DevSignal.h>
#include <API.h>
#include <DevServer.h>
#include <DevErrors.h>
#include <Admin.h>

#include <DevServerP.h>
#include <Hexapode.h>
#include <HexapodeP.h>

#define DEBUG_LVL1
#define DEBUG_LVL2
#define DEBUG_LVL3
/*
#define DEBUG_LVL1
#define DEBUG_LVL2
#define DEBUG_LVL3
#define DEBUG_LVL4
*/

#define HEXAPODE_CLASS_NAME "HexapodeClass"
/*
 * Timeout for protocol in seconds.
 */
#define PR_TIMEOUT   30
#define PR_KILLSLEEP 0

  /* extra microsecond sleep before sending a kill 
     leaves time to partner to get ready for new signals */
#ifdef linux
#define MICROSLEEP
#endif
#define PR_MICROSLEEP 100

/*
 * Macro.
 */
#define CheckArgin(n) if ( argin->length != (n) ) return(*error=DevErr_DeviceIllegalParameter, DS_NOTOK)
                      
/*
 * Needed .
 */
#define   SHNAME "hxpd%02d"

long             pid;

/* 
 * MP 22Apr03
 * doesn't work when launched from bliss_dserver script, 
 * let's PATH locate the Hexpito rather thant giving here a path to it
 *
char            *subprocname  = "./Hexapito";
 */
char            *subprocname  = "Hexapito";

struct timeval   time_now;
struct timezone  tzp;

int handshaking;

/*
 * Protocol related declarations.
 */
void     sigAccept();
void     sigSuccess();
void     sigError();
void     sigAckn();
void     sigKill();
long     ds_protocolStart();
long     ds_protocolWrite();
long     ds_protocolEnd();

/*
 * Auxiliary functions.
 */
void      print_resources();

/*
 * public methods
 */

static  long  class_initialise();
static  long  object_create();
static  long  object_initialise();
static  long  state_handler();

static  DevMethodListEntry methods_list[] = {
     {DevMethodClassInitialise, class_initialise},
     {DevMethodInitialise,      object_initialise},
     {DevMethodCreate,          object_create},
     {DevMethodStateHandler,    state_handler},
   };


HexapodeClassRec hexapodeClassRec = {
   /* n_methods */        sizeof(methods_list)/sizeof(DevMethodListEntry),
   /* methods_list */     methods_list,
   };

HexapodeClass hexapodeClass = (HexapodeClass)&hexapodeClassRec;

/*
 * public commands
 */
static  long  dev_description();
static  long  dev_state();
static  long  dev_status();
static  long  dev_reset();
static  long  dev_lasterror();
static  long  dev_lastresetmode();
static  long  dev_actuatorstatus();
static  long  dev_getmode();
static  long  dev_setmode();
static  long  dev_getvelocity();
static  long  dev_setvelocity();
static  long  dev_mechresolution();

static  long  dev_setdebug();
static  long  dev_setlibdebug();

static  long  dev_getrefsystem();
static  long  dev_getrefposition();
static  long  dev_defaultreference();
static  long  dev_setrefsystem();
static  long  dev_setrefaxis();
static  long  dev_setreforigin();
static  long  dev_setrefposition();

static  long  dev_getposition();
static  long  dev_move();
static  long  dev_rotate();
static  long  dev_translate();
static  long  dev_stop();
static  long  dev_checkposition();

static  long  dev_getlengths();
static  long  dev_setlengths();
static  long  dev_moveactuators();
static  long  dev_getresource();
static  long  dev_setresource();
static  long  dev_getmovresolution();
static  long  dev_setmovresolution();
static  long  dev_searchhome();
static  long  dev_newhomelengths();
static  long  dev_nominalposition();

static  DevCommandListEntry commands_list[] = {
   {DevHxpdDescription,      dev_description,   D_VOID_TYPE,   D_STRING_TYPE},
   {DevHxpdState,            dev_state,         D_VOID_TYPE,   D_SHORT_TYPE},
   {DevHxpdStatus,           dev_status,        D_VOID_TYPE,   D_STRING_TYPE},
   {DevHxpdReset,            dev_reset,         D_LONG_TYPE,   D_VOID_TYPE},
   {DevHxpdLastError,        dev_lasterror,     D_VOID_TYPE,   D_LONG_TYPE},
   {DevHxpdLastResetMode,    dev_lastresetmode, D_VOID_TYPE,   D_LONG_TYPE},
   {DevHxpdActuatorStatus,   dev_actuatorstatus,D_VOID_TYPE,   D_VAR_LONGARR},
   {DevHxpdGetMode,          dev_getmode,       D_VOID_TYPE,   D_LONG_TYPE},
   {DevHxpdSetMode,          dev_setmode,       D_LONG_TYPE,   D_VOID_TYPE},
   {DevHxpdGetVelocity,      dev_getvelocity,   D_VOID_TYPE,   D_FLOAT_TYPE},
   {DevHxpdSetVelocity,      dev_setvelocity,   D_FLOAT_TYPE,  D_VOID_TYPE},
   {DevHxpdMechResolution,   dev_mechresolution,D_VOID_TYPE,   D_DOUBLE_TYPE},

   {DevHxpdSetDebug,         dev_setdebug,      D_LONG_TYPE,   D_VOID_TYPE},
   {DevHxpdSetLibDebug,      dev_setlibdebug,   D_LONG_TYPE,   D_VOID_TYPE},

   {DevHxpdGetRefSystem,    dev_getrefsystem,   D_VOID_TYPE,   D_VAR_DOUBLEARR},
   {DevHxpdGetRefPosition,  dev_getrefposition, D_VOID_TYPE,   D_VAR_DOUBLEARR},
   {DevHxpdDefaultReference,dev_defaultreference,D_VOID_TYPE,  D_VOID_TYPE},
   {DevHxpdSetRefSystem,    dev_setrefsystem,   D_VAR_DOUBLEARR,D_VOID_TYPE},
   {DevHxpdSetRefAxis,      dev_setrefaxis,     D_VAR_DOUBLEARR,D_VOID_TYPE},
   {DevHxpdSetRefOrigin,    dev_setreforigin,   D_VAR_DOUBLEARR,D_VOID_TYPE},
   {DevHxpdSetRefPosition,  dev_setrefposition, D_VAR_DOUBLEARR,D_VOID_TYPE},

   {DevHxpdGetPosition,     dev_getposition,    D_VOID_TYPE,  D_VAR_DOUBLEARR},
   {DevHxpdMove,            dev_move,           D_VAR_DOUBLEARR,D_VOID_TYPE},
   {DevHxpdRotate,          dev_rotate,         D_VAR_DOUBLEARR,D_VOID_TYPE},
   {DevHxpdTranslate,       dev_translate,      D_VAR_DOUBLEARR,D_VOID_TYPE},
   {DevHxpdStop,            dev_stop,           D_VOID_TYPE,    D_VOID_TYPE},
   {DevHxpdCheckPosition,   dev_checkposition, D_VAR_DOUBLEARR,D_VAR_DOUBLEARR},

   {DevHxpdGetLengths,      dev_getlengths,     D_VOID_TYPE, D_VAR_DOUBLEARR},
   {DevHxpdSetLengths,      dev_setlengths,     D_VAR_DOUBLEARR,D_VOID_TYPE},
   {DevHxpdMoveActuators,   dev_moveactuators,  D_VAR_DOUBLEARR,D_VOID_TYPE},
   {DevHxpdGetResource,     dev_getresource,    D_STRING_TYPE,  D_STRING_TYPE},
   {DevHxpdSetResource,     dev_setresource,    D_VAR_STRINGARR,D_VOID_TYPE},
   {DevHxpdGetMovResolution,dev_getmovresolution,D_VOID_TYPE,   D_DOUBLE_TYPE},
   {DevHxpdSetMovResolution,dev_setmovresolution,D_DOUBLE_TYPE, D_VOID_TYPE},
   {DevHxpdSearchHome,      dev_searchhome,     D_VOID_TYPE,    D_VOID_TYPE},
   {DevHxpdNewHomeLengths,  dev_newhomelengths, D_VOID_TYPE,  D_VAR_DOUBLEARR},
   {DevHxpdNominalPosition, dev_nominalposition,D_VOID_TYPE,  D_VAR_DOUBLEARR},

};

static long n_commands = sizeof(commands_list)/sizeof(DevCommandListEntry);

/*
 * reserve space for a default copy of the hexapode object
 */

static HexapodeRec hexapodeRec;
static Hexapode hexapode = (Hexapode)&hexapodeRec;

/*
 * Hexapode resource tables used to access the static database
 */

db_resource res_object[] = {
   {"Description",           D_STRING_TYPE,      NULL},
   {"Fixed1X",                D_DOUBLE_TYPE,      NULL},
   {"Fixed1Y",                D_DOUBLE_TYPE,      NULL},
   {"Fixed1Z",                D_DOUBLE_TYPE,      NULL},
   {"Fixed2X",                D_DOUBLE_TYPE,      NULL},
   {"Fixed2Y",                D_DOUBLE_TYPE,      NULL},
   {"Fixed2Z",                D_DOUBLE_TYPE,      NULL},
   {"Fixed3X",                D_DOUBLE_TYPE,      NULL},
   {"Fixed3Y",                D_DOUBLE_TYPE,      NULL},
   {"Fixed3Z",                D_DOUBLE_TYPE,      NULL},
   {"Fixed4X",                D_DOUBLE_TYPE,      NULL},
   {"Fixed4Y",                D_DOUBLE_TYPE,      NULL},
   {"Fixed4Z",                D_DOUBLE_TYPE,      NULL},
   {"Fixed5X",                D_DOUBLE_TYPE,      NULL},
   {"Fixed5Y",                D_DOUBLE_TYPE,      NULL},
   {"Fixed5Z",                D_DOUBLE_TYPE,      NULL},
   {"Fixed6X",                D_DOUBLE_TYPE,      NULL},
   {"Fixed6Y",                D_DOUBLE_TYPE,      NULL},
   {"Fixed6Z",                D_DOUBLE_TYPE,      NULL},
   {"Moving1X",               D_DOUBLE_TYPE,      NULL},
   {"Moving1Y",               D_DOUBLE_TYPE,      NULL},
   {"Moving1Z",               D_DOUBLE_TYPE,      NULL},
   {"Moving2X",               D_DOUBLE_TYPE,      NULL},
   {"Moving2Y",               D_DOUBLE_TYPE,      NULL},
   {"Moving2Z",               D_DOUBLE_TYPE,      NULL},
   {"Moving3X",               D_DOUBLE_TYPE,      NULL},
   {"Moving3Y",               D_DOUBLE_TYPE,      NULL},
   {"Moving3Z",               D_DOUBLE_TYPE,      NULL},
   {"Moving4X",               D_DOUBLE_TYPE,      NULL},
   {"Moving4Y",               D_DOUBLE_TYPE,      NULL},
   {"Moving4Z",               D_DOUBLE_TYPE,      NULL},
   {"Moving5X",               D_DOUBLE_TYPE,      NULL},
   {"Moving5Y",               D_DOUBLE_TYPE,      NULL},
   {"Moving5Z",               D_DOUBLE_TYPE,      NULL},
   {"Moving6X",               D_DOUBLE_TYPE,      NULL},
   {"Moving6Y",               D_DOUBLE_TYPE,      NULL},
   {"Moving6Z",               D_DOUBLE_TYPE,      NULL},
   {"Topology",               D_STRING_TYPE,      NULL},
   {"MechanicalRef",          D_STRING_TYPE,      NULL},
   {"LengthUncertainty",      D_DOUBLE_TYPE,      NULL},
   {"LinkLength1",            D_DOUBLE_TYPE,      NULL},
   {"LinkLength2",            D_DOUBLE_TYPE,      NULL},
   {"LinkLength3",            D_DOUBLE_TYPE,      NULL},
   {"LinkLength4",            D_DOUBLE_TYPE,      NULL},
   {"LinkLength5",            D_DOUBLE_TYPE,      NULL},
   {"LinkLength6",            D_DOUBLE_TYPE,      NULL},
   {"MinActuatorLength",      D_DOUBLE_TYPE,      NULL},
   {"MaxActuatorLength",      D_DOUBLE_TYPE,      NULL},
   {"MinLength1",             D_DOUBLE_TYPE,      NULL},
   {"MinLength2",             D_DOUBLE_TYPE,      NULL},
   {"MinLength3",             D_DOUBLE_TYPE,      NULL},
   {"MinLength4",             D_DOUBLE_TYPE,      NULL},
   {"MinLength5",             D_DOUBLE_TYPE,      NULL},
   {"MinLength6",             D_DOUBLE_TYPE,      NULL},
   {"MaxLength1",             D_DOUBLE_TYPE,      NULL},
   {"MaxLength2",             D_DOUBLE_TYPE,      NULL},
   {"MaxLength3",             D_DOUBLE_TYPE,      NULL},
   {"MaxLength4",             D_DOUBLE_TYPE,      NULL},
   {"MaxLength5",             D_DOUBLE_TYPE,      NULL},
   {"MaxLength6",             D_DOUBLE_TYPE,      NULL},
   {"MaxTiltAngle",           D_DOUBLE_TYPE,      NULL},
   {"MaxIncrementX",          D_DOUBLE_TYPE,      NULL},
   {"MaxIncrementY",          D_DOUBLE_TYPE,      NULL},
   {"MaxIncrementZ",          D_DOUBLE_TYPE,      NULL},
   {"MaxIncrementPhi",        D_DOUBLE_TYPE,      NULL},
   {"MaxIncrementTheta",      D_DOUBLE_TYPE,      NULL},
   {"MaxIncrementPsi",        D_DOUBLE_TYPE,      NULL},
   {"MaxMovementResolution",  D_DOUBLE_TYPE,      NULL},
   {"NominalLength1",         D_DOUBLE_TYPE,      NULL},
   {"NominalLength2",         D_DOUBLE_TYPE,      NULL},
   {"NominalLength3",         D_DOUBLE_TYPE,      NULL},
   {"NominalLength4",         D_DOUBLE_TYPE,      NULL},
   {"NominalLength5",         D_DOUBLE_TYPE,      NULL},
   {"NominalLength6",         D_DOUBLE_TYPE,      NULL},
   {"DefRefSystemX",          D_DOUBLE_TYPE,      NULL},
   {"DefRefSystemY",          D_DOUBLE_TYPE,      NULL},
   {"DefRefSystemZ",          D_DOUBLE_TYPE,      NULL},
   {"DefRefSystemPhi",        D_DOUBLE_TYPE,      NULL},
   {"DefRefSystemTheta",      D_DOUBLE_TYPE,      NULL},
   {"DefRefSystemPsi",        D_DOUBLE_TYPE,      NULL},
   {"ReferenceSystemLock",    D_BOOLEAN_TYPE,     NULL},
   {"DefRefPositionX",        D_DOUBLE_TYPE,      NULL},
   {"DefRefPositionY",        D_DOUBLE_TYPE,      NULL},
   {"DefRefPositionZ",        D_DOUBLE_TYPE,      NULL},
   {"DefRefPositionPhi",      D_DOUBLE_TYPE,      NULL},
   {"DefRefPositionTheta",    D_DOUBLE_TYPE,      NULL},
   {"DefRefPositionPsi",      D_DOUBLE_TYPE,      NULL},
   {"HomeLength1",            D_DOUBLE_TYPE,      NULL},
   {"HomeLength2",            D_DOUBLE_TYPE,      NULL},
   {"HomeLength3",            D_DOUBLE_TYPE,      NULL},
   {"HomeLength4",            D_DOUBLE_TYPE,      NULL},
   {"HomeLength5",            D_DOUBLE_TYPE,      NULL},
   {"HomeLength6",            D_DOUBLE_TYPE,      NULL},
   {"MovementMode",           D_STRING_TYPE,      NULL},
   {"Backlash",               D_DOUBLE_TYPE,      NULL},
   {"ActuatorBacklash",       D_DOUBLE_TYPE,      NULL},
   {"MotorType",              D_STRING_TYPE,      NULL},
   {"VPAP_StepsPerMM",        D_FLOAT_TYPE,       NULL},
   {"VPAP_MaxSpeed",          D_FLOAT_TYPE,       NULL},
   {"VPAP_SlopeRate",         D_SHORT_TYPE,       NULL},
   {"VPAP_InitialSpeed",      D_FLOAT_TYPE,       NULL},
   {"VPAP_HomeSearchSpeed",   D_FLOAT_TYPE,       NULL},
   {"VPAP_HomeHysteresis",    D_LONG_TYPE,        NULL},
   {"VPAPcratenum",           D_SHORT_TYPE,       NULL},
   {"VPAPmotor1Unit",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor1Chan",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor2Unit",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor2Chan",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor3Unit",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor3Chan",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor4Unit",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor4Chan",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor5Unit",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor5Chan",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor6Unit",         D_SHORT_TYPE,       NULL},
   {"VPAPmotor6Chan",         D_SHORT_TYPE,       NULL},
/* begmod PM */
   {"SerialDeviceName",      D_STRING_TYPE,      NULL},
   {"StepsPerMeter",         D_FLOAT_TYPE,       NULL},
   {"FeedbackProcess",       D_STRING_TYPE,      NULL},
/* endmod PM */
   {"IcepapHostname",         D_STRING_TYPE,      NULL},
   {"IcepapDebug",            D_SHORT_TYPE,       NULL},
   {"IcepapLibDebug",         D_SHORT_TYPE,       NULL},
   {"IcepapMotor1Addr",       D_SHORT_TYPE,       NULL},
   {"IcepapMotor2Addr",       D_SHORT_TYPE,       NULL},
   {"IcepapMotor3Addr",       D_SHORT_TYPE,       NULL},
   {"IcepapMotor4Addr",       D_SHORT_TYPE,       NULL},
   {"IcepapMotor5Addr",       D_SHORT_TYPE,       NULL},
   {"IcepapMotor6Addr",       D_SHORT_TYPE,       NULL},
   {"IcepapStepsPerMM",       D_FLOAT_TYPE,       NULL},
   {"IcepapVelocity",         D_SHORT_TYPE,       NULL},

};

int res_object_size = sizeof(res_object)/sizeof(db_resource);

db_resource res_class[] = {
   {"FilePath",           D_STRING_TYPE,      NULL},
};

int res_class_size = sizeof(res_class)/sizeof(db_resource);

/*
 * Pointers.
 */
SharedData   *SD;
GeometryData *GD;
ActuatorData *AD;

double   double_out[10];
long     long_out[10];

/*
 * Arrays to keep the list of pid and s names for
 * correct termination.
 */

#define MAX_HEXDEV  10
long   dev_counter = 0;

long   no_dev=MAX_HEXDEV;


key_t  shKeyArray[MAX_HEXDEV];

struct sigaction o_accepted,
                 o_success,
                 o_error,
                 o_end_ackn,
                 o_sigint,
                 o_sigquit,
                 o_sigterm;

long  *pidArray;


/*********************************************************************
 *  Function:      static long class_initialise()
 * 
 *  Description:  routine to be called the first time a device is 
 *                created which belongs to this class (or is a subclass
 *                thereof. This routine will be called only once.
 * 
 *  Arg(s) In:  none
 * 
 *  Arg(s) Out:  long *error - pointer to error code
 *********************************************************************/
static long class_initialise(error)
long *error;
{
    long i;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE class_initialise: '%s'>\n", HEXAPODE_CLASS_NAME);
#endif

/*
 * HexapodeClass is a subclass of the DevServerClass
 */

   hexapodeClass->devserver_class.superclass = devServerClass;
   hexapodeClass->devserver_class.class_name = (char*)malloc(sizeof(HEXAPODE_CLASS_NAME)+1);
   if ( hexapodeClass->devserver_class.class_name == 0 ){
        *error = DevErr_InsufficientMemory;
        return (DS_NOTOK);
   }
   strcpy(hexapodeClass->devserver_class.class_name, HEXAPODE_CLASS_NAME);
   hexapodeClass->devserver_class.class_inited = 1;
   hexapodeClass->devserver_class.n_commands = n_commands;
   hexapodeClass->devserver_class.commands_list = commands_list;

/*
 * initialise hexapode with default values. these will be used
 * for every Hexapode object created.
 */

   hexapode->devserver.class_pointer = (DevServerClass)hexapodeClass;

/*
 * Interrogate the static database for default values
 */

   hexapodeClass->hexapode_class.filepath = "./";

   res_class[0].resource_adr  = &(hexapodeClass->hexapode_class.filepath);

   if(db_getresource(hexapodeClass->hexapode_class.class_svr_name, res_class, res_class_size, error))
   {
     return(DS_NOTOK);
   }

   /*
    * Allocate memory for arrays of sh_memory and pid's
    */

    pidArray    = (long *) malloc(no_dev * sizeof(long));
    if (pidArray == 0) 
    {
        printf("Insufficient memory\n");
        return(DS_NOTOK);
    }

    print_resources(res_class, res_class_size);

   /*
    *  Register signals for protocol. 
    */
   handshaking = NO_COMMAND;

   ds__signal(COMMAND_ACCEPTED,  sigAccept,  error);
   ds__signal(COMMAND_SUCCESFUL, sigSuccess, error);
   ds__signal(COMMAND_ERROR,     sigError,   error);
   ds__signal(COMMAND_END_ACKN,  sigAckn,    error);

   ds__signal(SIGINT,  sigKill, error);
   ds__signal(SIGQUIT, sigKill, error);
   ds__signal(SIGTERM, sigKill, error);

   return(DS_OK);
}


/*********************************************************************
 * Function:  static long object_create()
 * 
 * Description: routine to be called on creation of a device object
 * 
 * Arg(s) In:   char      *name   - name to be given to device
 * 
 * Arg(s) Out:  DevServer *ds_ptr - pointer to created device
 *              long      *error  - pointer to error code.
 **********************************************************************/
static long object_create(name, ds_ptr, error)
char      *name;
DevServer *ds_ptr;
long      *error;
{
   Hexapode ds;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE object_create: '%s'>\n", name);
#endif

   ds = (Hexapode)malloc(sizeof(HexapodeRec));

/*
 * initialise device with default object
 */

   *(HexapodeRec*)ds = *(HexapodeRec*)hexapode;

/*
 * finally initialise the non-default values
 */

   ds->devserver.name = (char*)malloc(strlen(name)+1);
   sprintf(ds->devserver.name,"%s",name);

   *ds_ptr = (DevServer)ds;

   return(DS_OK);
}


/*****************************************************************************
 * 
 * Function:  static long object_initialise()
 * 
 * Description:  routine to be called on initialisation of a device object
 * 
 * Arg(s) In:  Hexapode ds  - object to initialise
 * 
 * Arg(s) Out:
 * 
 *            long *error     - pointer to error code.
 *****************************************************************************/
static long object_initialise(ds, error)
Hexapode ds;
long  *error;
{
   long       time_diff;
   char      *ptmodule;
   long       i, j;

   int        fp_length;
   long       number;
   float      StepsPerMM;
   char       *MotorType,
/* begmod PM */
              *DeviceName,
              *FeedbackProcess,   /* for hydraulic controllers only */
/* endmod PM */
              *Topology,
              *MechRef,
              *MovementMode;

   char       *shmname;
   char       *NamePtr;

   vpapData   *VpapPrivate;
   icepapData *IcepapPrivate;
   char       *IcepapHostname;
   DevShort    IcepapDebug;
   DevShort    IcepapLibDebug;
   DevShort    IcepapMotorAddr[6];
   DevFloat    IcepapStepsPerMM;
   DevShort    IcepapVelocity;


/* begmod PM */
#ifdef linux
   char       mod_key_arg[128];
   key_t      mod_key;
#endif
/* endmod PM */

#ifdef DEBUG_LVL1
   printf("<HEXAPODE object_initialise>: %s\n", ds->devserver.name);
#endif

    shmname = ds->hexapode.shname = (char*)malloc(sizeof(SHNAME)*sizeof(char)); 
    if (shmname == NULL){
        *error = DevErr_InsufficientMemory;
        return(DS_NOTOK);
    }

    for (number = 1; number < 100; number++){

        sprintf(shmname, SHNAME, number);

       /*
        * If dataport exist, choose another name.
        */

        mod_key = ftok(".",(char) number);
        if ( (ptmodule = modcreate(mod_key,sizeof(SharedData))) == (char *) -1)  {

#ifdef DEBUG_LVL1
            printf("Module %s already exists\n",shmname);
#endif
        } else { /* Shared memory created */ 
            break;
        }

    }

    if (number >= 100){
        printf("Mecachis!\n");
        *error = DevErr_InsufficientMemory;
        return(DS_NOTOK);
    }

    /*
     * Create shared memory for this hexapode.
     */

    ds->hexapode.shared = (SharedData *)(ptmodule);

   /*
    * Fill it the new created shared memory with values.
    */
    ds->hexapode.shared->time = 0;
    ds->hexapode.shared->serverpid = getpid();

   /*
    * Assign pointers.
    */
    SD = ds->hexapode.shared;
    GD = &(SD->GData);
    AD = &(SD->AData);
  
#ifdef DEBUG_LVL1
    printf("<HEXAPODE> object_initialize: reading resources\n");
#endif

   ds->hexapode.description = "";
   Topology = MechRef = MovementMode = MotorType = "";
   DeviceName = FeedbackProcess = "";

   
   IcepapHostname = "";
   IcepapDebug    = -1;
   IcepapLibDebug = -1;
   for(i=0;i<6;i++) IcepapMotorAddr[i] = -1;

   /*
    * Read resources from database.
    */
   i = 0;
   res_object[i++].resource_adr = &(ds->hexapode.description);
   res_object[i++].resource_adr = &(GD->FixedPoint[0][x]);
   res_object[i++].resource_adr = &(GD->FixedPoint[0][y]);
   res_object[i++].resource_adr = &(GD->FixedPoint[0][z]);
   res_object[i++].resource_adr = &(GD->FixedPoint[1][x]);
   res_object[i++].resource_adr = &(GD->FixedPoint[1][y]);
   res_object[i++].resource_adr = &(GD->FixedPoint[1][z]);
   res_object[i++].resource_adr = &(GD->FixedPoint[2][x]);
   res_object[i++].resource_adr = &(GD->FixedPoint[2][y]);
   res_object[i++].resource_adr = &(GD->FixedPoint[2][z]);
   res_object[i++].resource_adr = &(GD->FixedPoint[3][x]);
   res_object[i++].resource_adr = &(GD->FixedPoint[3][y]);
   res_object[i++].resource_adr = &(GD->FixedPoint[3][z]);
   res_object[i++].resource_adr = &(GD->FixedPoint[4][x]);
   res_object[i++].resource_adr = &(GD->FixedPoint[4][y]);
   res_object[i++].resource_adr = &(GD->FixedPoint[4][z]);
   res_object[i++].resource_adr = &(GD->FixedPoint[5][x]);
   res_object[i++].resource_adr = &(GD->FixedPoint[5][y]);
   res_object[i++].resource_adr = &(GD->FixedPoint[5][z]);
   res_object[i++].resource_adr = &(GD->MovingPoint[0][x]);
   res_object[i++].resource_adr = &(GD->MovingPoint[0][y]);
   res_object[i++].resource_adr = &(GD->MovingPoint[0][z]);
   res_object[i++].resource_adr = &(GD->MovingPoint[1][x]);
   res_object[i++].resource_adr = &(GD->MovingPoint[1][y]);
   res_object[i++].resource_adr = &(GD->MovingPoint[1][z]);
   res_object[i++].resource_adr = &(GD->MovingPoint[2][x]);
   res_object[i++].resource_adr = &(GD->MovingPoint[2][y]);
   res_object[i++].resource_adr = &(GD->MovingPoint[2][z]);
   res_object[i++].resource_adr = &(GD->MovingPoint[3][x]);
   res_object[i++].resource_adr = &(GD->MovingPoint[3][y]);
   res_object[i++].resource_adr = &(GD->MovingPoint[3][z]);
   res_object[i++].resource_adr = &(GD->MovingPoint[4][x]);
   res_object[i++].resource_adr = &(GD->MovingPoint[4][y]);
   res_object[i++].resource_adr = &(GD->MovingPoint[4][z]);
   res_object[i++].resource_adr = &(GD->MovingPoint[5][x]);
   res_object[i++].resource_adr = &(GD->MovingPoint[5][y]);
   res_object[i++].resource_adr = &(GD->MovingPoint[5][z]);
   res_object[i++].resource_adr = &(Topology);
   res_object[i++].resource_adr = &(MechRef);
   res_object[i++].resource_adr = &(SD->LengthUncertainty);
   res_object[i++].resource_adr = &(GD->LinkLength[0]);
   res_object[i++].resource_adr = &(GD->LinkLength[1]);
   res_object[i++].resource_adr = &(GD->LinkLength[2]);
   res_object[i++].resource_adr = &(GD->LinkLength[3]);
   res_object[i++].resource_adr = &(GD->LinkLength[4]);
   res_object[i++].resource_adr = &(GD->LinkLength[5]);
   res_object[i++].resource_adr = &(GD->MinActuatorLength);
   res_object[i++].resource_adr = &(GD->MaxActuatorLength);
   res_object[i++].resource_adr = &(GD->MinLength[0]);
   res_object[i++].resource_adr = &(GD->MinLength[1]);
   res_object[i++].resource_adr = &(GD->MinLength[2]);
   res_object[i++].resource_adr = &(GD->MinLength[3]);
   res_object[i++].resource_adr = &(GD->MinLength[4]);
   res_object[i++].resource_adr = &(GD->MinLength[5]);
   res_object[i++].resource_adr = &(GD->MaxLength[0]);
   res_object[i++].resource_adr = &(GD->MaxLength[1]);
   res_object[i++].resource_adr = &(GD->MaxLength[2]);
   res_object[i++].resource_adr = &(GD->MaxLength[3]);
   res_object[i++].resource_adr = &(GD->MaxLength[4]);
   res_object[i++].resource_adr = &(GD->MaxLength[5]);
   res_object[i++].resource_adr = &(GD->MaxTiltAngle);
   res_object[i++].resource_adr = &(GD->MaxIncrement[x]);
   res_object[i++].resource_adr = &(GD->MaxIncrement[y]);
   res_object[i++].resource_adr = &(GD->MaxIncrement[z]);
   res_object[i++].resource_adr = &(GD->MaxIncrement[phi]);
   res_object[i++].resource_adr = &(GD->MaxIncrement[theta]);
   res_object[i++].resource_adr = &(GD->MaxIncrement[psi]);
   res_object[i++].resource_adr = &(SD->MaxMovementResolution);
   res_object[i++].resource_adr = &(GD->NominalLength[0]);
   res_object[i++].resource_adr = &(GD->NominalLength[1]);
   res_object[i++].resource_adr = &(GD->NominalLength[2]);
   res_object[i++].resource_adr = &(GD->NominalLength[3]);
   res_object[i++].resource_adr = &(GD->NominalLength[4]);
   res_object[i++].resource_adr = &(GD->NominalLength[5]);
   res_object[i++].resource_adr = &(GD->DefaultReferenceSystem[x]);
   res_object[i++].resource_adr = &(GD->DefaultReferenceSystem[y]);
   res_object[i++].resource_adr = &(GD->DefaultReferenceSystem[z]);
   res_object[i++].resource_adr = &(GD->DefaultReferenceSystem[phi]);
   res_object[i++].resource_adr = &(GD->DefaultReferenceSystem[theta]);
   res_object[i++].resource_adr = &(GD->DefaultReferenceSystem[psi]);
   res_object[i++].resource_adr = &(GD->ReferenceSystemLock);
   res_object[i++].resource_adr = &(GD->DefaultReferencePosition[x]);
   res_object[i++].resource_adr = &(GD->DefaultReferencePosition[y]);
   res_object[i++].resource_adr = &(GD->DefaultReferencePosition[z]);
   res_object[i++].resource_adr = &(GD->DefaultReferencePosition[phi]);
   res_object[i++].resource_adr = &(GD->DefaultReferencePosition[theta]);
   res_object[i++].resource_adr = &(GD->DefaultReferencePosition[psi]);
   res_object[i++].resource_adr = &(AD->HomeLength[0]);
   res_object[i++].resource_adr = &(AD->HomeLength[1]);
   res_object[i++].resource_adr = &(AD->HomeLength[2]);
   res_object[i++].resource_adr = &(AD->HomeLength[3]);
   res_object[i++].resource_adr = &(AD->HomeLength[4]);
   res_object[i++].resource_adr = &(AD->HomeLength[5]);
   res_object[i++].resource_adr = &(MovementMode);
   res_object[i++].resource_adr = &(AD->Backlash);
   res_object[i++].resource_adr = &(AD->ActuatorBacklash);
   res_object[i++].resource_adr = &(MotorType);
   res_object[i++].resource_adr = &(StepsPerMM);
VpapPrivate = &AD->MotorData.MotorPrivate.vpap;
   res_object[i++].resource_adr = &(VpapPrivate->MaxSpeed);
   res_object[i++].resource_adr = &(VpapPrivate->SlopeRate);
   res_object[i++].resource_adr = &(VpapPrivate->InitialSpeed);
   res_object[i++].resource_adr = &(VpapPrivate->HomeSearchSpeed);
   res_object[i++].resource_adr = &(VpapPrivate->HomeHysteresis);
   res_object[i++].resource_adr = &(VpapPrivate->cratenum);
   res_object[i++].resource_adr = &(VpapPrivate->motor[0].unitnum);
   res_object[i++].resource_adr = &(VpapPrivate->motor[0].channel);
   res_object[i++].resource_adr = &(VpapPrivate->motor[1].unitnum);
   res_object[i++].resource_adr = &(VpapPrivate->motor[1].channel);
   res_object[i++].resource_adr = &(VpapPrivate->motor[2].unitnum);
   res_object[i++].resource_adr = &(VpapPrivate->motor[2].channel);
   res_object[i++].resource_adr = &(VpapPrivate->motor[3].unitnum);
   res_object[i++].resource_adr = &(VpapPrivate->motor[3].channel);
   res_object[i++].resource_adr = &(VpapPrivate->motor[4].unitnum);
   res_object[i++].resource_adr = &(VpapPrivate->motor[4].channel);
   res_object[i++].resource_adr = &(VpapPrivate->motor[5].unitnum);
   res_object[i++].resource_adr = &(VpapPrivate->motor[5].channel);
/* begmod PM */
   res_object[i++].resource_adr = &(DeviceName);
   res_object[i++].resource_adr = &(AD->MotorData.MotorPrivate.pi.StepsPerMeter);
   res_object[i++].resource_adr = &(FeedbackProcess);
/* endmod PM */


IcepapPrivate = &AD->MotorData.MotorPrivate.icepap;
   res_object[i++].resource_adr = &(IcepapHostname);
   res_object[i++].resource_adr = &(IcepapDebug);
   res_object[i++].resource_adr = &(IcepapLibDebug);
   res_object[i++].resource_adr = &(IcepapMotorAddr[0]);
   res_object[i++].resource_adr = &(IcepapMotorAddr[1]);
   res_object[i++].resource_adr = &(IcepapMotorAddr[2]);
   res_object[i++].resource_adr = &(IcepapMotorAddr[3]);
   res_object[i++].resource_adr = &(IcepapMotorAddr[4]);
   res_object[i++].resource_adr = &(IcepapMotorAddr[5]);
   res_object[i++].resource_adr = &(IcepapStepsPerMM);
   res_object[i++].resource_adr = &(IcepapVelocity);

    if (i != res_object_size){
        printf("Wrong internal resource table\n");
        return(DS_NOTOK);
    }
 
   if(db_getresource(ds->devserver.name, res_object, res_object_size, error))
   {
     return(DS_NOTOK);
   }

   print_resources(res_object, res_object_size);


   /*
    * User sees units in degrees,mm. Program in radians, m. Conversion is
    * needed.
    */
   
   for(i = 0; i < 6; i++) {
      for (j = 0; j < 3; j++) {
         GD->FixedPoint[i][j]  = FromMMtoM(GD->FixedPoint[i][j]);
         GD->MovingPoint[i][j] = FromMMtoM(GD->MovingPoint[i][j]);
      }
      GD->LinkLength[i]    = FromMMtoM(GD->LinkLength[i]);
      GD->NominalLength[i] = FromMMtoM(GD->NominalLength[i]);
      AD->HomeLength[i]    = FromMMtoM(AD->HomeLength[i]);
      GD->MinLength[i]     = FromMMtoM(GD->MinLength[i]);
      GD->MaxLength[i]     = FromMMtoM(GD->MaxLength[i]);
   }

   printf("<MinLength1> %f\n", GD->MinLength[0]);
   printf("<MaxLength1> %f\n", GD->MaxLength[5]);

   for(i = 0; i < 3; i++) {
      GD->MaxIncrement[i] = FromMMtoM(GD->MaxIncrement[i]);
      GD->DefaultReferenceSystem[i] = FromMMtoM(GD->DefaultReferenceSystem[i]);
      GD->DefaultReferencePosition[i] = 
                    FromMMtoM(GD->DefaultReferencePosition[i]);
   }

   for(i = 3; i < 6; i++) {
      GD->MaxIncrement[i] = FromDEGtoRAD(GD->MaxIncrement[i]);
      GD->DefaultReferenceSystem[i] =
                                   FromDEGtoRAD(GD->DefaultReferenceSystem[i]);
      GD->DefaultReferencePosition[i] = 
                    FromDEGtoRAD(GD->DefaultReferencePosition[i]);
   }

   SD->LengthUncertainty = FromMMtoM(SD->LengthUncertainty);
   GD->MinActuatorLength = FromMMtoM(GD->MinActuatorLength);
   GD->MaxActuatorLength = FromMMtoM(GD->MaxActuatorLength);
   GD->MaxTiltAngle      = FromDEGtoRAD(GD->MaxTiltAngle);
   AD->Backlash          = FromMMtoM(AD->Backlash);
   AD->ActuatorBacklash  = FromMMtoM(AD->ActuatorBacklash);

   VpapPrivate->StepsPerMeter = 1000.0 * StepsPerMM;    /* Conversion end */
#ifdef DEBUG_LVL1
   printf("Conversion Factor:  %f steps/m\n", VpapPrivate->StepsPerMeter);
#endif

   /* 
    * Interpret some of the values read.
    */
       /*
        * Set Actuator type.
        */
   if ( (AD->MotorData.MotorType = LabelToValue(MotorType, &motorTypeList))
                                                                        == -1 )
   {
       printf("Hexapode Error: bad actuator type value\n");
      *error = DevErr_DeviceIllegalParameter;
       return(DS_NOTOK);
   }






  /* 
   * Add minimum ICEPAP resources check
   */
   if ( AD->MotorData.MotorType == ICEPAP ) 
   {
    /* 
     * Get ICEPAP hostname 
     */
     if(!strlen(IcepapHostname))
     {
       printf("Hexapode Error: missing resource: \"ICEPAPhostname\"\n");
       *error = DevErr_DatabaseRead;
       return(DS_NOTOK);
     }
     strncpy(IcepapPrivate->hostname,     \
             IcepapHostname,              \
             ICEPAP_HOSTNAME_MAX);
     

    /* 
     * Get verbose level of HxptIcepap and libdeep
     */
     if(IcepapDebug != -1)
       IcepapPrivate->debug = (int)(IcepapDebug);
     else
       IcepapPrivate->debug = 3;

     if(IcepapLibDebug != -1)
       IcepapPrivate->libdebug = (int)(IcepapLibDebug);
     else
       IcepapPrivate->libdebug = 1;



    /* 
     * Get ICEPAP drivers CAN addressed 
     */
     for(i=0;i<6;i++)
     {
       if(IcepapMotorAddr[i] == -1)
       {
        printf("Hexapode Error: missing resource: \"ICEPAPmotor%dAddr\"\n",i+1);
        *error = DevErr_DatabaseRead;
        return(DS_NOTOK);
       }
       IcepapPrivate->motor[i].addr = (int)(IcepapMotorAddr[i]);
     }



    /* 
     * Structure initialization
     */
     IcepapPrivate->initialized   = 0;
     IcepapPrivate->StepsPerMeter = 1000.0 * IcepapStepsPerMM; 
     IcepapPrivate->Velocity      = IcepapVelocity;

   } /* if(MotorType == ICEPAP ) */
   






      /* 
       * Add reading of PI data and do conversions 
       */
   if ( AD->MotorData.MotorType == PHYSIKINSTRUMENT ) {
       strcpy(AD->MotorData.MotorPrivate.pi.devicename,DeviceName);
#ifdef DEBUG_LVL1
     printf("Device name = %s\n",AD->MotorData.MotorPrivate.pi.devicename);
#endif
   }

      /*
       * Add reading of Hydraulic data 
       */
   if ( AD->MotorData.MotorType == HYDRAULIC ) {
       strcpy(AD->MotorData.MotorPrivate.hyd.feedback_process_name,FeedbackProcess);
#ifdef DEBUG_LVL1
       printf("Feedback Process name = %s\n",
                   AD->MotorData.MotorPrivate.hyd.feedback_process_name);
#endif
   }

      /*
       * Set Topology
       */
   if ( (GD->Topology = LabelToValue(Topology, &topologyList)) == -1 )
   {
       printf("Hexapode Error: bad topology value\n");
      *error = DevErr_DeviceIllegalParameter;
       return(DS_NOTOK);
   }

       /*
        * Set Mechanical Reference.
        */
   if ( (AD->MechanicalRef = LabelToValue(MechRef, &mechRefList)) == -1 )
   {
       printf("Hexapode Error: bad mechanical reference value\n");
      *error = DevErr_DeviceIllegalParameter;
       return(DS_NOTOK);
   }

       /*
        * Set Mode.
        */
   if ((SD->MovementMode = LabelToValue(MovementMode,&operationModeList))== -1)
   {
       printf("Hexapode Error: bad mechanical operation mode value\n");
      *error = DevErr_DeviceIllegalParameter;
       return(DS_NOTOK);
   }

   /*
    * COMMENT: other checking of the values read should go here.
    */

   strncpy(SD->Description, ds->hexapode.description, MAX_FILENAME_LENGTH);
   if (strlen(ds->hexapode.description) >= MAX_FILENAME_LENGTH)
      SD->Description[MAX_FILENAME_LENGTH - 1] = '\0';

   /*
    * Build backup filename: filepath/dserver_name 
    */
   NamePtr = ((HexapodeClass)(ds->devserver.class_pointer))->
                                                    hexapode_class.filepath;
   fp_length = strlen(NamePtr);
   if (fp_length + strlen(ds->devserver.name) + 2 > MAX_FILENAME_LENGTH){
        *error = DevErr_InsufficientMemory;
        return(DS_NOTOK);
   }
    strcpy(SD->BackupFile, NamePtr);
    if (SD->BackupFile[fp_length - 1] != '/'){
      strcat(SD->BackupFile, "/");
      fp_length++;
   }
    strcat(SD->BackupFile, ds->devserver.name);

    for (i = fp_length; i < strlen(SD->BackupFile); i++)
         if (SD->BackupFile[i] == '/') SD->BackupFile[i] = '.';

   /*
    * Build temporary backup filename: backup-filename.tmp
    */
   if (strlen(SD->BackupFile) + 5 > MAX_FILENAME_LENGTH){
        *error = DevErr_InsufficientMemory;
        return(DS_NOTOK);
    }
   strcpy(SD->TmpBackupFile, SD->BackupFile);
   strcat(SD->TmpBackupFile, ".tmp");

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> Backup File Name:     '%s'\n", SD->BackupFile);
   printf("<HEXAPODE> Tmp Backup File Name: '%s'\n", SD->TmpBackupFile);
#endif

   /*
    * Fork a dedicated process.
    */

       handshaking = NO_COMMAND;
 
#ifdef DEBUG_LVL1
    printf("\t<HEXAPODE> Linux %s\n", MovementMode);
#endif

       if ( (pid = fork()) == -1) {
           *error = DevErr_DeviceOpen;
            modremove(mod_key);
 
#ifdef DEBUG_LVL1
            printf("<HEXAPODE> Cannot fork subprocess!");
#endif
       
            return(DS_NOTOK);
       }

       handshaking = NO_COMMAND;

       if ( pid == 0 ) { /* client side */
           sprintf(mod_key_arg, "%d",  mod_key);
           if (execlp( subprocname, subprocname, mod_key_arg, (char *)0) == -1) {
               printf("<Hexapode> cannot fork subprocess %s!!!", subprocname);
           }
       }
       shKeyArray[dev_counter] = mod_key;


       printf("Process id for hexapode %d is %d\n", dev_counter, pid);

       pidArray[dev_counter] = pid;

       dev_counter += 1;

       SD->pid = pid;
       
     
       sleep(PR_TIMEOUT);
       /*if (handshaking != COMMAND_SUCCESFUL ) {
            sleep(PR_TIMEOUT);
       }*/

       if (handshaking != COMMAND_SUCCESFUL ) {
#ifdef DEBUG_LVL1
   printf("\t<HEXAPODE> Protocol error\n");
#endif
            *error = DevErr_DeviceTimedOut;
            return(DS_NOTOK);
       }


    /*
    * Send Soft Reset Command
    */
#ifdef DEBUG_LVL1
   printf("<HEXAPODE> Soft Reset\n");
#endif

/* begmod PM */
#ifdef linux
    /* wait a little bit to allow hexapito entering its main loop */
    usleep(100000);
#endif
/* endmod PM */

    SD->ResetMode = SOFT_RESET;
    SD->Command = DEV_RESET;
    if (ds_protocolStart(error) == DS_OK)
        ds_protocolWrite(error);
   else
      printf("<HEXAPODE> Error sending SOFT RESET\n");
    ds_protocolEnd(error);


#ifdef DEBUG_LVL1
   printf("<HEXAPODE object_initialise() done>\n");
#endif
   return(DS_OK);
}


/*******************************************************************
 *  Function:      static long state_handler()
 * 
 *  Description: this routine is reserved for checking wether the command
 *               requested can be executed in the present state.
 * 
 *  Arg(s) In:  Hexapode    ds - device on which command is to executed
 *              DevCommand cmd - command to be executed
 * 
 *  Arg(s) Out: long    *error - pointer to error code.
 *********************************************************************/
static long state_handler(ds, cmd, error)
Hexapode   ds;
DevCommand cmd;
long      *error;
{
   long   state;

#ifdef DEBUG_LVL2
   printf("---------\n");
   printf("<HEXAPODE> State_Handler\n");
#endif

   /*
    * Assign pointers.
    */
   SD = ds->hexapode.shared;
   GD = &(SD->GData);
   AD = &(SD->AData);
  
   switch (cmd){
      /*
       * Commands managed by Hexapode:
       */
      case DevHxpdSetResource:
         if(dev_state(ds, NULL, &state, error) != DS_OK)
            return(DS_NOTOK);
         if (state != HXPD_MOVING)
            return(DS_OK);
         break;

      /*
       * Commands managed by Hexapito: don't do anything here
       */
      default:
         return(DS_OK);
   }
   *error = DevErr_AttemptToViolateStateMachine;
   return (DS_NOTOK);

}

/***************************************************************************
 *  Function:     static long dev_status()
 * 
 *  Description:  Routine to return state as an ASCII string
 * 
 *  Arg(s) In:    Hexapode   ds      - 
 *                DevVoid    *argin  - none
 *              
 *  Arg(s) Out:   DevString  *argout - contains string 
 ***************************************************************************/
static long dev_status(ds, argin, argout, error)
Hexapode   ds;
DevVoid    *argin;
DevString  *argout;
long       *error;
{
   short   state;
   char    *status;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_status()\n");
#endif

   if(dev_state(ds, NULL, &state, error) != DS_OK)
      return(DS_NOTOK);
    
   if ((status = ValueToLabel(state, &hexapodeStateList)) == NULL){
      *error = DevErr_InternalServerErrorDetected;
      return(DS_NOTOK);
   }
   *argout = status;
   
   return (DS_OK);
}


/*****************************************************************************
 *  Function:      static long dev_state()
 *
 *  Description:   Reads the state of the hexapode
 * 
 *  Arg(s) In:     Hexapode   ds     - 
 *                 DevVoid    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid    *argout - none
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
static long  
dev_state(ds, argin, argout, error)
Hexapode   ds;
DevVoid   *argin;
DevShort  *argout;
long      *error;
{

  short       r_state;
  long        ret;
  
#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_state()\n");
#endif /*DEBUG_LVL1*/

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;
       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
        r_state  = SD->State;
       *argout   = r_state;
   
   }

   return(ds_protocolEnd(error));
}

 
/*****************************************************************************
 *  Function:      static long dev_reset()
 *
 *  Description:   reset the hexapode.
 * 
 *  Arg(s) In:     Hexapode   ds     - 
 *                 DevVoid    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid     *argout - none
 *                 long       *error  - pointer to error code.
 *
 *  Error code(s):
 *
 *****************************************************************************/
static long  
dev_reset(ds, argin, argout, error)
Hexapode   ds;
DevLong   *argin;
DevVoid   *argout;
long      *error;
{
   long  ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_reset()>\n");
#endif 

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_RESET;
       SD->ResetMode = *argin; 
       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}
/*****************************************************************************
 *  Function:      static long dev_lastresetmode()
 *
 *  Description:   Returns the mode (SOFT or HARD) of the last succesful reset.
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - 
 *              
 *  Arg(s) Out:    DevLong    *argout - reset mode
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_lastresetmode(ds, argin, argout, error)
Hexapode          ds;
DevVoid          *argin;
DevLong          *argout;
long             *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_lastresetmode()\n");
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;
       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
       *argout = SD->LastResetMode;
   }

   return(ds_protocolEnd(error));

}

/*****************************************************************************
 *  Function:      static long dev_searchhome()
 *
 *  Description:   Do a search for the home position.
 * 
 *  Arg(s) In:     Hexapode   ds     - 
 *                 DevVoid    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid     *argout - none
 *                 long        *error  - pointer to error code
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
static long  
dev_searchhome(ds, argin, argout, error)
Hexapode   ds;
DevVoid   *argin;
DevVoid   *argout;
long      *error;
{
   long        ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_searchhome()>\n");
#endif 

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_SEARCH_HOME;
       ret = ds_protocolWrite(error);
   }
   return(ds_protocolEnd(error));
}

/*****************************************************************************
 *  Function:      static long dev_nominalposition()
 *
 *  Description:   Get the position 
 * 
 *  Arg(s) In:     Hexapode           ds     - 
 *                 DevVoid           *argin  - none
 *              
 *  Arg(s) Out:    DevVarDoubleArray  *argout - Array with the values.
 *                 long              *error  - pointer to error code
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
static long  
dev_nominalposition(ds, argin, argout, error)
Hexapode            ds;
DevVoid            *argin;
DevVarDoubleArray  *argout;
long               *error;
{
  long     int i;
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_nominalposition()\n");
#endif 

   ret = ds_protocolStart(error);

   if ( ret == DS_OK ) {
       /*
        * Write data.
        */
      SD->Command = DEV_GET_INFO;
      ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK ) {
      /*
       * Read data.
       */
      for ( i = 0; i < 3; i++ ) {
          /*
           * Convert from m to mm and from rad to deg.
           */
          double_out[i]   = FromMtoMM(GD->NominalPosition[i]);
          double_out[i+3] = FromRADtoDEG(GD->NominalPosition[i+3]);
      }
      argout->length   = 6;
      argout->sequence = double_out;
   }
   return(ds_protocolEnd(error));
}

/*****************************************************************************
 *  Function:      static long dev_setrefsystem()
 *
 *  Description:   
 * 
 *  Arg(s) In:    Hexapode     ds     - 
 *                DevVoid     *argin  - 
 *              
 *  Arg(s) Out:   DevVoid     *argout - none
 *                long        *error  - pointer to error code
 *
 *  Error code(s):
 *
 *****************************************************************************/
static long  
dev_setrefsystem(ds, argin, argout, error)
Hexapode          ds;
DevVarDoubleArray *argin;
DevVoid          *argout;
long             *error;
{
   int i;
   long        ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_setrefsystem()>\n");
#endif

   CheckArgin(6);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_SET_REF_SYSTEM;
       for ( i = 0; i < 3; i++)
       {
          SD->NewRefSystem[i]   = FromMMtoM(argin->sequence[i]);
          SD->NewRefSystem[i+3] = FromDEGtoRAD(argin->sequence[i+3]);
       }

       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}


/*****************************************************************************
 *  Function:      static long dev_getrefsystem()
 *
 *  Description:   Returns the reference system in use.
 * 
 *  Arg(s) In:     Hexapode           ds     - 
 *                 DevVoid           *argin  - none
 *              
 *  Arg(s) Out:    DevVarDoubleArray  *argout - none
 *                 long              *error  - pointer to error code
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
static long  
dev_getrefsystem(ds, argin, argout, error)
Hexapode   ds;
DevVoid   *argin;
DevVarDoubleArray   *argout;
long     *error;
{

   long     int i;
   long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_getrefsystem\n");
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;

       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
      for ( i = 0; i < 3; i++ )
      {
          /*
           * Convert to mm, deg.
           */
          double_out[i]   = FromMtoMM(GD->ReferenceSystem[i]);
          double_out[i+3] = FromRADtoDEG(GD->ReferenceSystem[i + 3]);
      }
   
      argout->length = 6;
      argout->sequence = double_out;
   
   }
   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_setrefaxis()
 *
 *  Description:   Sets the main axis of the reference system.
 * 
 *  Arg(s) In:      Hexapode           ds     - 
 *                  DevVarDoubleArray  *argin  - none
 *              
 *  Arg(s) Out:     DevVoid           *argout - none
 *                  long              *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_setrefaxis(ds, argin, argout, error)
Hexapode          ds;
DevVarDoubleArray *argin;
DevVoid          *argout;
long             *error;
{

  long     int i;
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_setrefaxis()\n");
#endif 

   CheckArgin(8);
      
   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
      SD->Command = DEV_SET_REF_AXIS;
      for ( i = 0; i <3; i++ )
      {
          SD->OldDirection[i]   = FromMMtoM(argin->sequence[i]);
          SD->NewDirection[i+4]   = FromMMtoM(argin->sequence[i+4]);
      }
      SD->OldDirection[3]   = argin->sequence[3];
      SD->NewDirection[7]   = argin->sequence[7];
      ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_setreforigin()
 *
 *  Description:   Sets the origin of the reference system.
 * 
 *  Arg(s) In:     Hexapode           ds     - 
 *                 DevVarDoubleArray  *argin  - none
 *              
 *  Arg(s) Out:    DevVoid           *argout - none
 *                 long              *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_setreforigin(ds, argin, argout, error)
Hexapode          ds;
DevVarDoubleArray *argin;
DevVoid          *argout;
long             *error;
{

  long     int i;
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_setreforigin()\n");
#endif 
      
   CheckArgin(4);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
      SD->Command = DEV_SET_REF_ORIGIN;
      for (i = 0; i < 3; i++)
          SD->NewReferenceOrigin[i] = FromMMtoM(argin->sequence[i]);
      SD->NewReferenceOrigin[3]   = argin->sequence[3];
      ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_defaultreference()
 *
 *  Description:   Sets the current ref. system and position to the defaults.
 * 
 *  Arg(s) In:     Hexapode   ds     - 
 *                 DevVoid    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid    *argout - none
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_defaultreference(ds, argin, argout, error)
Hexapode   ds;
DevVoid   *argin;
DevVoid   *argout;
long      *error;
{
  long     int i;
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_defaultreference()>\n");
#endif 
      
   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
      SD->Command = DEV_DEF_REFERENCE;
      ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));

}

/*****************************************************************************
 *  Function:      static long dev_setrefposition()
 *
 *  Description:   
 * 
 *  Arg(s) In:    Hexapode           ds     - 
 *                DevVarDoubleArray  *argin  - New value. 
 *              
 *  Arg(s) Out:   DevVoid           *argout - none
 *                long              *error  - pointer to error code
 *
 *  Error code(s):
 *
 *****************************************************************************/
static long  
dev_setrefposition(ds, argin, argout, error)
Hexapode          ds;
DevVarDoubleArray *argin;
DevVoid          *argout;
long             *error;
{
   int i;
   long        ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_setrefposition()\n");
#endif

   CheckArgin(12);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_SET_REF_POSITION;
       for ( i = 0; i < 3; i++)
       {
          SD->OldPosition[i]   = FromMMtoM(argin->sequence[i]);
          SD->OldPosition[i+3] = FromDEGtoRAD(argin->sequence[i+3]);
          SD->NewPosition[i]   = FromMMtoM(argin->sequence[i+6]);
          SD->NewPosition[i+3] = FromDEGtoRAD(argin->sequence[i+9]);
       }

       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}


/*****************************************************************************
 *  Function:      static long dev_getrefposition()
 *
 *  Description:   Returns the reference position in use.
 * 
 *  Arg(s) In:     Hexapode           ds     - 
 *                 DevVoid           *argin  - none
 *              
 *  Arg(s) Out:    DevVarDoubleArray  *argout - none
 *                 long              *error  - pointer to error code
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
static long  
dev_getrefposition(ds, argin, argout, error)
Hexapode          ds;
DevVoid          *argin;
DevVarDoubleArray *argout;
long             *error;
{

   long     int i;
   long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_getrefposition()>\n");
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;

       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
      for ( i = 0; i < 3; i++ )
      {
          /*
           * Convert to mm, deg.
           */
          double_out[i]   = FromMtoMM(GD->ReferencePosition[i]);
          double_out[i+3] = FromRADtoDEG(GD->ReferencePosition[i+3]);
      }
   
      argout->length = 6;
      argout->sequence = double_out;
   
   }
   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_moveactuators()
 *
 *  Description:   Move the legs
 * 
 *  Arg(s) In:     Hexapode          ds     - 
 *                 DevVarDoubleArray *argin  - New length of the legs.
 *              
 *  Arg(s) Out:    DevVoid          *argout - none
 *                 long             *error  - pointer to error code
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
static long  
dev_moveactuators(ds, argin, argout, error)
Hexapode          ds;
DevVarDoubleArray *argin;
DevVoid          *argout;
long             *error;
{

   int i;
   long        ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_moveactuators()>\n");
#endif 

   CheckArgin(6);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_MOVE_ACTUATORS;
       for ( i = 0; i < 6; i++ )
       {
          SD->FinalActuatorLength[i] = FromMMtoM(argin->sequence[i]);
       }

       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_move()
 *
 *  Description:   Move the hexapode to a certain position
 * 
 *  Arg(s) In:     Hexapode             ds     - 
 *                 DevVarDoubleArray    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid    *argout - none
 *                 long       *error  - pointer to error code, in case routine 
 *                                      fails. 
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
static long  
dev_move(ds, argin, argout, error)
Hexapode   ds;
DevVarDoubleArray   *argin;
DevVoid   *argout;
long     *error;
{
   int i;
   long        ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_move()>\n");
#endif /*DEBUG_LVL1*/

   CheckArgin(6);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_MOVE_TABLE;
       for ( i = 0; i < 3; i++ )
       {
          SD->FinalPosition[i] = FromMMtoM(argin->sequence[i]);
          SD->FinalPosition[i+3] = FromDEGtoRAD(argin->sequence[i+3]);
       }

       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_rotate()
 *
 *  Description:   Rotate the hexapode. 
 * 
 *  Arg(s) In:     Hexapode             ds     - 
 *                 DevVarDoubleArray    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid             *argout - none
 *                 long                *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_rotate(ds, argin, argout, error)
Hexapode          ds;
DevVarDoubleArray *argin;
DevVoid          *argout;
long             *error;
{
   int i;
   long        ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_rotate()>\n");
#endif 

   CheckArgin(9);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_ROTATE_TABLE;
       for ( i = 0; i < 3; i++ )
       {
          SD->RotationOrigin[i] = FromMMtoM(argin->sequence[i]);
          SD->RotationAxis[i]   = FromMMtoM(argin->sequence[i+4]);
       }
       SD->RotationOrigin[3] = argin->sequence[3];
       SD->RotationAxis[3]   = argin->sequence[7];
       SD->RotationAngle = FromDEGtoRAD(argin->sequence[8]);

       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}


/*****************************************************************************
 *  Function:      static long dev_translate()
 *
 *  Description:   Translate the hexapode a given vector 
 * 
 *  Arg(s) In:     Hexapode             ds     - 
 *                 DevVarDoubleArray    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid             *argout - none
 *                 long                *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_translate(ds, argin, argout, error)
Hexapode   ds;
DevVarDoubleArray   *argin;
DevVoid   *argout;
long     *error;
{
   int i;
   long        ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_translate()>\n");
#endif /*DEBUG_LVL1*/

   CheckArgin(4);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_TRANSLATE_TABLE;
       for ( i = 0; i < 3; i++ )
       {
          SD->TranslationVector[i] = FromMMtoM(argin->sequence[i]);
       }
       SD->TranslationVector[3] = argin->sequence[3];

       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}


/*****************************************************************************
 *  Function:      static long dev_stop()
 *
 *  Description:   Cancels the movement
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid    *argout - none
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_stop(ds, argin, argout, error)
Hexapode   ds;
DevVoid   *argin;
DevVoid   *argout;
long      *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_stop()\n");
#endif 

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
      SD->Command = DEV_ABORT_MOVEMENT;
      ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}


/*****************************************************************************
 *  Function:      static long dev_checkposition()
 *
 *  Description:   Verifies if it is possible to reach a particular position. 
 * 
 *  Arg(s) In:     Hexapode             ds     - 
 *                 DevVarDoubleArray    *argin  - position to be cheched
 *              
 *  Arg(s) Out:    DevVarDoubleArray    *argout - corresponding lengths
 *                 long                 *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_checkposition(ds, argin, argout, error)
Hexapode   ds;
DevVarDoubleArray   *argin;
DevVarDoubleArray   *argout;
long     *error;
{
   int i;
   long        ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_checkposition()>\n");
#endif /*DEBUG_LVL1*/

   CheckArgin(6);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_CHECK_POSITION;
       for ( i = 0; i < 3; i++ )
       {
          SD->FinalPosition[i] = FromMMtoM(argin->sequence[i]);
          SD->FinalPosition[i+3] = FromDEGtoRAD(argin->sequence[i+3]);
       }

       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
      /*
       * Read data.
       */
      for ( i = 0; i < 6; i++ )
      {
          /*
           * Convert to mm.
           */
          double_out[i] = (double) FromMtoMM(SD->FinalActuatorLength[i]);
      }
   
      argout->length   = 6;
      argout->sequence = double_out;
   
   }
   return(ds_protocolEnd(error));
}


/*****************************************************************************
 *  Function:      static long dev_lasterror()
 *
 *  Description:   Return the error condition that changed the state
 *                 of the device to FAULT.
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - none
 *              
 *  Arg(s) Out:    DevLong    *argout - last error
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_lasterror(ds, argin, argout, error)
Hexapode   ds;
DevVoid   *argin;
DevLong   *argout;
long      *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_lasterror()\n");
#endif 

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;
       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
       *argout = SD->LastError;
   
   }
   return(ds_protocolEnd(error));
}

/*****************************************************************************
 *  Function:      static long dev_actuatorstatus()
 *
 *  Description:   Reads the status of each leg.
 * 
 *  Arg(s) In:     Hexapode         ds     - 
 *                 DevVoid         *argin  - none
 *              
 *  Arg(s) Out:    DevVarLongArray *argout - none
 *                 long            *error  - pointer to error code
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
static long  
dev_actuatorstatus(ds, argin, argout, error)
Hexapode            ds;
DevVoid            *argin;
DevVarLongArray    *argout;
long               *error;
{
  long     int i;
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_actuatorstatus()\n");
#endif 

      
   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
      SD->Command = DEV_GET_INFO;
      ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
      /*
       * Read data.
       */
      for ( i = 0; i < 6; i++ )
      {
          long_out[i] = SD->ActuatorStatus[i];
      }
   
      argout->length   = 6;
      argout->sequence = long_out;
   
   }
   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_setlengths()
 *
 *  Description:   Set the values for the lengths of the legs.
 * 
 *  Arg(s) In:    Hexapode              ds     - 
 *                DevVarDoubleArray     *argin  - 
 *              
 *  Arg(s) Out:   DevVoid              *argout - none
 *                long                 *error  - pointer to error code
 *
 *  Error code(s):
 *
 *****************************************************************************/
static long  
dev_setlengths(ds, argin, argout, error)
Hexapode          ds;
DevVarDoubleArray *argin;
DevVoid          *argout;
long             *error;
{
   int i;
   long        ret;

#ifdef DEBUG_LVL3
   printf("<HEXAPODE dev_setlengths()>\n");
#endif

   CheckArgin(6);

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_SET_ACT_LENGTH;
       for ( i = 0; i < 6; i++) {
          SD->NewActuatorLength[i] = FromMMtoM(argin->sequence[i]);
#ifdef DEBUG_LVL3
   printf("%d: %12.10f -> %12.10f\n", i+1, argin->sequence[i], SD->NewActuatorLength[i]);
#endif
       }

       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}

/*****************************************************************************
 *  Function:      static long dev_getlengths()
 *
 *  Description:   Reads the lengths of the legs.
 * 
 *  Arg(s) In:     Hexapode          ds     - 
 *                 DevVoid          *argin  - none
 *              
 *  Arg(s) Out:    DevVarDoubleArray *argout - none
 *                 long             *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_getlengths(ds, argin, argout, error)
Hexapode            ds;
DevVoid            *argin;
DevVarDoubleArray   *argout;
long               *error;
{
  long     int i;
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_getlengths()\n");
#endif 

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
      SD->Command = DEV_GET_INFO;
      ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
      /*
       * Read data.
       */
      for ( i = 0; i < 6; i++ )
      {
          /*
           * Convert to mm.
           */
          double_out[i] = (double) FromMtoMM(SD->ActuatorLength[i]);
      }
   
      argout->length   = 6;
      argout->sequence = double_out;
   
   }
   return(ds_protocolEnd(error));

}

/*****************************************************************************
 *  Function:      static long dev_newhomelengths()
 *
 *  Description:   Reads the lengths of the legs.
 * 
 *  Arg(s) In:     Hexapode          ds     - 
 *                 DevVoid          *argin  - none
 *              
 *  Arg(s) Out:    DevVarDoubleArray *argout - none
 *                 long             *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_newhomelengths(ds, argin, argout, error)
Hexapode            ds;
DevVoid            *argin;
DevVarDoubleArray   *argout;
long               *error;
{
  long     int i;
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_newhomelengths()\n");
#endif 

      
   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
      SD->Command = DEV_GET_INFO;
      ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
      /*
       * Read data.
       */
      for ( i = 0; i < 6; i++ )
      {
          /*
           * Convert to mm.
           */
          double_out[i] = (double) FromMtoMM(AD->HomeLengthFound[i]);
#ifdef DEBUG_LVL1
         printf("%d: %15.10f <- %15.10f\n",i+1, double_out[i], AD->HomeLengthFound[i]);
#endif 
          
      }
   
      argout->length   = 6;
      argout->sequence = double_out;
   
   }
   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_getposition()
 *
 *  Description:   Reads the position of the hexapode.
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - none
 *              
 *  Arg(s) Out:    DevVoid    *argout - none
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_getposition(ds, argin, argout, error)
Hexapode          ds;
DevVoid          *argin;
DevVarDoubleArray *argout;
long             *error;
{
  long     int i;
  long     ret;


#ifdef DEBUG_LVL3
   printf("<HEXAPODE> dev_getposition()\n");
#endif 

   ret = ds_protocolStart(error);

#ifdef DEBUG_LVL3
   printf("  - protocol started (%d)\n",ret);
#endif 

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
      SD->Command = DEV_GET_INFO;
      ret = ds_protocolWrite(error);
#ifdef DEBUG_LVL3
   printf("  - protocol wrote commnad (%d)\n",ret);
#endif 
   }

   if ( ret == DS_OK )
   {
      /*
       * Read data.
       */
      for ( i = 0; i < 3; i++ )
      {
          /*
           * Convert to mm, deg.
           */
          double_out[i] = (double) FromMtoMM(SD->Position[i]);
          double_out[i+3] = (double) FromRADtoDEG(SD->Position[i+3]);
      }
   
      argout->length = 6;
      argout->sequence = double_out;
   
   }

#ifdef DEBUG_LVL5
   printf("  - protocol ending \n");
#endif 
   ret = ds_protocolEnd(error);

#ifdef DEBUG_LVL5
   printf("  - protocol ended (%d)\n",ret);
#endif 

   return(ret);

}


/*****************************************************************************
 *  Function:      static long dev_setmode()
 *
 *  Description:   Changes the movement mode of the hexapode.
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevLong    *argin  - new mode
 *              
 *  Arg(s) Out:    DevVoid    *argout - none
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_setmode(ds, argin, argout, error)
Hexapode          ds;
DevLong          *argin;
DevVoid          *argout;
long             *error;
{
  long     ret;
  long     mode;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE dev_setmode()>\n");
#endif

   mode = *argin;

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_SET_MODE;
       SD->NewMovementMode = mode; 
       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));

}

/*****************************************************************************
 *  Function:      static long dev_getmode()
 *
 *  Description:   Returns the movement mode of the hexapode.
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - 
 *              
 *  Arg(s) Out:    DevLong    *argout - movement mode
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_getmode(ds, argin, argout, error)
Hexapode          ds;
DevVoid          *argin;
DevLong          *argout;
long             *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_getmode()\n");
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;
       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
       *argout = SD->MovementMode;
   }

   return(ds_protocolEnd(error));

}


/*****************************************************************************
 *  Function:      static long dev_description()
 *
 *  Description:   Returns a string describing the device.
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - none
 *              
 *  Arg(s) Out:    DevString  *argout - Description string.
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_description(ds, argin, argout, error)
Hexapode            ds;
DevVoid            *argin;
DevString          *argout;
long               *error;
{
#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_description()\n");
#endif 


   *argout = (DevString)ds->hexapode.description;

   return(DS_OK);
}

/*****************************************************************************
 *  Function:      static long dev_getvelocity()
 *
 *  Description:   
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - 
 *              
 *  Arg(s) Out:    DevFloat   *argout - 
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_getvelocity(ds, argin, argout, error)
Hexapode          ds;
DevVoid          *argin;
DevFloat         *argout;
long             *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_getvelocity()\n");
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;
       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
       *argout = 100 * AD->Velocity;
   }

   return(ds_protocolEnd(error));

}

/*****************************************************************************
 *  Function:      static long dev_setvelocity()
 *
 *  Description:   
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevFloat   *argin  - 
 *              
 *  Arg(s) Out:    DevVoid    *argout - 
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_setvelocity(ds, argin, argout, error)
Hexapode          ds;
DevFloat         *argin;
DevVoid          *argout;
long             *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_setvelocity() - %f\n", *argin);
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_SET_VELOCITY;
       SD->NewVelocity = *argin / 100.;
       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));

}






/*****************************************************************************
 *****************************************************************************/
static long  
dev_setdebug(ds, argin, argout, error)
Hexapode          ds;
DevLong          *argin;
DevVoid          *argout;
long             *error;
{

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_setdebug() - %d\n", *argin);
#endif

   if ( AD->MotorData.MotorType == ICEPAP ) 
   {
    AD->MotorData.MotorPrivate.icepap.debug = *argin;
    return(DS_OK);
   }
   else
   {
    *error = DevErr_FunctionNotImplemented;
    return(DS_NOTOK);
   }
}






/*****************************************************************************
 *****************************************************************************/
static long  
dev_setlibdebug(ds, argin, argout, error)
Hexapode          ds;
DevLong          *argin;
DevVoid          *argout;
long             *error;
{
#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_setlibdebug() - %d\n", *argin);
#endif

   *error = DevErr_FunctionNotImplemented;
   return(DS_NOTOK);
}






/*****************************************************************************
 *  Function:      static long dev_getresource()
 *
 *  Description:   
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevString   *argin - Resource name
 *              
 *  Arg(s) Out:    DevString  *argout - Resource value in string format
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_getresource(ds, argin, argout, error)
Hexapode          ds;
DevString        *argin;
DevString        *argout;
long             *error;
{
   long         ret;
   static char  *value;
   db_resource  reso;
   int          i;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_getresource() \"%s\"\n", *argin);
#endif

   value = (char *) malloc(80 * sizeof(char));

   reso.resource_name = *argin;
   reso.resource_type = D_STRING_TYPE;
   reso.resource_adr  = &value;

   /*
    * Check the resource exists.
    */

   for ( i = 0; i < res_object_size ; i++)
   {
      if (strcmp(reso.resource_name,res_object[i].resource_name) == 0 ) break;
   }

   if ( i == res_object_size )
   {
       *error = DevErr_InvalidResourceString;
        return(DS_NOTOK);
   }
 
   if(db_getresource(ds->devserver.name, &reso, 1, error))
   {
     *error = DevErr_DatabaseRead;
     return(DS_NOTOK);
   }

   *argout = value;
   return(DS_OK);

}

/*****************************************************************************
 *  Function:      static long dev_setresource()
 *
 *  Description:   
 * 
 *  Arg(s) In:     Hexapode           ds     - 
 *                 DevVarStringArray *argin  - 
 *              
 *  Arg(s) Out:    DevVoid           *argout - 
 *                 long              *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_setresource(ds, argin, argout, error)
Hexapode           ds;
DevVarStringArray *argin;
DevVoid           *argout;
long              *error;
{
  DevString res_name;
  DevString res_value;
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_setresource()\n");
#endif

   *error = DevErr_CommandNotImplemented;
   return(NOT_OK);

   if (argin->length != 2){
       *error = DevErr_DeviceIllegalParameter;
   }
   res_name = argin->sequence[0];
   res_value = argin->sequence[1];

   /*
    *   Here check if resource name is valid : (to be implemented)
    */

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write the new value in shared memory (to be implemented)
        * and execute a soft reset.
        */

       SD->Command = DEV_RESET;
       SD->ResetMode = SOFT_RESET;
       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}

/*****************************************************************************
 *  Function:      static long dev_getmovresolution()
 *
 *  Description:   
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - 
 *              
 *  Arg(s) Out:    DevFloat   *argout - 
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_getmovresolution(ds, argin, argout, error)
Hexapode          ds;
DevVoid          *argin;
DevDouble        *argout;
long             *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_getmovresolution()\n");
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;
       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
       *argout = GD->MovementResolution;
   }

   return(ds_protocolEnd(error));
}

/*****************************************************************************
 *  Function:      static long dev_setmovresolution()
 *
 *  Description:   
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevFloat   *argin  - 
 *              
 *  Arg(s) Out:    DevVoid    *argout - 
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_setmovresolution(ds, argin, argout, error)
Hexapode          ds;
DevDouble        *argin;
DevVoid          *argout;
long             *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_setmovresolution() - %g\n", *argin);
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_SET_RESOLUTION;
       SD->NewMovementResolution = *argin;
       ret = ds_protocolWrite(error);
   }

   return(ds_protocolEnd(error));
}

/*****************************************************************************
 *  Function:      static long dev_mechresolution()
 *
 *  Description:   
 * 
 *  Arg(s) In:     Hexapode    ds     - 
 *                 DevVoid    *argin  - 
 *              
 *  Arg(s) Out:    DevFloat   *argout - 
 *                 long       *error  - pointer to error code
 *
 *  Error code(s): 
 *
 *****************************************************************************/
static long  
dev_mechresolution(ds, argin, argout, error)
Hexapode          ds;
DevVoid          *argin;
DevDouble        *argout;
long             *error;
{
  long     ret;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dev_mechresolution()\n");
#endif

   ret = ds_protocolStart(error);

   if ( ret == DS_OK )
   {
       /*
        * Write data.
        */
       SD->Command = DEV_GET_INFO;
       ret = ds_protocolWrite(error);
   }

   if ( ret == DS_OK )
   {
       /*
        * Read data.
        */
       *argout = FromMtoMM(GD->MechResolution);
   }

   return(ds_protocolEnd(error));
}

/*****************************************************************************
 *
 *  Function:     sigKill()
 *
 *  Description:   signal handler for killing signal
 * 
 *  Error code(s): 
 *
 *****************************************************************************/
void sigKill()
{
   long i;

#ifdef linux
   pid_t  stop_pid;
   int    stop_status;
#endif

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> dying with the boots on sigKill\n");
#endif
   
   for ( i =0; i < no_dev; i++)
   {
        printf("<Killing hexapito number> %d\n",i+1);

        printf("%d,%d\n",pidArray[i],shKeyArray[i]);
        kill(pidArray[i], SIGINT);

#ifdef linux
        /* wait a little bit to allow hexapito killing itself */
        usleep(100000);
#endif
        stop_pid = waitpid(pidArray[i],&stop_status,WUNTRACED);

        if (stop_pid == -1)
          printf("<HEXAPODE> Panic killing Hexapito\n");
         
        if((WIFEXITED(stop_status) != 0 ) && WEXITSTATUS(stop_status) != 0) {
          printf("<HEXAPODE> Hexapito exited with code %d\n",WEXITSTATUS(stop_status));
        }

        if(WIFSIGNALED(stop_status)) {
            printf("<HEXAPODE> Hexapito died with signal %d\n",WTERMSIG(stop_status));
        } else {
            printf("<HEXAPODE> Hexapito properly stopped\n");
        }

        modremove(shKeyArray[i]);
   }
        
   exit(0);
}


/*****************************************************************************
 *
 *  Functions:     sigAckn(), sigAccept(), sigSuccess(), sigError()
 *
 *  Description:   signal handlers for subprocess handshaking
 * 
 *  Error code(s): 
 *
 *****************************************************************************/
void sigAckn()
{
   handshaking = COMMAND_END_ACKN;
}


void sigAccept()
{
   handshaking = COMMAND_ACCEPTED;
}


void sigSuccess()
{
   handshaking = COMMAND_SUCCESFUL;
}


void sigError()
{
   handshaking = COMMAND_ERROR;
}


/*****************************************************************************
 *  Functions:     ds_protocolStart()
 *
 *  Description:   
 * 
 *  Error code(s): 
 *
 *****************************************************************************/
long ds_protocolStart(error)
long *error;
{
#ifdef DEBUG_LVL1
   printf("<HEXAPODE> ds_protocolStart()\n");
#endif 
   handshaking = NO_COMMAND;

#ifdef MICROSLEEP
   usleep(PR_MICROSLEEP);
#endif
   kill(SD->pid,COMMAND_REQUEST);
   sleep(PR_TIMEOUT); 
 
   /* first try sleeping 
   if (handshaking != COMMAND_ACCEPTED)
        sleep(PR_TIMEOUT);
   */

   /* if not yet responded... error */
   if (handshaking != COMMAND_ACCEPTED)
   {
       *error = DevErr_DeviceTimedOut;
       return(DS_NOTOK);
   }

   return(DS_OK);

}


/*****************************************************************************
 *  Functions:     ds_protocolWrite()
 *
 *  Description:   
 * 
 *  Error code(s): 
 *
 *****************************************************************************/
long ds_protocolWrite(error)
long *error;
{
#ifdef DEBUG_LVL1
   printf("<HEXAPODE> ds_protocolWrite\n");
#endif 

#ifdef MICROSLEEP
   usleep(PR_MICROSLEEP);
#endif
      kill(SD->pid,COMMAND_EXECUTE);
      sleep(PR_TIMEOUT); 
   
      /* if ((handshaking != COMMAND_SUCCESFUL) &&
               (handshaking != COMMAND_ERROR) ) 
          sleep(PR_TIMEOUT);
      */

      if ((handshaking != COMMAND_SUCCESFUL) &&
               (handshaking != COMMAND_ERROR) ) 
      {
              *error = DevErr_DeviceTimedOut;
              return(DS_NOTOK);
      }

      return(DS_OK);
}


/*****************************************************************************
 *  Function:     ds_protocolEnd()
 *
 *  Description:   
 * 
 *  Error code(s): 
 *
 *****************************************************************************/
long ds_protocolEnd(error)
long *error;
{
      long ret = DS_OK;

#ifdef DEBUG_LVL1
   printf("<HEXAPODE> ds_protocolEnd\n");
#endif 

      if ( handshaking == COMMAND_ERROR )
      {
         *error = SD->Error;
         ret = DS_NOTOK;
      }

      if ((handshaking == COMMAND_ERROR) || 
          (handshaking == COMMAND_SUCCESFUL))
      {


#ifdef MICROSLEEP
   usleep(PR_MICROSLEEP);
#endif
          kill(SD->pid,COMMAND_COMPLETED);
          sleep(PR_TIMEOUT); 

          /* if (handshaking != COMMAND_END_ACKN )
               sleep(PR_TIMEOUT);
          */

          if (handshaking != COMMAND_END_ACKN )
          {
              *error = DevErr_DeviceTimedOut;
              ret = DS_NOTOK;
          }
      } else { 
          ret = DS_NOTOK; 
      }

      return(ret);
}

/*****************************************************************************
 *  Function:     print_resources()
 *
 *  Description:
 *
 *  Error code(s):
 *
 *****************************************************************************/
void print_resources(res, n)
db_resource    *res;
int            n;
{
#ifdef DEBUG_LVL3
    printf("\t\t(resource name)\t->\t(resource value)\n");

    for(; n > 0; n--, res++) {
        printf("\t\t%s\t->\t", res->resource_name);

        if (res->resource_adr == NULL) {
            printf("--ERROR--\n");
            continue;
        }

        switch (res->resource_type) {

         case D_BOOLEAN_TYPE:
           printf("%s\n", *((DevBoolean *)res->resource_adr)? "True" : "False");
           break;

         case D_SHORT_TYPE:
            printf("%d\n",  *((DevShort *)res->resource_adr));
            break;

         case D_LONG_TYPE:
            printf("%d\n",  *((DevLong *)res->resource_adr));
            break;

         case D_FLOAT_TYPE:
            printf("%f\n",  *((DevFloat *)res->resource_adr));
            break;

         case D_DOUBLE_TYPE:
            printf("%f\n",  *((DevDouble *)res->resource_adr));
            break;

         case D_STRING_TYPE:
            printf("'%s'\n",  *((DevString *)res->resource_adr));
            break;

         default:
            printf("--WRONG TYPE--\n");
        }
    }
#endif
}
