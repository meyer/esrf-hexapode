/*********************************************************************
 *
 * File:       Geometry.c
 *
 * Project:    Hexapito (Hexapode control program)
 *
 * Purpose :   Provide a function set that implements all the geometric
 *             calculations required to control the hexapode.
 *             
 * Author(s):  Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Date:       01/06/93
 *
 * Copyright (c) 1993 European Synchrotron Radiation Facility,
 *                    Grenoble, France
 *
 *                   All Rights Reserved
 *
 **********************************************************************
 *
 *  Remarks:  - Units are always meters and radians.
 *
 *            - 5 different referece systems are used:
 *                FRS (Fixed frame ref. syst.) 
 *                MRS (Moving frame ref. syst.) 
 *                LRS (Laboratory ref. syst.) 
 *                IRS (Instrument ref. syst.) 
 *                ARS (Auxiliary ref. syst.) 
 *
 *
 *
 *
 **********************************************************************/

/*
#define DEBUG_LVL1
#define DEBUG_LVL2
#define DEBUG_LVL3
#define DEBUG_LVL4
*/

#include <math.h>
#include <Hexapito.h>

#define TINY_VALUE 1E-15

long GeometryInit();
long SetDefaultReference();
long NormalVectorExtraction();
long UpdateReference();
long SetAuxRefSystem();
long VectorToLRS();
long PointToLRS();
long VectorToFRS();
long PointToFRS();
void VectorIRStoLRS();
void PointIRStoLRS();
void VectorFRStoLRS();
void PointFRStoLRS();
void VectorLRStoFRS();
void PointLRStoFRS();
long PositionToL();
void CalculateQ();
void CalculateInvQ();
long QToL();
long CalculateL_Top0();
long CalculateL_Top1();
long PhiToL();
long ZToL();
long LToPosition();
long CalculateJ();
long CalculatedL();
long SolveLinear();
long CalculateR();
void RotateVector();
void ReduceAngles();
double Normalize();
long CheckLengths();
long CheckTilt();
long CheckPosition();
long CheckAll();
long SetActuatorTrajectory();
long SetTableTrajectory();
long SetRotationTrajectory();
long SetTraslationTrajectory();
long CheckTrajectory();
long ActuatorTrajectory();
long TableTrajectory();
long RotationTrajectory();
long TraslationTrajectory();
long ExtractOrientation();
float DotProduct();
void VectorialProduct();
void CenterOfMass();
void CopyVector();
long SetReferenceAxis();
long SetReferenceOrigin();
long SetReferenceSystem();
long SetReferencePosition();

extern GeometryData	*GD;
extern SharedData       *SD;

double  TypicalLength;
double	SquaredLinkLength[6];
double	FixedP[6][3], MovingP[6][3];
double	AuxFixedP[6][3], AuxMovingP[6][3];
double	FixedNormal[3];
double	MovingNormal[3];
double	cosMaxTiltAngle;
long		(*CalculateL)();

long		(*Trajectory)();
double	Delta_t;
double	StartLength[6], DeltaLength[6];
double	StartPosition[6], DeltaPosition[6];
double	DeltaAngle;
double	DeltaVector;

/*************************
 *  GeometryInit()
 **************************************************************/
/*
 *  Purpose:  Initializes the reference system and other
 *            parameters that depend on them.
 *            
 *  Remarks: - This function must be called once before any
 *             other geometric rutine.
 *
 *  Returns:  OK      If no error
 *            NOTOK   If error
 *
 *  Error Codes:
 *               
 *
 **************************************************************/
long GeometryInit()
{
   double   *TypicalSize;
	int      i;

#ifdef DEBUG_LVL4
	printf("<GEOMETRY> Geometry Init\n");
#endif

	cosMaxTiltAngle = cos(GD->MaxTiltAngle);
	switch(GD->Topology){
		case SUPPORT:
			CalculateL  = CalculateL_Top0;
                        TypicalSize = GD->NominalLength;
			break;

		case MANIPULATOR:
			CalculateL  = CalculateL_Top1;
                        TypicalSize = GD->LinkLength;
                        for (i = 0; i < 6; i++)
                             SquaredLinkLength[i] = GD->LinkLength[i] * 
                                                    GD->LinkLength[i];
			break;

		default:
			SD->Error = DevErr_BadTopology;
			return(NOT_OK);
	}

        TypicalLength = 0;
        for (i = 0; i < 6; i++)
           TypicalLength += TypicalSize[i];
        TypicalLength /= 6.;

#ifdef DEBUG_LVL4
        printf("<GEOMETRY> Typical Length = %g m\n", TypicalLength);
#endif

	return(SetDefaultReference());
}

/*************************
 *  SetDefaultReference()
 **************************************************************/
/*
 *  Purpose:  Updates the static variables that define the 
 *            reference system and reference position with the
 *            default values, and initializes other parameters
 *            that depend on them.
 *            
 *  Remarks: - This function must be called before doing any
 *             geometric calculation.
 *
 *  Returns:  OK      If no error
 *            NOTOK   If error
 *
 *  Error Codes:
 *               
 *
 **************************************************************/
long SetDefaultReference()
{
	int	i, j;
	double	aux;

#ifdef DEBUG_LVL4
   printf("<GEOMETRY> Set Default Reference Sytem\n");
#endif

for(i = 0; i < 6; i++){
		GD->ReferenceSystem[i] = GD->DefaultReferenceSystem[i];
		GD->ReferencePosition[i] = GD->DefaultReferencePosition[i];
	}
	return(UpdateReference(GD->ReferenceSystem,
					GD->ReferencePosition));
}

/*****************************
 *  NormalVectorExtraction()
 **************************************************************/
/*
 *  Purpose:  Calculates an unitary vector normal to one of the
 *            joint planes (Fixed or Moving) in the same reference
 *            system in which the points are defined.
 *            
 *  Remarks: - This function assumes that the joint points are
 *             in a certain order.
 *
 *  Returns:  nothing
 *
 *  Input Parameter(s):
 *            PointSet - pointer to the array of six joint points
 *
 *  Output Parameter(s):
 *            Normal - pointer to the calculated normal vector
 *
 *  Error Codes:
 *               
 *
 **************************************************************/
long NormalVectorExtraction(PointSet, Normal)
	double	PointSet[6][3];
	double	Normal[3];
{
	double	v0[3], v1[3];
	double	vector[3];
	int	i;

#ifdef DEBUG_LVL1
   printf("<GEOMETRY> Normal Vector Extraction\n");
#endif

	for (i = 0; i < 3; i++){
		v0[i] = PointSet[2][i] - PointSet[0][i];
		v1[i] = PointSet[4][i] - PointSet[0][i];
	}

	VectorialProduct(v0, v1, vector);
	if (Normalize(vector, Normal) < TINY_VALUE){
		SD->Error = DevErr_ColinearJointPoints;
		return(NOT_OK);
	} else
		return(OK);
}

/*****************************
 *  UpdateReference()
 **************************************************************/
/*
 *  Purpose:  Calculates the coordinates of the joint points and
 *            other paremeters in a particular reference system
 *            and for a particular reference position of the
 *            instrument, and updates the corresponding static
 *            variables.
 *            
 *  Remarks: - This function must be called when either the
 *             reference system or the reference position is
 *             modified and before doing any new geometric
 *             calculation.
 *
 *  Returns:  OK      If no error
 *            NOTOK   If error
 *
 *  Input Parameter(s):
 *            Rs - new Laboratory Reference System defined in the FRS
 *            Rp - new reference position defined in (FRS,MRS)
 *
 *  Output Parameter(s): (static global variables)
 *            FixedP - coordinates of the fixed joint points in
 *                    the new reference system (LRS)
 *            MovingP - coordinates of the moving joint points in
 *                     the new reference system (IRS)
 *            FixedNormal - normal vector to the fixed frame in the LRS
 *            MovingNormal - normal vector to the moving frame in the IRS
 *            NominalPosition - Nominal position of the instrument 
 *                              in the (LRS,IRS)
 *
 *  Error Codes:
 *               
 *
 **************************************************************/
long UpdateReference(Rs, Rp)
	double	Rs[6];			/* New Reference System   */
	double	Rp[6];			/* New Reference Position */
{
	int	i, j, k;
	double	Qp[3][3], InvQs[3][3];
	double	len;
	double	SqActProj;
	double	aux[3], auxbis;
        double  FixedCOM[3], MovingCOM[3];
	double	L[6];

#ifdef DEBUG_LVL4
   printf("<GEOMETRY> Update Ref. System\n");
#endif
	
        ReduceAngles(Rs);
        ReduceAngles(Rp);

	CalculateQ(Qp, Rp[phi], Rp[theta], Rp[psi]); 

	CalculateInvQ(InvQs, Rs[phi], Rs[theta], Rs[psi]); 

	for(i = 0; i < 6; i++){
		for(j = 0; j < 3; j++){
			FixedP[i][j] = 0;
			for(k = 0; k < 3; k++)
				FixedP[i][j] += InvQs[j][k] *
					(GD->FixedPoint[i][k] - Rs[k]);
		}
	}

	for(i = 0; i < 6; i++){
		for(j = 0; j < 3; j++){
			aux[j] = Rp[j] - Rs[j];
			MovingP[i][j] = GD->MovingPoint[i][j];
			for(k = 0; k < 3; k++)
				aux[j] += Qp[j][k] * GD->MovingPoint[i][k];
		}

		for(j = 0; j < 3; j++){
			MovingP[i][j] = 0;
			for(k = 0; k < 3; k++)
				MovingP[i][j] += InvQs[j][k] * aux[k];
		}
	}

#ifdef DEBUG_LVL3
	for(i = 0; i < 6; i++)
		printf("(%10.7f, %10.7f, %10.7f) - (%10.7f, %10.7f, %10.7f)\n",
		   GD->FixedPoint[i][x],GD->FixedPoint[i][y],GD->FixedPoint[i][z],
		   FixedP[i][x],FixedP[i][y],FixedP[i][z]);
	printf("\n");
	for(i = 0; i < 6; i++)
		printf("(%10.7f, %10.7f, %10.7f) - (%10.7f, %10.7f, %10.7f)\n",
	        GD->MovingPoint[i][x],GD->MovingPoint[i][y],GD->MovingPoint[i][z],
		   MovingP[i][x],MovingP[i][y],MovingP[i][z]);
	printf("\n");
#endif

	if (NormalVectorExtraction(FixedP, FixedNormal) != OK ||
	    NormalVectorExtraction(MovingP, MovingNormal) != OK)
		return(NOT_OK);

        /* begin modif by P.Mangiagalli */

        /*
	len = (GD->MinActuatorLength + GD->MaxActuatorLength) / 2;
        */
        len = GD->NominalLength[0];

        /* end modif */

	len = len * len;

	for (i = 0; i < 3; i++){
		auxbis = GD->MovingPoint[0][i] - GD->FixedPoint[0][i];
		len -= auxbis *auxbis;
	}

	if (len > 0)
		len = sqrt(len);
	else
		len = 0;
 
        /* begin modif by P.Mangiagalli */
        CenterOfMass(FixedP,   FixedCOM);
        CenterOfMass(MovingP,  MovingCOM);
	for(i = 0; i < 3; i++){
           GD->NominalPosition[i] = FixedCOM[i] + len * FixedNormal[i]
                                                - MovingCOM[i];
        }

        /*
	for(i = 0; i < 3; i++){
		auxbis = 0;
		for(j = 0; j < 3; j++)
			auxbis += InvQs[i][j] * Rp[j];
		GD->NominalPosition[i] = -auxbis + len * FixedNormal[i];
	}
        */

        /* end modif */

	GD->NominalPosition[phi] = GD->NominalPosition[theta] = GD->NominalPosition[psi] = 0;

#ifdef DEBUG_LVL2
   printf("  - set ref system returns after checkall\n");
#endif

	return (CheckAll(GD->NominalLength, GD->NominalPosition));
}

/*****************************
 *  SetAuxRefSystem()
 **************************************************************/
/*
 *  Purpose:  Calculates the coordinates of the joint points in
 *            an auxiliary reference system (ARS) that is used
 *            only to speed up trajectory calculations in
 *            rotation and translation movements.
 *            
 *  Remarks: - This function must be called before any call to
 *             PhiToL() or ZToL() in order to prepare the
 *             auxiliary reference system.
 *           - The auxiliary reference system is chosen to have
 *             the origin in a given point and the z-axis
 *             along a certain direction defined in the LRS.
 *
 *  Returns:  OK      If no error
 *            NOTOK   If error
 *
 *  Input Parameter(s):
 *            Pos - Instrument position in (LRS, IRS)
 *            NewOrigin - ARS origin defined in the LRS
 *            NewZAxis - ARS z direction defined in the LRS
 *
 *  Output Parameter(s): (static global variables)
 *            AuxFixedP - coordinates of the fixed joint points in
 *                       the new auxiliary system (ARS)
 *            AuxMovingP - coordinates of the moving joint points in
 *                        the new auxiliary system (IRS) when the
 *                        intrument is in the Pos position.
 *
 *  Error Codes:
 *               
 *
 **************************************************************/
long SetAuxRefSystem(Pos, NewOrigin, NewZAxis)
	double Pos[6];
	double NewOrigin[3];
	double NewZAxis[3];
{
	double	R[3][3];
	double	AuxVector[3];
	int	i, j, k;

#ifdef DEBUG_LVL2
   printf("<GEOMETRY> Set Aux. Ref. System\n");
#endif

AuxVector[x] = 0;
	AuxVector[y] = 0;
	AuxVector[z] = 1.;

	if (CalculateR(R, NewZAxis, AuxVector) != OK)
		return(NOT_OK);

	for(i = 0; i < 6; i++){

		for(j = 0; j < 3; j++)
			AuxVector[j] = FixedP[i][j] - NewOrigin[j];
		RotateVector(R, AuxVector, AuxFixedP[i]);

	
		PointIRStoLRS(Pos, MovingP[i], AuxVector);
		for(j = 0; j < 3; j++)
			AuxVector[j] -= NewOrigin[j];
		RotateVector(R, AuxVector, AuxMovingP[i]);
	}

	return (OK);
}

/*****************************
 *  VectorToLRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the LRS coordinates of a vector
 *            defined in any of the refence systems when the
 *            instrument is at the current position.
 *            
 *  Remarks: 
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Pos - instrument position in (LRS,IRS)
 *            Vector - generalized vector defined in LRS, IRS
 *		       or FRS.
 *
 *  Output Parameter(s):
 *            Vector - vector defined in the LRS
 *
 **************************************************************/
long VectorToLRS(Pos, Vector)
	double	Pos[6];
	double	Vector[4];
{
	double	NewVector[3];

#ifdef DEBUG_LVL2
   printf("\t\t\tVector to LRS\n");
#endif

	if (Vector[refs] == LRS)
		return(OK);
	else if (Vector[refs] == IRS)
		VectorIRStoLRS(Pos, Vector, NewVector);
	else if (Vector[refs] == FRS)
		VectorFRStoLRS(Vector, NewVector);
	else {
		SD->Error = DevErr_InvalidReferenceSystem;
		return(NOT_OK);
	}
	CopyVector(3, NewVector, Vector);
	Vector[refs] = LRS;
	return(OK);
}

/*****************************
 *  PointToLRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the LRS coordinates of a point
 *            defined in any of the refence systems when the
 *            instrument is at the current position.
 *            
 *  Remarks: 
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Pos - instrument position in (LRS,IRS)
 *            Point - generalized point defined in LRS, IRS
 *		       or FRS.
 *  Output Parameter(s):
 *            Point - point defined in the LRS
 *
 **************************************************************/
long PointToLRS(Pos, Point)
	double	Pos[6];
	double	Point[4];
{
	double	NewPoint[3];

#ifdef DEBUG_LVL2
   printf("\t\t\tPoint to LRS\n");
#endif

	if (Point[refs] == LRS)
		return(OK);
	else if (Point[refs] == IRS)
		PointIRStoLRS(Pos, Point, NewPoint);
	else if (Point[refs] == FRS)
		PointFRStoLRS(Point, NewPoint);
	else {
		SD->Error = DevErr_InvalidReferenceSystem;
		return(NOT_OK);
	}

	CopyVector(3, NewPoint, Point);
	Point[refs] = LRS;
	return(OK);
}

/*****************************
 *  VectorToFRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the FRS coordinates of a vector
 *            defined in any of the refence systems when the
 *            instrument is at a given position.
 *            
 *  Remarks: 
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Pos - instrument position in (LRS,IRS)
 *            Vector - generalized vector defined in LRS, IRS
 *		       or FRS.
 *
 *  Output Parameter(s):
 *            Vector - vector defined in the FRS
 *
 **************************************************************/
long VectorToFRS(Pos, Vector)
	double	Pos[6];
	double	Vector[4];
{
	double	NewVector[3];

#ifdef DEBUG_LVL2
   printf("\t\t\tVector to FRS\n");
#endif

	if (Vector[refs] == LRS)
		VectorLRStoFRS(Vector, NewVector);
	else if (Vector[refs] == IRS){
		VectorToLRS(Pos, Vector);
		VectorLRStoFRS(Vector, NewVector);
	} else if (Vector[refs] == FRS)
		return(OK);
	else {
		SD->Error = DevErr_InvalidReferenceSystem;
		return(NOT_OK);
	}
	CopyVector(3, NewVector, Vector);
	Vector[refs] = FRS;
	return(OK);
}

/*****************************
 *  PointToFRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the FRS coordinates of a point
 *            defined in any of the refence systems when the
 *            instrument is at a given position.
 *            
 *  Remarks: 
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Pos - instrument position in (LRS,IRS)
 *            Point - generalized point defined in LRS, IRS
 *		       or FRS.
 *
 *  Output Parameter(s):
 *            Point - point defined in the FRS
 *
 **************************************************************/
long PointToFRS(Pos, Point)
	double	Pos[6];
	double	Point[4];
{
	double	NewPoint[3];

#ifdef DEBUG_LVL2
   printf("\t\t\tPoint to FRS (%1f)\n", Point[refs]);
#endif

	if (Point[refs] == LRS)
		PointLRStoFRS(Point, NewPoint);
	else if (Point[refs] == IRS){
		PointToLRS(Pos, Point);
		PointLRStoFRS(Point, NewPoint);
	} else if (Point[refs] == FRS)
		return(OK);
	else {
		SD->Error = DevErr_InvalidReferenceSystem;
		return(NOT_OK);
	}
	CopyVector(3, NewPoint, Point);
	Point[refs] = FRS;
	return(OK);
}

/*****************************
 *  VectorIRStoLRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the LRS coordinates of a vector
 *            defined in the IRS when the instrument is at
 *            particular position.
 *            
 *  Input Parameter(s):
 *            Pos - instrument position in (LRS,IRS)
 *            VIRS - vector defined in the IRS
 *
 *  Output Parameter(s):
 *            VLRS - vector defined in the LRS
 *
 **************************************************************/
void VectorIRStoLRS(Pos, VIRS, VLRS)
	double	Pos[6];
	double  VIRS[3];
	double  VLRS[3];
{
	double	Q[3][3];
	int	i, j;
	
	CalculateQ(Q, Pos[phi], Pos[theta], Pos[psi]);

	RotateVector(Q, VIRS, VLRS);
}

/*****************************
 *  PointIRStoLRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the LRS coordinates of a point
 *            defined in the IRS when the instrument is at
 *            particular position.
 *            
 *  Input Parameter(s):
 *            Pos - instrument position in (LRS,IRS)
 *            PIRS - point defined in the IRS
 *
 *  Output Parameter(s):
 *            PLRS - point defined in the LRS
 *
 **************************************************************/
void PointIRStoLRS(Pos, PIRS, PLRS)
	double	Pos[6];
	double  PIRS[3];
	double  PLRS[3];
{
	int	i;

	VectorIRStoLRS(Pos, PIRS, PLRS);
	for(i = 0; i < 3; i++)
		PLRS[i] += Pos[i];
}

/*****************************
 *  VectorFRStoLRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the LRS coordinates of a vector
 *            defined in the FRS.
 *
 *  Input Parameter(s):
 *            VFRS - vector defined in the FRS
 *
 *  Output Parameter(s):
 *            VLRS - vector defined in the LRS
 *
 **************************************************************/
void VectorFRStoLRS(VFRS, VLRS)
	double  VFRS[3];
	double  VLRS[3];
{
	double	InvQs[3][3];
	int	i, j;
	
	CalculateInvQ(InvQs, GD->ReferenceSystem[phi],
			GD->ReferenceSystem[theta], GD->ReferenceSystem[psi]);

	RotateVector(InvQs, VFRS, VLRS);
}


/*****************************
 *  PointFRStoLRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the LRS coordinates of a point
 *            defined in the FRS.
 *            
 *  Remarks: 
 *
 *  Returns:  nothing
 *
 *  Input Parameter(s):
 *            PFRS - point defined in the IRS
 *
 *  Output Parameter(s):
 *            PLRS - point defined in the LRS
 *
 **************************************************************/
void PointFRStoLRS(PFRS, PLRS)
	double  PFRS[3];
	double  PLRS[3];
{
	int	i;

	VectorFRStoLRS(PFRS, PLRS);
	for(i = 0; i < 3; i++)
		PLRS[i] -= GD->ReferenceSystem[i];
}

/*****************************
 *  VectorLRStoFRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the FRS coordinates of a vector
 *            defined in the LRS.
 *            
 *  Remarks: 
 *
 *  Returns:  nothing
 *
 *  Input Parameter(s):
 *            VLRS - vector defined in the LRS
 *
 *  Output Parameter(s):
 *            VFRS - vector defined in the FRS
 *
 **************************************************************/
void VectorLRStoFRS(VLRS, VFRS)
	double  VLRS[3];
	double  VFRS[3];
{
	double	Qs[3][3];
	int	i, j;
	
	CalculateQ(Qs, GD->ReferenceSystem[phi], GD->ReferenceSystem[theta],
					       GD->ReferenceSystem[psi]);

	RotateVector(Qs, VLRS, VFRS);
}


/*****************************
 *  PointLRStoFRS()
 **************************************************************/
/*
 *  Purpose:  Calculates the FRS coordinates of a point
 *            defined in the LRS.
 *
 *  Input Parameter(s):
 *            PLRS - point defined in the LRS
 *
 *  Output Parameter(s):
 *            PFRS - point defined in the FRS
 *
 **************************************************************/
void PointLRStoFRS(PLRS, PFRS)
	double  PLRS[3];
	double  PFRS[3];
{
	int	i;

	VectorLRStoFRS(PLRS, PFRS);
	for(i = 0; i < 3; i++)
		PFRS[i] += GD->ReferenceSystem[i];
}

/*****************************
 *  PositionToL()
 **************************************************************/
/*
 *  Purpose:  Calculates the lengths of the 6 actuators for a
 *            particular position defined in the (LRS,IRS)
 *            
 *  Remarks: 
 *
 *  Returns:  nothing
 *
 *  Input Parameter(s):
 *            Pos - instrument position in (LRS,IRS)
 *
 *  Output Parameter(s):
 *            L - pointer to the calculated actuator lengths
 *
 **************************************************************/
long PositionToL(Pos, L)
	double	Pos[6];
	double  L[6];
{
	double Q[3][3];      			/* Rotation matrix */

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> in PositionToL\n");
#endif

	CalculateQ(Q, Pos[phi], Pos[theta], Pos[psi]);
	return(QToL(Pos[x], Pos[y], Pos[z], Q, L));
}

/*****************************
 *  CalculateQ()
 **************************************************************/
/*
 *  Purpose:  Calculates the rotation matrix for a given set
 *            of Euler angles.
 *            
 *  Remarks: 
 *
 *  Returns:  nothing
 *
 *  Input Parameter(s):
 *            _phi   - Euler angle
 *            _theta -   "     "
 *            _psi   -   "     "
 *
 *  Output Parameter(s):
 *            Q - rotation matrix
 *
 **************************************************************/
void CalculateQ(Q, _phi, _theta, _psi)
	double Q[3][3];      			/* Rotation matrix */
	double _phi, _theta, _psi;			/*         */
{
	double cPhi = cos(_phi);
	double sPhi = sin(_phi);
	double cTheta = cos(_theta);
	double sTheta = sin(_theta);
	double cPsi = cos(_psi);
	double sPsi = sin(_psi);

	Q[x][x] = cPhi * cTheta;
	Q[x][y] = cPhi * sTheta * sPsi - sPhi * cPsi;
	Q[x][z] = sPhi * sPsi + cPhi * sTheta * cPsi;
	Q[y][x] = sPhi * cTheta; 
	Q[y][y] = cPhi * cPsi + sPhi * sTheta * sPsi; 
	Q[y][z] = sPhi * sTheta * cPsi - cPhi * sPsi;
	Q[z][x] = -sTheta; 
	Q[z][y] = cTheta * sPsi; 
	Q[z][z] = cTheta * cPsi;
}

/*****************************
 *  CalculateInvQ()
 **************************************************************/
/*
 *  Purpose:  Calculates the inverse of the rotation matrix
 *            for a given set of Euler angles.
 *            
 *  Remarks: - This function profits the fact that the transpose
 *             of the rotation matrix is its inverse.
 *
 *  Returns:  nothing
 *
 *  Input Parameter(s):
 *            _phi   - Euler angle
 *            _theta -   "     "
 *            _psi   -   "     "
 *
 *  Output Parameter(s):
 *            InvQ - inverse rotation matrix
 *
 **************************************************************/
void CalculateInvQ(InvQ, _phi, _theta, _psi)
	double InvQ[3][3];
	double _phi, _theta, _psi;
{
	double Q[3][3];
	int	i,j;

	CalculateQ(Q, _phi, _theta, _psi);
	for (i = 0; i < 3; i++)
		for (j = 0; j < 3; j++)
			InvQ[i][j] = Q[j][i];
}

/*****************************
 *  QToL()
 **************************************************************/
/*
 *  Purpose:  Calculates the length of the actuators for a
 *            certain displacement and orientation of the
 *            instrument in the LRS.
 *            
 *  Remarks: - The rotation matrix has to be calculated before
 *             calling this function.
 *
 *  Returns:  nothing
 *
 *  Input Parameter(s):
 *            x0 - x displacement of the instrument in the LRS
 *            y0 - y     "         "  "       "      "  "   "
 *            z0 - z     "         "  "       "      "  "   "
 *            Q - Rotation matrix
 *
 *  Output Parameter(s):
 *            L - pointer to the calculated actuator lengths
 *
 **************************************************************/
long QToL(x0, y0, z0, Q, L)
	double	x0, y0, z0;
	double	Q[3][3];
	double  L[6];
	
{
	double	dx, dy, dz;
	double *FixedNode, *MovingNode;
	int i; 

	for(i = 0; i < 6; i++){
		FixedNode = FixedP[i];
		MovingNode = MovingP[i];

		dx = x0 - FixedNode[x] +
			Q[x][x] * MovingNode[x] +
			Q[x][y] * MovingNode[y] +
			Q[x][z] * MovingNode[z];

		dy = y0 - FixedNode[y] +
			Q[y][x] * MovingNode[x] +
			Q[y][y] * MovingNode[y] +
			Q[y][z] * MovingNode[z];

		dz = z0 - FixedNode[z] +
			Q[z][x] * MovingNode[x] +
			Q[z][y] * MovingNode[y] +
			Q[z][z] * MovingNode[z];

		if (CalculateL(L, i, dx, dy, dz) != OK)
			return(NOT_OK);
	}
	return(OK);
}

/*****************************
 *  CalculateL_Top0()
 **************************************************************/
/*
 *  Purpose:  Calculates the length of one actuator for a
 *            topology '0' hexapode (SUPPORT).
 *            
 *  Remarks: 
 *
 *  Returns:  always OK
 *
 *  Input Parameter(s):
 *            i  - actuator number
 *            dx - x distance between fixed and moving joints
 *            dy - y     "       "       "  "    "       "
 *            dz - z     "       "       "  "    "       "
 *
 *  Output Parameter(s):
 *            L[i] - calculated actuator length
 *
 **************************************************************/
long CalculateL_Top0(L, i, dx, dy, dz)
	double	L[6];
	int	i;
	double	dx, dy,	dz;
{
	L[i] = sqrt(dx * dx + dy * dy + dz * dz);
	return(OK);
}

/*****************************
 *  CalculateL_Top1()
 **************************************************************/
/*
 *  Purpose:  Calculates the length of one actuator for a
 *            topology '1' hexapode (MANIPULATOR).
 *            
 *  Remarks: 
 *
 *  Returns:  OK      If no error
 *            NOTOK   If error
 *
 *  Input Parameter(s):
 *            i  - actuator number
 *            dx - x distance between fixed and moving joints
 *            dy - y     "       "       "  "    "       "
 *            dz - z     "       "       "  "    "       "
 *
 *  Output Parameter(s):
 *            L[i] - calculated actuator length
 *
 **************************************************************/
long CalculateL_Top1(L, i, dx, dy, dz)
	double	L[6];
	int	i;
	double	dx, dy,	dz;
{
	double h;
	double b2;
	double aux;

	h = dx * FixedNormal[x] + dy * FixedNormal[y] + dz * FixedNormal[z];

	aux = dx - h * FixedNormal[x];
	b2 = aux * aux;
	aux = dy - h * FixedNormal[y];
	b2 += aux * aux;
	aux = dz - h * FixedNormal[z];
	b2 += aux * aux;

	if ((aux = SquaredLinkLength[i] - b2) >= 0){
		L[i] = h - sqrt(aux);
		return(OK);
	} else {
		SD->Error = DevErr_ImposibleToCalculateLengths;
		return(NOT_OK);
	}
}

/*****************************
 *  PhiToL()
 **************************************************************/
/*
 *  Purpose:  Calculates the length of the actuators for a
 *            certain rotation angle around the z-axis in the
 *            auxiliary reference system (ARS).
 *            
 *  Remarks: - The SetAuxRefSystem() function must be used before
 *             any call to PhiToL() in order to define the ARS.
 *           - This function is much faster than PositionToL()
 *             and is preferable for tracking the hexapode
 *             position during rotary movements. The ARS can be
 *             chosen in such a way that its z-axis matches any
 *             arbitrary line in the space. This allows using
 *             this function for any arbitrary rotation.
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            _phi - rotation angle around the z-axis in the ARS
 *
 *  Output Parameter(s):
 *            L - pointer to the calculated actuator lengths
 *
 **************************************************************/
long PhiToL(_phi, L)
	double	_phi;
	double  L[6];
	
{
	double cPhi; 
   double sPhi;
	double	dx, dy, dz;
	double *FixedNode, *MovingNode;
	int i; 

	cPhi = cos(_phi);
	sPhi = sin(_phi);

	for(i = 0; i < 6; i++){
		FixedNode = AuxFixedP[i];
		MovingNode = AuxMovingP[i];

		dx = cPhi * MovingNode[x] - sPhi * MovingNode[y] - FixedNode[x];

		dy = sPhi * MovingNode[x] + cPhi * MovingNode[y] - FixedNode[y];

		dz = MovingNode[z] - FixedNode[z];

		if (CalculateL(L, i, dx, dy, dz) != OK)
			return(NOT_OK);
	}
	return(OK);
}


/*****************************
 *  ZToL()
 **************************************************************/
/*
 *  Purpose:  Calculates the length of the actuators for a
 *            certain translation along the z-axis in the
 *            auxiliary reference system (ARS).
 *            
 *  Remarks: - The SetAuxRefSystem() function must be used before
 *             any call to ZToL() in order to define the ARS.
 *           - This function is much faster than PositionToL()
 *             and is preferable for tracking the hexapode
 *             position during translation movements. The ARS
 *             can be chosen in such a way that its z-axis
 *             matches any direction in the space. This
 *             allows using this function for any arbitrary
 *             translation.
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            _z - translation along the z-axis in the ARS
 *
 *  Output Parameter(s):
 *            L - pointer to the calculated actuator lengths
 *
 **************************************************************/
long ZToL(_z, L)
	double	_z;
	double  L[6];
	
{
	double	dx, dy, dz;
	double *FixedNode, *MovingNode;
	int i; 

	for(i = 0; i < 6; i++){
		FixedNode = AuxFixedP[i];
		MovingNode = AuxMovingP[i];

		dx = MovingNode[x] - FixedNode[x];

		dy = MovingNode[y] - FixedNode[y];

		dz = _z + MovingNode[z] - FixedNode[z];

		if (CalculateL(L, i, dx, dy, dz) != OK)
			return(NOT_OK);
	}
	return(OK);
}

/**************************
 *  LToPosition()
 **************************************************************/
/*
 *  Purpose:  Calculates the position of the instrument in the
 *            LRS for a certain set of actuator lengths.
 *            
 *  Remarks: - In this function there is not any dependency on
 *             the particular topology of the hexapode. This
 *             function inverts PositionToL() using numerical 
 *             methods.
 *           - The routine applies the standard Newton-Raphson 
 *             algorithm that shows to be very efficient. 
 *           - The convergency of the algorithm is not guaranted
 *             and it is posible to find problems, but only in
 *             extreme anomalous conditions that are highly
 *             improbable to happen.
 *           - This function can use the position value pointed
 *             by the parameter Pos as an initial estimation.
 *             Calling the function with a good estimation in
 *             Pos reduces the calculation time and is advisable
 *             but not mandatory. The function always compares
 *             the value in Pos with the nominal position in the
 *             static variable GD->NominalPosition and chooses 
 *             the best as initial estimation.
 *
 *  Returns:  OK      If no error
 *            NOTOK   If error
 *
 *  Input Parameter(s):
 *            L0 - actuator lengths
 *            Pos - position estimation
 *
 *  Output Parameter(s):
 *            Pos - calculated position
 *
 *
 **************************************************************/
long LToPosition(L0, Pos)
	double	L0[6];
	double	Pos[6];
{
	double	NewPos[6];
	double	L[6], dL[6], auxdL[6];
	double	dPos[6];
	double	J[6][6];
	double	error0, error;
	double	lambda;
	long	   converg = NOT_OK;
	int   	i;

#ifdef DEBUG_LVL1
   printf("<GEOMETRY> in LToPosition\n");
#endif

	if (PositionToL(Pos, L) == OK){
		if (CalculatedL(L0, L, dL, &error0) == OK){
			return(OK);
      }
	} else
		error0 = -1;

#ifdef DEBUG_LVL3
   printf("\t\t\t<GEOMETRY> Error Estimation %e\n", error0);
#endif
	if (L0 != GD->NominalLength){
		converg = CalculatedL(L0, GD->NominalLength, auxdL, &error);
#ifdef DEBUG_LVL3
      printf("\t\t\t<GEOMETRY> Error Nominal %lf\n", error);
#endif
		if (error0 < 0 || error < error0){
			error0 = error;
			for ( i = 0; i < 6; i++){
				Pos[i] = GD->NominalPosition[i];
				dL[i] = auxdL[i];
			}
		}
	}
#ifdef DEBUG_LVL3
   printf("\t\t\t<GEOMETRY> Final Error %le\n",error0);
#endif
	if (error0 < 0)
		return(NOT_OK);

   if (converg == OK)
      return(OK);

	while(1){
		if (CalculateJ(J, L, Pos) != OK ||
		    SolveLinear(dL, J, dPos) != OK)
			return(NOT_OK);

		lambda = 1;
		while (1) {
			for (i = 0; i < 6; i++) {
		            NewPos[i] = Pos[i] - dPos[i] * lambda;
                        }

			if (PositionToL(NewPos, L) != OK){
				lambda /= 2.;
				continue;
			}
			converg = CalculatedL(L0, L, dL, &error);
#ifdef DEBUG_LVL4
			printf ("Err= %13.5E  lambda= %f\n", error, lambda);
#endif
			if (error >= error0) {
				if (lambda > 1e-8 )
					lambda /= 2.;
				else {
					SD->Error = DevErr_ConvergencyProblem;
					return(NOT_OK);
				}
			} else {
				error0 = error;
				for (i = 0; i < 6; i++)
					Pos[i] = NewPos[i];
				if (converg == OK)
					return(OK);
				else
					break;
			}
		}	
	}
}

/**************************
 *  CalculateJ()
 **************************************************************/
/*
 *  Purpose:  Calculates the Jacobian matrix of the position
 *            to length transformation for a particular position
 *            of the instrument.
 *            
 *  Remarks: - The calculation is done numerically, calling the
 *             function QToL() to obtain the partial derivatives. 
 *
 *  Returns:  nothing
 *
 *  Input Parameter(s):
 *            Pos - position
 *
 *  Output Parameter(s):
 *            J  - Jacobian Matrix
 *            L0 - actuator lengths
 *
 *
 **************************************************************/
long CalculateJ(J, L0, Pos)
	double	J[6][6];
	double	L0[6];
	double	Pos[6];
{
	double	Q[3][3];
	short	i, j;
	double	L1[6][6];

	CalculateQ(Q, Pos[phi], Pos[theta], Pos[psi]);
	if (QToL(Pos[x], Pos[y], Pos[z], Q, L0) != OK ||
	    QToL(Pos[x] + GD->MechResolution, Pos[y], Pos[z], Q, L1[x]) != OK ||
	    QToL(Pos[x], Pos[y] + GD->MechResolution, Pos[z], Q, L1[y]) != OK ||
	    QToL(Pos[x], Pos[y], Pos[z] + GD->MechResolution, Q, L1[z]) != OK)
		return(NOT_OK);

	CalculateQ(Q, Pos[phi] + GD->MechResolution, Pos[theta], Pos[psi]);
	if (QToL(Pos[x], Pos[y], Pos[z], Q, L1[phi]) != OK)
		return(NOT_OK);

	CalculateQ(Q, Pos[phi], Pos[theta] + GD->MechResolution, Pos[psi]);
	if (QToL(Pos[x], Pos[y], Pos[z], Q, L1[theta]) != OK)
		return(NOT_OK);

	CalculateQ(Q, Pos[phi], Pos[theta], Pos[psi] + GD->MechResolution);
	if (QToL(Pos[x], Pos[y], Pos[z], Q, L1[psi]) != OK)
		return(NOT_OK);

	for (i = 0; i < 6; i++)
		for (j = 0; j < 6; j++)
			J[i][j] = (L1[j][i] - L0[i])/GD->MechResolution;

	return(OK);
}

/**************************
 *  CalculatedL()
 **************************************************************/
/*
 *  Purpose:  Calculates the differences between two sets of
 *            actuator lengths, evaluates an error estimator
 *            and checks the convergency criterion.
 *            
 *  Returns:  OK     if the convergency criterion is fulfilled
 *            NOT_OK otherwise
 *
 *  Input Parameter(s):
 *            L0 - first set of lengths (goal lengths)
 *            L - second set of lengths (current lengths)
 *
 *  Output Parameter(s):
 *            dL - vector of individual length differences
 *            error - error estimator
 *
 **************************************************************/
long CalculatedL(L0, L, dL, error)
	double	L0[6];
	double	L[6];
	double	dL[6];
	double	*error;
{
#define MAX_ERROR (GD->MechResolution * GD->MechResolution / 10000)
	long	flag = OK;
	double	aux;
	int	i;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> in CalculatedL\n");
#endif

	*error = 0;
	for (i = 0; i < 6; i++) {
		dL[i] = aux = L[i] - L0[i];
		if ((aux *= aux) > MAX_ERROR) flag = NOT_OK;
		*error += aux;
	}
#ifdef DEBUG_LVL1
     printf("<GEOMETRY> in CalculatedL End %d\n",flag);
#endif

	return(flag);
}

/**************************
 *  SolveLinear()
 **************************************************************/
/*
 *  Purpose:  Solves the linear system:     Y = M X 
 *            
 *  Remarks: - This routine uses the LU descomposition with the
 *             Crout's method as it is presented in the book:
 *             W.Press, B.Flannery, S.Teukolsky, W.Vetterling,
 *             "NUMERICAL RECIPES, The Art of Scientific Computing",
 *             Cambridge University Press (1989).
 *
 *  Returns: 
 *
 *  Input Parameter(s):
 *            Y - first terms
 *            M - system matrix
 *
 *  Output Parameter(s):
 *            X - vector of solutions
 *
 **************************************************************/
long SolveLinear(Y, M, X)
	double Y[6];	
	double M[6][6];	
	double X[6];	
{
#define N 6
	double	Mmax;
	double	aux;
	double	vv[N];
	int	i, j, k;
	int	row;
	int	row_index[N];

	for ( i = 0; i < N; i++){
		Mmax = 0;
		for (j = 0; j < N; j++){
			if ((aux = fabs(M[i][j])) > Mmax)
				Mmax = aux;
		}
		if (Mmax == 0.) {
			SD->Error = DevErr_SingularMatrix;
			return(NOT_OK);
		}
		vv[i] = 1 / Mmax;
	}

	for ( j = 0; j < N; j++){
		for ( i = 0; i < j; i++){
			aux = M[i][j];
			for ( k = 0; k < i; k++)
				aux -= M[i][k] * M[k][j];
			M[i][j] = aux;
		}
		Mmax = 0;
		row = j;
		for ( i = j; i < N; i++){
			aux = M[i][j];
			for (k = 0; k < j; k++)
				aux -= M[i][k] * M[k][j];
			M[i][j] = aux;
			aux = vv[i] * fabs(aux);
			if ( aux >= Mmax ){
				Mmax = aux;
				row = i;
			}
		}
		if (j != row) {
			for (k = 0; k < N; k++){
				aux = M[row][k];
				M[row][k] = M[j][k];
				M[j][k] = aux;
			}
			vv[row] = vv[j];
		}
		row_index[j] = row;
		if ((aux = M[j][j]) == 0.) aux = TINY_VALUE;
		if (j < N - 1)
			for (i = j + 1; i < N; i++)
				M[i][j] = M[i][j] / aux;
	}

	for (i = 0; i < N; i++){
		row = row_index[i];
		aux = Y[row];
		Y[row] = Y[i];
		for (j = 0; j < i; j++)
			aux -= M[i][j] * Y[j];
		Y[i] = aux;
	}
	for (i = N - 1; i >= 0; i--){
		aux = Y[i];
		for (j = i + 1; j < N; j++)
			aux -= M[i][j] * X[j];
		X[i] = aux / M[i][i];
	}

	return(OK);
}

/**************************
 *  CalculateR()
 **************************************************************/
/*
 *  Purpose:  Calculates the rotation matrix that transforms
 *            a given vector in another one  by rotation around
 *            an axis orthogonal to both of them.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Vold - vector before the rotation
 *            Vnew - vector after the rotation
 *
 *  Output Parameter(s):
 *            R - rotation matrix
 *
 **************************************************************/
long CalculateR(R, Vold, Vnew)
	double R[3][3];
	double Vold[3];
	double Vnew[3];
{
	double VoldN[3];
	double VnewN[3];
	double SinAlpha;
	double CosAlpha;
	double U[3];
	double OneMinusCosAlpha;
	double aux;
	int	i, j;

	if (fabs(Normalize(Vold, VoldN)) < TINY_VALUE ||
	    fabs(Normalize(Vnew, VnewN)) < TINY_VALUE){
		SD->Error = DevErr_VectorModulusTooSmall;
		return(NOT_OK);
	}

	CosAlpha = DotProduct(VoldN, VnewN);

	VectorialProduct(VoldN, VnewN, U);
	if ((SinAlpha = fabs(Normalize(U, U))) < TINY_VALUE){
      /*
       *  The vectors are colinear
       */
		SinAlpha = 0.;
		if (CosAlpha > 0.)
			CosAlpha = 1.;
		else
			CosAlpha = -1.;
      /*
       *  The following code calculates an arbitrary
       *  vector U orthogonal to Vnew.
       */
		VnewN[0] = VnewN[1] = VnewN[2] = 0.;
		for (i = 0; fabs(VoldN[i]) < .5; (i = ++i % 3));
		VnewN[(++i % 3)] = 1.;
	   VectorialProduct(VoldN, VnewN, U);
      Normalize(U, U);
	}

	OneMinusCosAlpha = 1- CosAlpha;
	for (i = 0; i < 3; i++){
		for (j = 0; j < 3; j++){
			R[i][j] = U[i] * U[j] * OneMinusCosAlpha;
			if (i == j) 
				R[i][j] += CosAlpha;
			else {
				if ((3 + i - j) % 3 == 1)
					aux = 1.;
				else
					aux = -1.;

				R[i][j] += aux * U[3 - i - j] * SinAlpha;
			}
		}
	}
	return(OK);
}

/**************************
 *  RotateVector()
 **************************************************************/
/*
 *  Purpose:  Rotates a vector.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            R - rotation matrix
 *            OldVector - vector before rotation
 *
 *  Output Parameter(s):
 *            NewVector - vector after rotation
 *
 **************************************************************/
void RotateVector(R, OldVector, NewVector)
	double R[3][3];
	double OldVector[3];
	double NewVector[3];
{
	int	i, j;

	for (i = 0; i < 3; i++){
		NewVector[i] = 0;
		for (j = 0; j < 3; j++)
			NewVector[i] += R[i][j] * OldVector[j];
	}
}

/**************************
 *  ReduceAngles()
 **************************************************************/
/*
 *  Purpose:  Reduces the Euler angles to values within the
 *            ranges used by the program.
 *            
 *  Remarks: - The Euler angles are returned within the following 
 *             ranges:
 *                  phi    ( -PI  , +PI )
 *                  theta  (-PI/2 , PI/2) 
 *                  psi    ( -PI  , +PI )
 *           - The coordinates x, y and z are not modified at all
 *
 *  Input Parameter(s):
 *            P - position and orientation before angle reduction
 *
 *  Output Parameter(s):
 *            P - position and orientation after angle reduction
 *
 **************************************************************/
void ReduceAngles(P)
	double P[6];
{
	int	i;

   /* Reduce angles to values in the (-PI, +PI) interval  */
	for (i = 3; i < 6; i++){
		if(fabs(P[i]) > PI)
         if(P[i] > 0)
            P[i] -= 2 * PI * (int)((P[i] + PI) / (2*PI));
         else
            P[i] -= 2 * PI * (int)((P[i] - PI) / (2*PI));
	}

   /*
    *  Check particular cases:
    */
   if (fabs(P[theta]/PI/2) == 1.){
      /*
       *  If theta is -PI/2 or +PI/2 reset psi to zero and correct phi:
       */
      P[phi] -= sin(P[theta]) * P[psi];
      P[psi] = 0;
      ReduceAngles(P);
   } else if (fabs(P[theta]) > PI/2) {
      /*
       *  If theta is not in the (-PI/2, +PI/2) interval:
       */
      P[phi] += PI;
      P[theta] = PI - P[theta];
      P[psi] += PI;
      ReduceAngles(P);
   }
}

/**************************
 *  Normalize()
 **************************************************************/
/*
 *  Purpose:  Normalizes a vector and returns its modulus.
 *            
 *  Remarks:  
 *
 *  Returns:  Modulus of the vector
 *
 *  Input Parameter(s):
 *            vector - vector to normalize
 *
 *  Output Parameter(s):
 *            unitary_vector - normalized vector
 *
 **************************************************************/
double Normalize(vector, unitary_vector)
	double vector[3];
	double unitary_vector[3];
{
	double modulus = 0;
	int	i;

	for (i = 0; i < 3; i++)
		modulus += vector[i] * vector[i];

	modulus = sqrt(modulus);

	if (modulus > 0){
		for (i = 0; i < 3; i++)
			unitary_vector[i] = vector[i] / modulus;
		return(modulus);
	} else {
		unitary_vector[x] = unitary_vector[y] = unitary_vector[z] = 0.;
		return(0.);
	}
}

/**************************
 *  CheckLengths()
 **************************************************************/
/*
 *  Purpose:  Verifies whether or not the actuator lengths are
 *            within the allowed range.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            L - actuator lengths
 *
 *
 **************************************************************/
long CheckLengths(L)
	double	L[6];
{
	int	i;
        double  minlength, maxlength;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> in CheckLengths\n");
#endif

	for(i = 0; i < 6; i++) {
                if ( GD->MinLength[i] > 0.001) {
                     minlength = GD->MinLength[i];
                } else {
                     minlength = GD->MinActuatorLength;
                }
                if ( GD->MaxLength[i] > 0.001) {
                     maxlength = GD->MaxLength[i];
                } else {
                     maxlength = GD->MaxActuatorLength;
                }
#ifdef DEBUG_LVL1
     printf("  - Check leg %d (%3.4f,%3.4f) %3.4f is ",
                               i+1,minlength,maxlength,L[i]);
#endif
		if (L[i] < minlength || L[i] > maxlength){
#ifdef DEBUG_LVL1
     printf(" INVALID\n");
#endif
			SD->Error = DevErr_InvalidActuatorLengths;
			return(NOT_OK);
		} else {
#ifdef DEBUG_LVL1
     printf(" OK\n");
#endif
                }

        }
	return(OK);
}

/**************************
 *  CheckTilt()
 **************************************************************/
/*
 *  Purpose:  Verifies whether or not the hexapode is tilted
 *            more than the maximum allowed angle.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Pos - hexapode position in the LRS
 *
 **************************************************************/
long CheckTilt(Pos)
	double	Pos[6];
{
	double	Q[3][3];
	double	cosAngle;
	double	aux;
	int	i, j;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> in CheckTilt\n");
#endif

	cosAngle = 0;
	CalculateQ(Q, Pos[phi], Pos[theta], Pos[psi]);

	for(i = 0; i < 3; i++){
		aux = 0.;
		for(j = 0; j < 3; j++)
			aux += Q[i][j] * MovingNormal[j];
		cosAngle += FixedNormal[i] * aux;
	}

	if (cosAngle < cosMaxTiltAngle){
		SD->Error = DevErr_ExcesiveTiltAngle;
		return(NOT_OK);
	} else
		return(OK);
}

/**************************
 *  CheckPosition()
 **************************************************************/
/*
 *  Purpose:  Verifies whether or not the hexapode is within
 *            the ranges of allowed positions.
 *            
 *  Remarks: - The allowed range is given by MaxDisplacement
 *             and MaxAngle around the home position.
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Pos - hexapode position in the LRS
 *
 **************************************************************/
long CheckPosition(Pos)
	double	Pos[6];
{
	double	aux;
	int	i, j;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> in CheckPosition\n");
#endif

   ReduceAngles(Pos);

   for(i = 0; i < 6; i++) {
       if(fabs(Pos[i] - GD->NominalPosition[i]) > GD->MaxIncrement[i]){
          SD->Error = DevErr_PositionOutOfLimits;
          return(NOT_OK);
       }
   }

   return(OK);
}

/**************************
 *  CheckAll()
 **************************************************************/
/*
 *  Purpose:  Verifies whether or not the hexapode is in an
 *            allowed position.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            L - actuator lengths
 *
 *  Output Parameter(s):
 *            Pos - hexapode position in the LRS
 *            error - pointer to error
 *
 **************************************************************/
long CheckAll(L, Pos)
	double	L[6];
	double	Pos[6];
{
#ifdef DEBUG_LVL4
     printf("<GEOMETRY> Check All\n");
#endif

#ifdef DEBUG_LVL4
     printLengths(L);
     printPos(Pos);
#endif

	if (CheckLengths(L) != OK) 
		return(NOT_OK);
	
	if (LToPosition(L, Pos) != OK)
		return(NOT_OK);
	
        if (CheckTilt(Pos) != OK)
	  	return(NOT_OK);
  
	if (CheckPosition(Pos) != OK)
		return(NOT_OK);
#ifdef DEBUG_LVL4
     printf(" Checkall is going to return\n");
#endif

     return(OK);
}

/**************************
 *  SetActuatorTrajectory()
 **************************************************************/
/*
 *  Purpose:  Checks and initialises an actuator movement.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            StartL - initial actuator lengths
 *            EndL - final actuator lengths
 *
 *  Output Parameter(s):
 *            StartLength - initial actuator lengths
 *            DeltaLength - total increments in actuator lengths
 *            Delta_t - parameter increment
 *
 **************************************************************/
long SetActuatorTrajectory(StartL, EndL)
	double	StartL[6];
	double	EndL[6];
{
	double	max_incr;
	int	i;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> SetActuatorTrajectory\n");
#endif

	Trajectory = ActuatorTrajectory;

	max_incr = 0.;
	for (i = 0; i < 6; i++){
		StartLength[i] = StartL[i];
		DeltaLength[i] = EndL[i] - StartLength[i];
		if(fabs(DeltaLength[i]) > max_incr)
			max_incr = fabs(DeltaLength[i]);
	}

	if (max_incr < TINY_VALUE){
      SD->Error = DevErr_NullMovement;
      return(NOT_OK);
   }

	if ((Delta_t = GD->MovementResolution * TypicalLength / max_incr) > 1)
		Delta_t = 1;

	return (CheckTrajectory(Delta_t));
}

/**************************
 *  SetTableTrajectory()
 **************************************************************/
/*
 *  Purpose:  Checks and initialises a table movement.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            StartPos - initial instrument position in the LRS
 *            EndPos - final instrument position in the LRS
 *
 *  Output Parameter(s):
 *            StartPosition - initial instrument position
 *            DeltaPosition - total increments in position coords.
 *            Delta_t - parameter increment
 *
 **************************************************************/
long SetTableTrajectory(StartPos, EndPos)
	double	StartPos[6];
	double	EndPos[6];
{
	double	max_incr;
	int	i;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> SetTableTrajectory\n");
#endif

	Trajectory = TableTrajectory;
   ReduceAngles(StartPos);
   ReduceAngles(EndPos);

	max_incr = 0.;
	for (i = 0; i < 6; i++){
		StartPosition[i] = StartPos[i];
		DeltaPosition[i] = EndPos[i] - StartPosition[i];
		if(fabs(DeltaPosition[i] / (i < 3 ? TypicalLength : 1.)) > max_incr)
			max_incr = fabs(DeltaPosition[i] / (i < 3 ? TypicalLength : 1.));
	}

	if (max_incr < TINY_VALUE){
      SD->Error = DevErr_NullMovement;
      return(NOT_OK);
   }

	if ((Delta_t = GD->MovementResolution/max_incr) > 1)
		Delta_t = 1;

	return (CheckTrajectory(Delta_t));
}

/**************************
 *  SetRotationTrajectory()
 **************************************************************/
/*
 *  Purpose:  Checks and initialises a rotation movement.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            StartPos - initial instrument position in (LRS,IRS)
 *            Origin -  point of the rotation axis 
 *            Axis -  direction of the rotation axis
 *            Angle -  angle of rotation
 *
 *  Output Parameter(s):
 *            DeltaAngle - total angle of rotation.
 *            Delta_t - parameter increment
 *
 **************************************************************/
long SetRotationTrajectory(StartPos, Origin, Axis, Angle)
	double	StartPos[6];
	double	Origin[4];
	double	Axis[4];
	double	Angle;
{
#ifdef DEBUG_LVL1
     printf("<GEOMETRY> SetRotationTrajectory\n");
#endif

   ReduceAngles(StartPos);

	if (PointToLRS(StartPos, Origin) != OK ||
	    VectorToLRS(StartPos, Axis) != OK)
		return(NOT_OK);

	if (fabs(DeltaAngle = Angle) < TINY_VALUE){
      SD->Error = DevErr_NullMovement;
      return(NOT_OK);
   }

	Trajectory = RotationTrajectory;

	if (SetAuxRefSystem(StartPos, Origin, Axis) != OK)
		return(NOT_OK);

	if ((Delta_t = fabs(GD->MovementResolution / DeltaAngle)) > 1)
		Delta_t = 1;

	return (CheckTrajectory(Delta_t));
}

/**************************
 *  SetTraslationTrajectory()
 **************************************************************/
/*
 *  Purpose:  Checks and initialises a translation movement.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            StartPos - initial instrument position in the LRS
 *            Vector -  translation vector in the LRS
 *
 *  Output Parameter(s):
 *            DeltaVector - total translation distance.
 *            Delta_t - parameter increment
 *
 **************************************************************/
long SetTraslationTrajectory(StartPos, Vector)
	double	StartPos[6];
	double	Vector[4];
{
	double	DummyVector[3];

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> SetTraslationTrajectory\n");
#endif

   ReduceAngles(StartPos);

	if (VectorToLRS(StartPos, Vector) != OK)
		return(NOT_OK);

	if ((DeltaVector = Normalize(Vector, DummyVector)) < TINY_VALUE){
      SD->Error = DevErr_NullMovement;
      return(NOT_OK);
   }

	Trajectory = TraslationTrajectory;

	DummyVector[0] = DummyVector[1] = DummyVector[2] = 0.;

	if (SetAuxRefSystem(StartPos, DummyVector, Vector) != OK)
		return(NOT_OK);

	if ((Delta_t = GD->MovementResolution * TypicalLength / DeltaVector) > 1)
		Delta_t = 1;

	return (CheckTrajectory(Delta_t));
}

/**************************
 *  CheckTrajectory()
 **************************************************************/
/*
 *  Purpose:  Checks that the full trajectory is right.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            dt - trajectory step
 *
 **************************************************************/
long CheckTrajectory(dt)
	double	dt;
{
	double	L[6];
	double	Pos[6];
	double	t = dt;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> CheckTrajectory\n");
#endif

   CopyVector(6, GD->NominalPosition, Pos);
	if (Trajectory(1., L) == OK && CheckAll(L, Pos) != OK)
		return(NOT_OK);

   /* begin modif by P.Mangiagalli */
   if (dt != 1. ) {
   /* end modif by P.Mangiagalli */
        CopyVector(6, GD->NominalPosition, Pos);
	do
		if (Trajectory(t, L) != OK || CheckAll(L, Pos) != OK)
			return(NOT_OK);
	while((t += dt) < 1.);
   /* begin modif by P.Mangiagalli */
   }
   /* end modif by P.Mangiagalli */

   return(OK);
   
}

/**************************
 *  ActuatorTrajectory()
 **************************************************************/
/*
 *  Purpose:  Calculates the actuator lengths for a given point
 *            in an actuator trajectory.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            t - trajectory parameter  (0 < t < 1)
 *
 *  Output Parameter(s):
 *            L - actuator lengths.
 *
 **************************************************************/
long ActuatorTrajectory(t, L)
	double	t;
	double	L[6];
{
	int	i;

#ifdef DEBUG_LVL2
     printf("<GEOMETRY> ActuatorTrajectory\n");
#endif

	if (t <= 0.) t = 0.;
	else if (t >= 1.) t = 1.;

	for (i = 0; i< 6; i++)
		L[i] = StartLength[i] + t * DeltaLength[i];

	return(OK);
}

/**************************
 *  TableTrajectory()
 **************************************************************/
/*
 *  Purpose:  Calculates the actuator lengths for a given point
 *            in a table trajectory.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            t - trayectory parameter  (0 < t < 1)
 *
 *  Output Parameter(s):
 *            L - actuator lengths.
 *
 **************************************************************/
long TableTrajectory(t, L)
	double	t;
	double	L[6];
{
	int	i;
	double	Pos[6];

#ifdef DEBUG_LVL2
     printf("<GEOMETRY> TableTrajectory\n");
#endif

	if (t <= 0.) t = 0.;
	else if (t >= 1.) t = 1.;

	for (i = 0; i< 6; i++)
		Pos[i] = StartPosition[i] + t * DeltaPosition[i];

	return(PositionToL(Pos, L));
}

/**************************
 *  RotationTrajectory()
 **************************************************************/
/*
 *  Purpose:  Calculates the actuator lengths for a given point
 *            in a rotation trajectory.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            t - trayectory parameter  (0 < t < 1)
 *
 *  Output Parameter(s):
 *            L - actuator lengths.
 *
 **************************************************************/
long RotationTrajectory(t, L)
	double	t;
	double	L[6];
{
	int	i;

#ifdef DEBUG_LVL2
     printf("<GEOMETRY> RotationTrajectory\n");
#endif

	if (t <= 0.) t = 0.;
	else if (t >= 1.) t = 1.;

	return(PhiToL(t * DeltaAngle, L));
}


/**************************
 *  TraslationTrajectory()
 **************************************************************/
/*
 *  Purpose:  Calculates the actuator lengths for a given point
 *            in a translation trajectory.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            t - trayectory parameter  (0 < t < 1)
 *
 *  Output Parameter(s):
 *            L - actuator lengths.
 *
 **************************************************************/
long TraslationTrajectory(t, L)
	double	t;
	double	L[6];
{
	int	i;

#ifdef DEBUG_LVL2
     printf("<GEOMETRY> TraslationTrajectory\n");
#endif

	if (t <= 0.) t = 0.;
	else if (t >= 1.) t = 1.;

	return(ZToL(t * DeltaVector, L));
}

/**************************
 *  ExtractOrientation()
 **************************************************************/
/*
 *  Purpose:  Calculates the Euler angles for a new reference
 *            system.
 *            
 *  Remarks: - The new system is defined by two vectors along
 *             the directions of the new x and y axis.
 *           - The Euler angles are returned in the following 
 *             ranges:
 *                  phi    ( -PI  , +PI )
 *                  theta  (-PI/2 , PI/2) 
 *                  psi    ( -PI  , +PI )
 *
 *  Returns:  OK     if NewX and NewY are orthogonal
 *            NOT_OK otherwise
 *
 *  Input Parameter(s):
 *            NewX, NewY - directions of the new x and y axis
 *
 *  Output Parameter(s):
 *            _phi, _theta, _psi - pointers to the Euler angles
 *
 **************************************************************/
long ExtractOrientation(NewX, NewY, _phi, _theta, _psi)
	double	NewX[3];
	double	NewY[3];
	double	*_phi;
	double	*_theta;
	double	*_psi;
{
	double	NewZ[3];
	double	Yp[3];

#ifdef DEBUG_LVL3
     printf("<GEOMETRY> ExtractOrientation\n");
#endif

	if (fabs(Normalize(NewX, NewX)) < TINY_VALUE ||
	    fabs(Normalize(NewY, NewY)) < TINY_VALUE) {
		SD->Error = DevErr_VectorModulusTooSmall;
		return(NOT_OK);
	}
	if (fabs(DotProduct(NewX, NewY)) > TINY_VALUE){
		SD->Error = DevErr_VectorsNotOrthogonal;
		return(NOT_OK);
	}

	VectorialProduct(NewX, NewY, NewZ);

	if (fabs(NewX[x]) < TINY_VALUE && fabs(NewX[y]) < TINY_VALUE){

		/* Particular Case: NewX paralel to Z */

		*_phi = acos(NewY[y]);
      if (NewY[x] > 0.)
         *_phi = -*_phi;

		if (NewX[z] < 0.)
			*_theta =  PI / 2.;
		else
			*_theta = -PI / 2.;

      *_psi = 0;

	} else {

		/* Normal Case */

		Yp[x] = -NewX[y];
		Yp[y] =  NewX[x];
		Yp[z] =  0.;
		Normalize(Yp, Yp);

		*_phi = acos(Yp[y]);
		if(Yp[x] > 0)
			*_phi = -*_phi;

		*_theta = asin(-NewX[z]);

	   /* 
       *  The following calculation uses the fact that
	    *  cos(*_psi) = Yp.NewY  and sin(*_psi) = -Yp.NewZ
       */
	   *_psi = acos(DotProduct(Yp, NewY));
   	if(DotProduct(Yp, NewZ) > 0.)
   		*_psi = -*_psi;
	}

	return(OK);
}

/**************************
 *  DotProduct()
 **************************************************************/
/*
 *  Purpose:  Returns the scalar product of two vectors.
 *            
 *  Remarks:  
 *
 *  Returns:  the scalar product (dot product) of V1 and V2
 *
 *  Input Parameter(s):
 *            V1, V2 - vectors to multiply
 *
 **************************************************************/
float DotProduct(V1, V2)
	double	V1[3];
	double	V2[3];
{
	return(V1[x] * V2[x] + V1[y] * V2[y] + V1[z] * V2[z]);
}



/**************************
 *  VectorialProduct()
 **************************************************************/
/*
 *  Purpose:  Calculates the vectorial product of two vectors.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            V1, V2 - vectors to multiply
 *
 *  Output Parameter(s):
 *            V3 - vectorial product
 *
 **************************************************************/
void VectorialProduct(V1, V2, V3)
	double	V1[3];
	double	V2[3];
	double	V3[3];
{
	V3[x] = V1[y] * V2[z] - V1[z] * V2[y];
	V3[y] = V1[z] * V2[x] - V1[x] * V2[z];
	V3[z] = V1[x] * V2[y] - V1[y] * V2[x];
}

/* begin modif by P.Mangiagalli */
/**************************
 *  CenterOfMass()
 **************************************************************/
/*
 *  Purpose:  Calculates the center of mass of a joint set.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Point - points to process
 *
 *  Output Parameter(s):
 *            Com - center of mass
 *
 **************************************************************/
void CenterOfMass(Point, Com)
      double  Point[6][3];
      double  Com[3];
{
      int     i, j;

      for(j = 0; j < 3; j++){
              Com[j] = 0;
              for(i = 0; i < 6; i++)
                      Com[j] += Point[i][j];
              Com[j] /= 6;
      }
}
/* end modif by P.Mangiagalli */


/**************************
 *  CopyVector()
 **************************************************************/
/*
 *  Purpose:  Copies a vector.
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            n - dimension of the vector
 *            Vsource - pointer to source
 *
 *  Output Parameter(s):
 *            Vtarget - pointer to target
 *
 **************************************************************/
void CopyVector(n, Vsource, Vtarget)
	int	n;
	double *Vsource;
	double *Vtarget;
{
	while(n-- > 0) *(Vtarget++) = *(Vsource++);
}

/**************************
 *  SetReferenceAxis()
 **************************************************************/
/*
 *  Purpose:  .
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            OldAxis - 
 *            NewAxis - 
 *
 *  Output Parameter(s):
 *            
 *
 **************************************************************/
long SetReferenceAxis(Pos, OldAxis, NewAxis)
	double	Pos[6];
	double	OldAxis[4];
	double	NewAxis[4];
{
	double	Rs[6];
	double	R[3][3];
	double	Qs[3][3];
	double	OldRs[6];
	double	NewX[3];
	double	NewY[3];
	int	i, k;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> SetReferenceAxis\n");
#endif

	if (VectorToFRS(Pos, OldAxis) != OK ||
	    VectorToFRS(Pos, NewAxis) != OK )
		return(NOT_OK);

	CopyVector(6, GD->ReferenceSystem, Rs);

	CalculateQ(Qs, Rs[phi], Rs[theta], Rs[psi]);

	/* In the next function call the order of the arguments
         * has been inverted because we want to rotate not the
	 * vectors but the reference system.
	 */

	if(CalculateR(R, NewAxis, OldAxis) != OK)
      return(NOT_OK);

	for (i = 0; i < 3; i++){
		NewX[i] = 0;
		NewY[i] = 0;
		for (k = 0; k < 3; k++){
			NewX[i] += R[i][k] * Qs[k][x];
			NewY[i] += R[i][k] * Qs[k][y];
		}
	}
	if (ExtractOrientation(NewX, NewY, Rs + phi, Rs + theta, Rs + psi)
									 != OK)
		return(NOT_OK);
	else 
		return(SetReferenceSystem(Rs));
}

/**************************
 *  SetReferenceOrigin()
 **************************************************************/
/*
 *  Purpose:  .
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            NewRefOrigin - 
 *
 *  Output Parameter(s):
 *            
 *
 **************************************************************/
long SetReferenceOrigin(Pos, NewRefOrigin)
	double	Pos[6];
	double	NewRefOrigin[4];
{
	double	NewRs[6];

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> SetReferenceOrigin\n");
#endif

	if (PointToFRS(Pos, NewRefOrigin) != OK)
		return(NOT_OK);

	CopyVector(6, GD->ReferenceSystem, NewRs);
	CopyVector(3, NewRefOrigin, NewRs);
	return(SetReferenceSystem(NewRs));
}

/**************************
 *  SetReferenceSystem()
 **************************************************************/
/*
 *  Purpose:  
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            NewRs - 
 *
 *  Output Parameter(s):
 *            
 *
 **************************************************************/
long SetReferenceSystem(NewRs)
	double	NewRs[6];
{
   double  NewSystem[6];
   double  AuxVector[3];
   double  DefInvQs[3][3];
   double  NewInvQs[3][3];
   double  NewX[3];
   double  NewY[3];
   int     i;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> SetReferenceSystem\n");
#endif

   if (GD->ReferenceSystemLock){
      /*
       *  Calculate in NewSystem the coords. of the new system NewRs
       *  respect to the default one (GD->DefaultReferenceSystem)
       */
      CalculateInvQ(NewInvQs, NewRs[phi], NewRs[theta], NewRs[psi]);
      CalculateInvQ(DefInvQs, GD->DefaultReferenceSystem[phi], 
                              GD->DefaultReferenceSystem[theta],
                              GD->DefaultReferenceSystem[psi]);

      for(i = 0; i < 3; i++)
         AuxVector[i] = NewRs[i] - GD->DefaultReferenceSystem[i];

      RotateVector(DefInvQs, AuxVector, NewSystem);
      RotateVector(DefInvQs, NewInvQs[x], NewX);
      RotateVector(DefInvQs, NewInvQs[y], NewY);

      if (ExtractOrientation(NewX, NewY, NewSystem + phi,
                                     NewSystem + theta, NewSystem + psi) != OK)
         return(NOT_OK);

      /*
       *  Check that the new reference system is not too far from 
       *  the default one.
       */
      for(i = 0; i < 6; i++)
         if(fabs(NewSystem[i]) > GD->MaxIncrement[i]){
            SD->Error = DevErr_RefSystemLocked;
            return(NOT_OK);
         }
   }

	if (UpdateReference(NewRs, GD->ReferencePosition) == OK)
		CopyVector(6, NewRs, GD->ReferenceSystem);
	else {
		UpdateReference(GD->ReferenceSystem, GD->ReferencePosition);
		return(NOT_OK);
	}
	return(OK);
}

/***************************
 *  SetReferencePosition()
 **************************************************************/
/*
 *  Purpose:  
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            OldPos - 
 *            NewPos - 
 *
 *  Output Parameter(s):
 *            
 *
 **************************************************************/
long SetReferencePosition(OldPos, NewPos)
	double	OldPos[6];
	double	NewPos[6];
{
	double	NewRefPos[6];
	double	New[3][3];
	double	Qp[3][3];
	double	Qs[3][3];
	double	Qold[3][3];
	double	InvQnew[3][3];
	int	i, j, k, l, m, n;
	double	sum_l, sum_m, sum_n;

#ifdef DEBUG_LVL1
     printf("<GEOMETRY> SetReferencePosition\n");
#endif

	CalculateQ(Qp, GD->ReferencePosition[phi],
		       GD->ReferencePosition[theta],
		       GD->ReferencePosition[psi]);

	CalculateQ(Qs, GD->ReferenceSystem[phi],
			GD->ReferenceSystem[theta],
			GD->ReferenceSystem[psi]);

	CalculateQ(Qold, OldPos[phi], OldPos[theta], OldPos[psi]);
	CalculateInvQ(InvQnew, NewPos[phi], NewPos[theta], NewPos[psi]);

	for(i = 0; i < 3; i++){
		NewRefPos[i] = GD->ReferencePosition[i];
		for(j = 0; j < 3; j++)
			NewRefPos[i] += Qs[i][j] * (OldPos[j] - NewPos[j]);
	}

	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){
			New[j][i] = 0;
			for (k = 0; k < 3; k++){
				sum_l = 0;
				for(l = 0; l < 3; l++){
					sum_m = 0;
					for(m = 0; m < 3; m++){
						sum_n = 0;
						for(n = 0; n < 3; n++)
							sum_n +=
							   Qs[n][m] * Qp[n][j];
						sum_m += Qold[l][m] * sum_n;
					}
					sum_l += InvQnew[k][l] * sum_m;
				}
				New[j][i] += Qs[i][k] * sum_l;
			}
		}
	}
				
	if (ExtractOrientation(New[x], New[y], NewRefPos + phi,
				NewRefPos + theta, NewRefPos + psi) != OK)
		return(NOT_OK);

	if (UpdateReference(GD->ReferenceSystem, NewRefPos) == OK)
		CopyVector(6, NewRefPos, GD->ReferencePosition);
	else {
		UpdateReference(GD->ReferenceSystem,
				GD->ReferencePosition);
		return(NOT_OK);
	}
	return(OK);
}

/**************************
 *  CheckIfValidPosition()
 **************************************************************/
/*
 *  Purpose:  .
 *            
 *  Remarks:  
 *
 *  Returns:  
 *
 *  Input Parameter(s):
 *            Position - 
 *
 *  Output Parameter(s):
 *            Length - 
 *
 **************************************************************/
long CheckIfValidPosition(Position, Length)
	double	Position[6];
	double	Length[6];
{
#ifdef DEBUG_LVL1
     printf("<GEOMETRY> CheckIfValidPosition\n");
#endif

	if (PositionToL(Position, Length) != OK)
      return(NOT_OK);

	return(CheckAll(Length, Position));
}
