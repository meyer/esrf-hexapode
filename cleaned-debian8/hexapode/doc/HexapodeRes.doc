
Resources

Class resources:

Name:	FilePath
Type:	D_STRING_TYPE (23 characters max.)
Description: Full path, as seen from the system that is running the device server, where the backup files are saved. This is a directory name, the actual file names are automatically made up from the names of the devices. The device server must have read and write permissions to this directory.

Object Resources:

Name:	Description
Type:	D_STRING_TYPE (23 characters max.)
Description: Arbitrary string that identifies the hexapode.

Name(s):	Fixed1X, Fixed1Y, Fixed1Z  to  Fixed6X, Fixed6Y, Fixed6Z
Type:	D_DOUBLE_TYPE
Units:	mm
Description:  Coordinates (x, y, z) of the centers of the six ball-and-socket joints associated to the fixed plate that supports the hexapode. For a "SUPPORT" hexapode the joints are attached to the plate and they are actually fixed. For a "MANIPULATOR" the joints have one degree of freedom and can move perpendicularly to the plate, in this case the points defined by this resource correspond to the "zero" positions of the six joints and all of them must be contained in a plane parallel to the fixed plate. The particular reference system employed can be chosen arbitrarily but it becomes the Fixed Reference System (FRS) and it is used as an internal reference by the device server. The numbering of the joints must follow the numbering of the links that should follow the following rule: when looking from the moving plate, the links must be numbered in counter-clockwise sequence.

Name(s):	Moving1X, Moving1Y, Moving1Z  to  Moving6X, Moving6Y, Moving6Z
Type:	D_DOUBLE_TYPE
Units:	mm
Description:  Coordinates (x, y, z) of the centers of the six ball-and-socket joints attached to the moving plate of the hexapode. The coordinates are referred to a coordinate system that moves with the moving plate and can be chosen arbitrarily. It becomes the Moving Reference System (MRS) and it is used internally by the device server. As explained for the fixed joints, the numbering of the joints must follow the link numbering.

Name:	Topology
Type:	D_STRING_TYPE
Values:	"SUPPORT" or "MANIPULATOR"
Description: Type of mechanic structure. In hexapodes of type "SUPPORT" the centers of the ball-and-socket joints that are associated with the fixed plate are actually fixed and the motor driven actuators play the role of variable length links with the moving plate. In hexapodes of type "MANIPULATOR" the links are fixed-length segments and the movement is accomplished by means of linear displacement of the base joints (the so called �fixed� joints) along the direction perpendicular to the fixed base. 

Name:	MechanicalRef
Type:	D_STRING_TYPE
Values:	"NONE", "ABSOLUTE", "MIDPOINT" or "LIMIT"
Description: Type of absolute position reference implemented in the mechanics.
	NONE: No position reference
	ABSOLUTE: Absolute position detector or encoder.
	MIDPOINT: Reference switches within the working range of the hexapode.
	LIMIT: Reference switches out of the working range of the hexapode (normally limit switches).

Name:	LengthUncertainty
Type:	D_DOUBLE_TYPE
Units:	mm
Description: Maximum uncertainty in the lengths of the actuators that is considered acceptable after an error occurred during movements. If the uncertainty is smaller that this value the program can recover by means of a soft reset command, if the uncertainty is larger, a hard reset is necessary.

Name(s):	LinkLength1 to LinkLength6
Type:	D_DOUBLE_TYPE
Units:	mm
Description:  Fixed lengths of the link segments for "MANIPULATOR" hexapodes (see Topology resource). In the case of "SUPPORT" hexapodes these resources are ignored.

Name(s):	MinActuatorLength, MaxActuatorLength
Type:	D_DOUBLE_TYPE
Units:	mm
Description: Minimum and maximum length accepted for each actuator.

Name:	MaxTiltAngle
Type:	D_DOUBLE_TYPE
Units:	degrees
Description: Maximum acceptable angle formed between the normal directions to the fixed and moving plates. 

Name:	MaxJointAngle
Type:	D_DOUBLE_TYPE
Units:	degrees
Description: Maximum acceptable angle formed between the link segments and the normal direction to the moving plate. (To be implemented)

Name(s):	MaxIncrementX, MaxIncrementY, MaxIncrementZ
		MaxIncrementPhi, MaxIncrementTheta, MaxIncrementPsi
Type:	D_DOUBLE_TYPE
Units:	mm and degrees
Description: Maximum acceptable increments in the position coordinates (x, y, z, phi, theta, psi) respect to the nominal position. The coordinates are referred to the Laboratory Reference System (LRS).

Name(s):	MaxMovementResolution
Type:	D_DOUBLE_TYPE
Units:	-
Description: Parameter that limits the step size during movements. The maximum angular step is MaxMovementResolution in radians and the maximum displacement step is MaxMovementResolution*LENGTH, where LENGTH is the typical size of the particular hexapode.

Name(s):	NominalLength1  to  NominalLength6
Type:	D_DOUBLE_TYPE
Units:	mm
Description: Actuator lengths at the nominal position of the hexapode. They must be within the range delimited by MinActuatorLength y MaxActuatorLength. This is the actual definition of the nominal position, the spatial and angular coordinates are calculated from these lengths. The nominal position should be as close as possible to the �default� or most usual position of the hexapode.

Name(s):	DefRefSystemX, DefRefSystemY, DefRefSystemZ
		DefRefSystemPhi, DefRefSystemTheta, DefRefSystemPsi
Type:	D_DOUBLE_TYPE
Units:	mm and degrees
Description: Default position and orientation of the Laboratory Reference System (LRS) defined respect to the FRS.

Name(s):	ReferenceSystemLock
Type:	D_BOOLEAN_TYPE
Values:	"On" or "Off"
Description: If this flag is not set ("Off") the position and orientation of the reference system (LRS) can be modified without any restriction. If the flag is set ("On") it can only be modified respect to the default LRS by amounts smaller than the maximum values given by MaxIncrementX, MaxIncrementY, MaxIncrementZ, MaxIncrementPhi, MaxIncrementTheta and MaxIncrementPsi respectively.

Name(s):	DefRefPositionX, DefRefPositionY, DefRefPositionZ
		DefRefPositionPhi, DefRefPositionTheta, DefRefPositionPsi
Type:	D_DOUBLE_TYPE
Units:	mm and degrees
Description: Default reference position and orientation of the hexapode. The reference position is defined as the position of the Moving Reference System (MRS) respect to the Fixed Reference System (FRS) when the hexapode is at the �zero� position, and therefore it is not dependent on the particular LRS chosen.

Name(s):	HomeLength1  to  HomeLength6
Type:	D_DOUBLE_TYPE
Units:	mm
Description: Lengths of the six actuators at the positions of the mechanical reference switches.

Name:	MovementMode
Type:	D_STRING_TYPE
Values:	"NORMAL" or "SIMULATION"
Description: Initial mode of movement that is adopted by the program when it is started. In simulation mode no motor hardware is accessed and the backup files are not modified.

Name:	Backlash
Type:	D_DOUBLE_TYPE
Units:	mm
Description: Backlash correction. The sign of this value indicates the direction in which the movement is always finished.

Name:	MotorType
Type:	D_STRING_TYPE
Values:	"NOMOTOR", "VPAP", "VPAP_545", "VPAP_550", "VPAP_DS", "MAXE_DS", "PI-C800_SER"
Description: Selects the type of motor controller system.
 	NOMOTOR - simulated motor controller without any actual hardware.
          VPAP_545 - VPAP with CY545 microcontrollers + VPAP driver.
          VPAP_550 - VPAP with CY550 microcontrollers + VPAP driver.
          VPAP         - any combination of VPAP channels + VPAP driver.
	VPAP_DS - VPAP device server (not implemented yet).
	MAXE_DS - MAXE device server (not implemented yet).
	PI-C800_SER - Phisik Instrumente DC motor controller C-800 + serial line driver (not implemented yet).


VPAP specific device resources (for VPAP, VPAP_545 and VPAP_550) :

Name:	VPAP_StepsPerMM
Type:	D_FLOAT_TYPE
Units:	steps/mm
Description: Conversion ratio between VPAP steps and actual linear displacement. The DPAP power modules are programmed in half-step mode.

Name:	VPAP_MaxSpeed
Type:	D_FLOAT_TYPE
Units:	steps/sec
Description: Maximum speed in VPAP steps per second. The speed for each actuator is calculated from this value, the current velocity value in the program (see DevHxpdSetVelocity command) and the actual trajectory that the hexapode has to follow.

Name:	VPAP_InitialSpeed
Type:	D_FLOAT_TYPE
Units:	steps/sec
Description: Initial speed before acceleration in VPAP steps per second. If for a certain movement the desired final speed is smaller than VPAP_InitialSpeed, the motor is started at the final speed without acceleration phase.

Name:	VPAP_HomeSearchSpeed
Type:	D_FLOAT_TYPE
Units:	steps/sec
Description: Speed in the final phase of the home search algorithm in VPAP steps per second. If the home switch is of type LIMIT (see MechanicalRef resource) the whole search process uses this speed value.

Name:	VPAP_HomeHysteresis
Type:	D_LONG_TYPE
Units:	steps
Description: Hysteresis of the home switches in VPAP steps. This parameter must be larger than the actual hysteresis of the switches in order to assure that when a one of them is activated it can be deactivated by going back this number of steps.

Name:	VPAP_SlopeRate
Type:	D_SHORT_TYPE
Units:	-
Description: Slope parameter ranging from 1 to 255 that the Cybernetic motor controller in the VPAP board uses for generating the acceleration curve. There is no a direct meaning for this parameter and its effect is different for the CY545 and  CY550 controllers. However the average acceleration increases monotonously with its value for the same initial and final speeds. The value of this resource must be selected by testing the motor hardware.


Name:	VPAPmotor1Unit, VPAPmotor1Chan  to
		VPAPmotor6Unit, VPAPmotor6Chan
Type:	D_SHORT_TYPE
Units:	-
Description: VPAP devices used to control each one of the six actuators. VPAPmotorXUnit is the number of the VPAP board (starting from 0) and VPAPmotorXChan  is the number of the channel in the board (1 to 8) that controls the actuator X.
