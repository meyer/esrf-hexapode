/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/include/RCS/HexapodeP.h,v 2.3 2012/09/24 09:22:54 perez Rel $";*/
/*********************************************************************
 *
 * File:  HexapodeP.h
 *
 * Project:  Hexapode Device Server
 *
 * Description:  Private header for the  Hexapode Device Server
 *    
 *
 * Author(s):  Pablo Fajardo & Vicente Rey Bakaikoa 
 *
 * Original:  March 11 1993  
 *
 * $Log: HexapodeP.h,v $
 * Revision 2.3  2012/09/24 09:22:54  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:16  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:15  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.30  2006/05/22 14:50:45  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.22  2004/03/09 16:29:41  perez
 * Increase delay in ContinueHardReset()
 *
 * Revision 1.21  2004/01/15  16:41:07  16:41:07  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:42  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.1  1994/01/28  13:20:22  rey
 * Initial revision
 *
 * Revision 1.13  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.12  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.12  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.11  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.10  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.9  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.8  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.7  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.6  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.5  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.4  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.3  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.2  1993/10/26  07:53:43  rey
 * Version at 26 October
 *
 * Revision 1.1  1993/10/16  20:12:41  rey
 * Initial revision
 *
 * Revision 1.1  93/10/11  17:47:21  17:47:21  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 * Revision 1.1  93/10/11  17:46:07  17:46:07  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 * Revision 1.1  1993/09/30  13:01:39  rey
 * Initial revision
 *
 * Copyright (c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 *********************************************************************/

#ifndef _HEXAPODEP_H
#define _HEXAPODEP_H

#include <libdeep.h>


/*
 * Hexapito Flag values.
 */
#define HEXAP_RUNNING       1992
#define HEXAP_NOT_RUNNING   1993

#define COMMAND_REQUEST    SIGUSR1
#define COMMAND_ACCEPTED   SIGUSR2
#define COMMAND_EXECUTE    SIGXCPU
#define COMMAND_SUCCESFUL  SIGURG
#define COMMAND_ERROR      SIGUNUSED
#define COMMAND_COMPLETED  SIGXFSZ
#define COMMAND_END_ACKN   SIGVTALRM
#define NO_COMMAND         SIGPROF

/*
 * Hexapito commands.
 */
#define DEV_GET_INFO          0
#define DEV_SEARCH_HOME       2
#define DEV_SET_REF_SYSTEM    3
#define DEV_SET_REF_POSITION  4
#define DEV_SET_REF_ORIGIN    5
#define DEV_SET_REF_AXIS      6
#define DEV_DEF_REFERENCE     7
#define DEV_CHECK_POSITION    8
#define DEV_MOVE_ACTUATORS    9
#define DEV_MOVE_TABLE       10 
#define DEV_ROTATE_TABLE     11
#define DEV_TRANSLATE_TABLE  12 
#define DEV_ABORT_MOVEMENT   13 
#define DEV_SET_ACT_LENGTH   14 
#define DEV_SET_MODE         15
#define DEV_SET_VELOCITY     16
#define DEV_SET_RESOLUTION   17
#define DEV_RESET            18 
#define DEV_QUIT             19

#define NORMAL_SEARCH        47
#define RESET_SEARCH        123

typedef struct
{
      long      MotorCleverSearch;
      double    MotorResolution;
      double    EncoderResolution;
      double    MechanicalResolution;
} actuatorParameters;

typedef struct 
{
      short         cratenum;
      struct 
      {
          short         unitnum;
          short         channel;
          int           filepath;
      }             motor[6];
      float         StepsPerMeter;
      float         MaxSpeed;
      float         InitialSpeed;
      float         HomeSearchSpeed;
      long          HomeHysteresis;
      short         SlopeRate;
} vpapData;

#define ICEPAP_HOSTNAME_MAX 50
typedef struct 
{
      char          hostname[ICEPAP_HOSTNAME_MAX];
      int           debug;
      int           libdebug;
      int           initialized;
      deephandle_t  dev;
      float         StepsPerMeter;
      int           Velocity;
      struct 
      {
          int         addr;
      }             motor[6];
} icepapData;


typedef struct              /* data structure for PI controller (PM) */
{
   char    version[2][256]; /* firmware version of the two controllers */
   char    devicename[25];  /* filepath for the pysical device */     
   int     fd;              /* file descriptor for communication */   
   float   StepsPerMeter;
} piData;

typedef struct              /* data structure for Hydraulic controller (PM) */
{
  char    feedback_process_name[64];
#ifdef linux
  pid_t   feedback_pid;
#endif
} hydData;

typedef struct 
{
      long          MotorType;
      union
      {
          vpapData       vpap;
          icepapData     icepap;
          piData         pi;
          hydData        hyd;
      }             MotorPrivate;
} motorData; 

typedef struct _GeometryData {
                                       /* Hexapode definition */
      long          Topology;
      double        FixedPoint[6][3];
      double        MovingPoint[6][3];
      double        LinkLength[6];
      double        MinActuatorLength;
      double        MaxActuatorLength;
      double        MinLength[6];
      double        MaxLength[6];
      double        MaxTiltAngle;
      double        MaxIncrement[6];
      double        NominalLength[6];
      double        NominalPosition[6];
                                       /* Default Reference System */  
      double        DefaultReferenceSystem[6];
      double        DefaultReferencePosition[6];
                                       /* Current Reference System */
      double        ReferenceSystem[6];
      double        ReferencePosition[6];
                                       /* Mechanical Resolution (m)*/ 
      double        MechResolution;
                                       /* Movement Resolution (< 1)*/ 
      double        MovementResolution;
      char          ReferenceSystemLock;
} GeometryData;


typedef struct _ActuatorData {
      float     Velocity;
      double    HomeLength[6];
      double    HomeLengthFound[6];
		long      MechanicalRef;
      double    Backlash;
      double    ActuatorBacklash;
      long      CleverHomeSearch;
      motorData MotorData; 
} ActuatorData;


#define MAX_FILENAME_LENGTH 160

typedef struct _SharedData 
{
                                    /* Position filename  */
      char           BackupFile[MAX_FILENAME_LENGTH];
      char           TmpBackupFile[MAX_FILENAME_LENGTH];
      char           Description[MAX_FILENAME_LENGTH];
		double			LengthUncertainty;
                                    /* Dataport Variables */
      unsigned short pid;
      unsigned short serverpid;
      unsigned long  time;
                                    /* Geometry and Actuators Data */
      GeometryData   GData;
      ActuatorData   AData;
                                    /* Current Parameters */
      double         Position[6];
      double         ActuatorLength[6];
      long           ActuatorStatus[6];
                                    /* Absolute movement definitions */
      double         FinalPosition[6];
      double         FinalActuatorLength[6];
                                    /* Rotation and Translation definitions */
      double         RotationAngle;
      double         RotationOrigin[4];
      double         RotationAxis[4];
      double         TranslationVector[4];
                                    /* Reference Position */
      double         OldPosition[6];
      double         NewPosition[6];
                                    /* Reference System Orientation */
      double         NewRefSystem[6];
      double         NewReferenceOrigin[4];
      double         OldDirection[4];
      double         NewDirection[4];
      double         NewActuatorLength[6];
                                     /* Movement Mode */
      float          NewVelocity;
      long           NewMovementMode;
      long           ResetMode;
      double         NewMovementResolution;
                                     /* Movement Mode */
      long           MovementMode;
      long           LastResetMode;
      double         MaxMovementResolution;
                                     /* Interprocess  */        
      long           Command;
      long           State;
      long           Error;
      long           LastError;
} SharedData;

typedef struct _HexapodeClassPart
{
		char	*filepath;
		char  *class_svr_name;
}  HexapodeClassPart;


typedef struct _HexapodePart
{
      char           *subprocname;          /* Subprocess name           */
      char           *shname;               /* Shared Memory name        */
      char           *description;          /* Device description        */
      SharedData     *shared;               /* Pointer to Data Module    */
} HexapodePart;



typedef struct _HexapodeClassRec
{
      DevServerClassPart    devserver_class;
      HexapodeClassPart     hexapode_class;        
} HexapodeClassRec;



extern HexapodeClassRec hexapodeClassRec;


typedef struct _HexapodeRec
{
       DevServerPart      devserver;
       HexapodePart       hexapode;
} HexapodeRec;

extern int  os9forkc();
extern char **environ;

#endif 

