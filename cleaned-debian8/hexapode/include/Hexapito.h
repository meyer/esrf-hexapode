
#include <DevServer.h>
#include <DevServerP.h>
#include <Hexapode.h>
#include <HexapodeP.h>

typedef struct {
   long       (*MotorInit)();
   long       (*CheckMotors)();
   long       (*SetMotors)();
   long       (*MoveMotorsTo)();
   long       (*StopMotors)();
   long       (*SearchHomeSwitch)();
} MotorDefinition;
