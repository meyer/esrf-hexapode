/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/include/RCS/Hexapode.h,v 2.3 2012/09/24 09:22:54 perez Rel $ ";*/
/*********************************************************************
 *
 * File:  Hexapode.h
 *
 * Project:  Hexapode Device Server
 *
 * Description:  Public header for the  Hexapode Device Server
 *    
 *
 * Author(s):  Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:  March 11 1993
 *
 * $Log: Hexapode.h,v $
 * Revision 2.3  2012/09/24 09:22:54  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:16  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:15  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.30  2006/05/22 14:50:45  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.22  2004/03/09 16:29:37  perez
 * Increase delay in ContinueHardReset()
 *
 * Revision 1.21  2004/01/15  16:41:05  16:41:05  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:42  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.1  1994/01/28  13:20:22  rey
 * Initial revision
 *
 * Revision 1.13  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.12  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.12  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.11  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.10  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.9  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.8  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.7  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.6  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.5  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.4  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.3  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.2  1993/10/26  07:53:43  rey
 * Version at 26 October
 *
 * Revision 1.1  1993/10/16  20:12:41  rey
 * Initial revision
 *
 * Revision 1.1  93/10/11  17:47:19  17:47:19  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 * Revision 1.1  93/10/11  17:37:53  17:37:53  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 * Revision 1.1  1993/09/30  13:01:39  rey
 * Initial revision
 *
 *
 * Copyright(c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 *********************************************************************/

#ifndef _HEXAPODE_H
#define _HEXAPODE_H

#include <DevHexapodeCmds.h>
#include <DevHexapodeErrors.h>

typedef struct _HexapodeClassRec *HexapodeClass;
typedef struct _HexapodeRec *Hexapode;

typedef struct  {
       long     value;
       char    *label;
} _LabelList;

typedef struct  {
       long           size;
       _LabelList    *list;
} LabelList;

extern HexapodeClass hexapodeClass;
// TODO
//extern Hexapode hexapode;

/*
 * Convenience definition.
 */
#define  NOT_OK                    0
#define  OK                        1

#ifndef PI
#define PI 3.141592653590
#endif

/*
 * Conversion Macros.
 */
#define FromMMtoM(a)  ((a) / 1000.)             /* mm to meters  conversion */
#define FromMtoMM(a)  (1000. * (a))             /* meters to mm  conversion */
#define FromDEGtoRAD(a)  ((a) * PI / 180.)    /* Deg to Radian conversion */
#define FromRADtoDEG(a)  ((a) * 180./ PI )    /* Radian to Deg conversion */

/*
 * Reset modes.
 */
#define  HARD_RESET         0
#define  SOFT_RESET         1

/*
 * Operation modes.
 */
#define  NORMAL_MODE         0
#define  SIMULATION_MODE     1

/*
 * Possible mechanical reference types
 */
#define NONE            0
#define ABSOLUTE        1
#define MIDPOINT        2
#define LIMIT           3

/*
 * Different topologies.
 */
#define  SUPPORT          0
#define  MANIPULATOR      1

/*
 * Different types of motor controllers.
 */
#define  NOMOTOR          0
#define  VPAP             1
#define  VPAP_CY545       2
#define  VPAP_CY550       3
#define  PHYSIKINSTRUMENT 4
#define  HYDRAULIC        5
#define  ICEPAP           6

/*
 * Possible Hexapode states.
 */
#define HXPD_READY      0
#define HXPD_MOVING     1
#define HXPD_FAULT      2

/*
 * Possible Actuator status.
 */
#define DEV_READY           0x01
#define DEV_MOVING          0x02
#define DEV_UP_LIMIT        0x04
#define DEV_DOWN_LIMIT      0x08
#define DEV_LIMIT           0x0C /* (DEV_UP_LIMIT || DEV_DOWN_LIMIT) */
#define DEV_HOME            0x10
#define DEV_SEARCHING       0x20
#define DEV_OTHER           0x40
#define DEV_FAULT           0x80
#define DEV_STOP            0x100  /* added for PI controller */

/*
 * Reference systems.
 */
#define  LRS              0
#define  IRS              1
#define  FRS              2
#define  MRS              3

/*
 * Coordinates.
 */
#define  x                0
#define  y                1
#define  z                2
#define  refs             3
#define  phi              3
#define  theta            4
#define  psi              5

/*
 * Miscelaneous.
 */
#define  MIN_VELOCITY     0.001              /* Minimum relative velocity */

/*
 * Lists.
 */
extern LabelList operationModeList;
extern LabelList mechRefList;
extern LabelList topologyList;
extern LabelList motorTypeList;
extern LabelList resetModeList;
extern LabelList hexapodeStateList;
extern LabelList actuatorStateList;
extern LabelList referenceSystemList;

/*
 * Utility functions.
 */
extern long  LabelToValue();
extern char *ValueToLabel();

#endif 

