/*static char RcsId[] = "$Header:" */ 
/*********************************************************************
 *
 * File:         DevHexapodeErrors.h
 *
 * Project:      Hexapode Device Server
 *
 * Description:  Error definitions for the  Hexapode Device Server
 *    
 * Author(s):    Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:     October 5 1993
 *
 * Copyright(c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 * $Log: DevHexapodeErrors.h,v $
 * Revision 2.3  2012/09/24 09:22:54  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:16  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:15  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.30  2006/05/22 14:50:45  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.22  2004/03/09 16:29:43  perez
 * Increase delay in ContinueHardReset()
 *
 * Revision 1.21  2004/01/15  16:41:03  16:41:03  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:42  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.1  1994/01/28  13:20:22  rey
 * Initial revision
 *
 * Revision 1.13  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.12  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.12  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.11  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.10  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.9  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.8  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.7  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.6  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.5  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.4  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.3  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.2  1993/10/26  07:53:43  rey
 * Version at 26 October
 *
 * Revision 1.1  1993/10/16  20:12:41  rey
 * Initial revision
 *
 * Revision 1.1  93/10/11  17:47:26  17:47:26  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 *
 *********************************************************************/

#ifndef _HEXAPODEERRORS_H
#define _HEXAPODEERRORS_H

#ifndef DevHexapodeBase
#include <ExpDsNumbers.h>                /* Expg dserver base numbers */
#endif

/*
 * General errors:
 */
#define DevErr_HxpdNoError                  DevHexapodeBase
#define DevErr_InternalBug                  DevHexapodeBase +  1
#define DevErr_WrongInternalCommand         DevHexapodeBase +  2
#define DevErr_FunctionNotImplemented       DevHexapodeBase +  3
#define DevErr_InvalidResourceString        DevHexapodeBase +  4
#define DevErr_DatabaseRead                 DevHexapodeBase +  5
#define DevErr_NonAcceptableCommand         DevHexapodeBase +  6
#define DevErr_NonCompatibleFunction        DevHexapodeBase +  7
#define DevErr_BadVelocity                  DevHexapodeBase +  8
#define DevErr_BadMovementResolution        DevHexapodeBase +  9
#define DevErr_WrongMovementMode            DevHexapodeBase + 10
#define DevErr_BadMovementLimits            DevHexapodeBase + 11
#define DevErr_WrongResetMode               DevHexapodeBase + 12
#define DevErr_FileFormat                   DevHexapodeBase + 13
#define DevErr_OpeningFile                  DevHexapodeBase + 14
#define DevErr_DeleteFile                   DevHexapodeBase + 15
#define DevErr_FileAlreadyExists            DevHexapodeBase + 16
#define DevErr_UncertaintyTooBig            DevHexapodeBase + 17


/*
 *  Geometry errors:
 */
#define DevErr_BadTopology                  DevHexapodeBase + 30
#define DevErr_ColinearJointPoints          DevHexapodeBase + 31
#define DevErr_InvalidReferenceSystem       DevHexapodeBase + 32
#define DevErr_ImposibleToCalculateLengths  DevHexapodeBase + 33
#define DevErr_ConvergencyProblem           DevHexapodeBase + 34
#define DevErr_SingularMatrix               DevHexapodeBase + 35
#define DevErr_VectorModulusTooSmall        DevHexapodeBase + 36
#define DevErr_VectorsNotOrthogonal         DevHexapodeBase + 37
#define DevErr_InvalidActuatorLengths       DevHexapodeBase + 38
#define DevErr_ExcesiveTiltAngle            DevHexapodeBase + 39
#define DevErr_PositionOutOfLimits          DevHexapodeBase + 40  
#define DevErr_NullMovement                 DevHexapodeBase + 41  
#define DevErr_RefSystemLocked              DevHexapodeBase + 42  


/*
 * Actuator generic errors:
 */
#define DevErr_ActuatorTypeNotImplemented   DevHexapodeBase + 60
#define DevErr_UnexpectedMovement           DevHexapodeBase + 61
#define DevErr_BadActuatorParameter         DevHexapodeBase + 62
#define DevErr_BadActuatorLength            DevHexapodeBase + 63
#define DevErr_HomeSwitchDisfunction        DevHexapodeBase + 64
#define DevErr_LimitSwitchDisfunction       DevHexapodeBase + 65
#define DevErr_LimitSwitchReached           DevHexapodeBase + 66
#define DevErr_WrongHomeType                DevHexapodeBase + 67
#define DevErr_BlindResetInterrupted        DevHexapodeBase + 68
#define DevErr_HardResetFailure             DevHexapodeBase + 69


/*
 * VPAP specific errors:
 */
#define DevErr_WrongVpapVersion             DevHexapodeBase + 100
#define DevErr_VpapAlarm                    DevHexapodeBase + 101
#define DevErr_OpenVpapDriver               DevHexapodeBase + 102
#define DevErr_WriteVpapDriver              DevHexapodeBase + 103
#define DevErr_ReadVpapDriver               DevHexapodeBase + 104
#define DevErr_StopVpapController           DevHexapodeBase + 105
#define DevErr_ResetVpapController          DevHexapodeBase + 106


/*
 * HYD specific errors:
 */
#define DevErr_QueueOpen                    DevHexapodeBase + 107
#define DevErr_QueueWrite                   DevHexapodeBase + 108
#define DevErr_QueueClose                   DevHexapodeBase + 109
#define DevErr_QueueRead                    DevHexapodeBase + 110

/*
 * PI specific errors:
 */
#define DevErr_OpenSerialDevice             DevHexapodeBase + 111
#define DevErr_ReadSerialDevice             DevHexapodeBase + 112
#define DevErr_HomeLengthError              DevHexapodeBase + 113
#define DevErr_NotHomed                     DevHexapodeBase + 114


/*
 * ICEPAP specific errors:
 */
#define DevErr_OpenIcepapDevice             DevHexapodeBase + 200
#define DevErr_PowerIcepap                  DevHexapodeBase + 201
#define DevErr_ReadIcepap                   DevHexapodeBase + 202
#define DevErr_WriteIcepap                  DevHexapodeBase + 203

#define DevErr_LastHxpdError                DevHexapodeBase + (1 << DS_IDENT_SHIFT) - 1

#endif 

