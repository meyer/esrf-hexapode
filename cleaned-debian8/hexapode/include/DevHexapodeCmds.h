/*static char RcsId[] = "$Header:" */ 
/*********************************************************************
 *
 * File:  DevHexapodeCmds.h
 *
 * Project:  Hexapode Device Server
 *
 * Description:  Command definitions for the  Hexapode Device Server
 *    
 * Author(s):  Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:  October 5 1993
 *
 * Copyright(c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 * $Log: DevHexapodeCmds.h,v $
 * Revision 2.3  2012/09/24 09:22:54  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:16  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:15  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.30  2006/05/22 14:50:45  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.22  2004/03/09 16:29:39  perez
 * Increase delay in ContinueHardReset()
 *
 * Revision 1.21  2004/01/15  16:41:48  16:41:48  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11  09:02:42  09:02:42  rey (Vicente Rey-Bakaikoa)
 * Version before merging of Linux and OS9
 * 
 * Revision 1.1  1994/01/28  13:20:22  rey
 * Initial revision
 *
 * Revision 1.13  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.12  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.12  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.11  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.10  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.9  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.8  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.7  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.6  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.5  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.4  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.3  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.2  1993/10/26  07:53:43  rey
 * Version at 26 October
 *
 * Revision 1.1  1993/10/16  20:12:41  rey
 * Initial revision
 *
 * Revision 1.1  93/10/11  17:47:24  17:47:24  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 *
 *********************************************************************/

#ifndef _HEXAPODECMDS_H
#define _HEXAPODECMDS_H

#ifndef DevHexapodeBase
#include <ExpDsNumbers.h>                /* Expg dserver base numbers */
#endif

/*
 *  General Commands
 */
#define  DevHxpdDescription             DevHexapodeBase + 10
#define  DevHxpdState                   DevHexapodeBase + 11
#define  DevHxpdStatus                  DevHexapodeBase + 12
#define  DevHxpdReset                   DevHexapodeBase + 13
#define  DevHxpdLastError               DevHexapodeBase + 14
#define  DevHxpdLastResetMode           DevHexapodeBase + 15
#define  DevHxpdActuatorStatus          DevHexapodeBase + 16
#define  DevHxpdGetMode                 DevHexapodeBase + 17
#define  DevHxpdSetMode                 DevHexapodeBase + 18
#define  DevHxpdGetVelocity             DevHexapodeBase + 19
#define  DevHxpdSetVelocity             DevHexapodeBase + 20
#define  DevHxpdMechResolution          DevHexapodeBase + 21
#define  DevHxpdSetDebug                DevHexapodeBase + 22
#define  DevHxpdSetLibDebug             DevHexapodeBase + 23

/*
 *  Alignment Commands
 */
#define  DevHxpdGetRefSystem            DevHexapodeBase + 50
#define  DevHxpdGetRefPosition          DevHexapodeBase + 51
#define  DevHxpdDefaultReference        DevHexapodeBase + 52
#define  DevHxpdSetRefSystem            DevHexapodeBase + 53
#define  DevHxpdSetRefAxis              DevHexapodeBase + 54
#define  DevHxpdSetRefOrigin            DevHexapodeBase + 55
#define  DevHxpdSetRefPosition          DevHexapodeBase + 56


/*
 *  Movement Commands
 */
#define  DevHxpdGetPosition             DevHexapodeBase + 100
#define  DevHxpdMove                    DevHexapodeBase + 101
#define  DevHxpdRotate                  DevHexapodeBase + 102
#define  DevHxpdTranslate               DevHexapodeBase + 103
#define  DevHxpdStop                    DevHexapodeBase + 104
#define  DevHxpdCheckPosition           DevHexapodeBase + 105


/*
 *  Calibration Commands
 */
#define  DevHxpdGetLengths              DevHexapodeBase + 150
#define  DevHxpdSetLengths              DevHexapodeBase + 151
#define  DevHxpdMoveActuators           DevHexapodeBase + 152
#define  DevHxpdGetResource             DevHexapodeBase + 153
#define  DevHxpdSetResource             DevHexapodeBase + 154
#define  DevHxpdGetMovResolution        DevHexapodeBase + 155
#define  DevHxpdSetMovResolution        DevHexapodeBase + 156
#define  DevHxpdSearchHome              DevHexapodeBase + 157
#define  DevHxpdNewHomeLengths          DevHexapodeBase + 158
#define  DevHxpdNominalPosition         DevHexapodeBase + 159

#endif 

