/*********************************************************************
 *
 * File:     SharedMemory.h
 *
 * Project:  Hexapito (Hexapode control program)
 *
 * Description: Header to Shared memory management code under linux
 *
 * Author(s):  Paolo Mangiagalli
 *
 * Original: February 26 1998
 *
 *
 * Version: $Revision: 2.3 $
 * by:      $Author: perez $
 * date:    $Date: 2012/09/24 09:22:54 $
 *
 *          $Log: SharedMemory.h,v $
 *          Revision 2.3  2012/09/24 09:22:54  perez
 *          Add support of MIDPOINT reference for ICEPAP
 *
 *          Revision 2.2  2011/05/23 07:59:16  perez
 *          Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 *          Revision 2.1  2011/03/28 09:04:33  perez
 *          Add ICEPAPvelocity resource
 *
 *          Revision 2.0  2010/09/06 07:44:15  perez
 *          Add ICEPAP motor support
 *
 *          Revision 1.37  2010/06/22 13:04:50  perez
 *          Fix bug of ch numbering + high debug
 *
 *          Revision 1.36  2010/06/17 06:53:01  perez
 *          Better fix bug of channels not as first six ones
 *
 *          Revision 1.35  2009/06/18 11:51:22  perez
 *          Fix bug of channels not as first six ones
 *
 *          Revision 1.30  2006/05/22 14:50:45  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.22  2004/03/09 16:29:38  perez
 *          Increase delay in ContinueHardReset()
 *
 * Revision 1.21  2004/01/15  16:41:15  16:41:15  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 *          Revision 1.0  1999/05/12 06:50:12  dserver
 *          Locked by PM
 *
 *
 *
 * (c) 1998 by European Synchrotron Radiation Facility,
 *                     Grenoble, France
 *
 *********************************************************************/

#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>

void modunlink();
void modremove();
char *modcreate();
char *modlink();


