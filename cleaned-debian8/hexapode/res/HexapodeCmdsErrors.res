#static char RcsId[] =
#$Header: /segfs/dserver/classes/hexapode/res/RCS/HexapodeCmdsErrors.res,v 1.21 2010/09/06 08:00:40 perez Exp $
#************************************************************************
#
# File:          HexapodeCmdsErrors.res
# 
# Project:       Hexapode Device Server
# 
# Description:   CLASS, CMDS and ERROR resources
# 
# Author(s):     V. Rey-Bakaikoa and P. Fajardo
# 
# Original:      October 1993
# 
# Copyright (c) 1993 by European Synchrotron Radiation Facility,
#               Grenoble,France 
# 
# $Log: HexapodeCmdsErrors.res,v $
# Revision 1.21  2010/09/06 08:00:40  perez
# Add ICEPAP specific errors
#
# Revision 1.20  2003/02/11 09:05:10  rey
# Version before merging of Linux and OS9
#
# Revision 1.1  1994/01/28  13:20:22  rey
# Initial revision
# 
#
#************************************************************************
#   ProgTeamNumber:  3
#   Hexapode DS Id:  1
#************************************************************************
#
# Commands.
# --------
#  General commands.
CMDS/3/1/10:     "DevHxpdDescription"
CMDS/3/1/11:     "DevHxpdState"
CMDS/3/1/12:     "DevHxpdStatus"
CMDS/3/1/13:     "DevHxpdReset"
CMDS/3/1/14:     "DevHxpdLastError"
CMDS/3/1/15:     "DevHxpdLastResetMode"
CMDS/3/1/16:     "DevHxpdActuatorStatus"
CMDS/3/1/17:     "DevHxpdGetMode"
CMDS/3/1/18:     "DevHxpdSetMode"
CMDS/3/1/19:     "DevHxpdGetVelocity"
CMDS/3/1/20:     "DevHxpdSetVelocity"
CMDS/3/1/21:     "DevHxpdMechResolution"
CMDS/3/1/22:     "DevHxpdSetDebug"
CMDS/3/1/23:     "DevHxpdSetLibDebug"
#
# Alignment Commands.
#
CMDS/3/1/50:     "DevHxpdGetRefSystem"
CMDS/3/1/51:     "DevHxpdGetRefPosition"
CMDS/3/1/52:     "DevHxpdDefaultReference"
CMDS/3/1/53:     "DevHxpdSetRefSystem"
CMDS/3/1/54:     "DevHxpdSetRefAxis"
CMDS/3/1/55:     "DevHxpdSetRefOrigin"
CMDS/3/1/56:     "DevHxpdSetRefPosition"
#
# Movement Commands.
#
CMDS/3/1/100:    "DevHxpdGetPosition"
CMDS/3/1/101:    "DevHxpdMove"
CMDS/3/1/102:    "DevHxpdRotate"
CMDS/3/1/103:    "DevHxpdTranslate"
CMDS/3/1/104:    "DevHxpdStop"
CMDS/3/1/105:    "DevHxpdCheckPosition"
#
# Calibration Commands.
#
CMDS/3/1/150:    "DevHxpdGetLengths"
CMDS/3/1/151:    "DevHxpdSetLengths"
CMDS/3/1/152:    "DevHxpdMoveActuators"
CMDS/3/1/153:    "DevHxpdGetResource"
CMDS/3/1/154:    "DevHxpdSetResource"
CMDS/3/1/155:    "DevHxpdGetMovResolution"
CMDS/3/1/156:    "DevHxpdSetMovResolution"
CMDS/3/1/157:    "DevHxpdSearchHome"
CMDS/3/1/158:    "DevHxpdNewHomeLengths"
CMDS/3/1/159:    "DevHxpdNominalPosition"
#
# Errors.
# ------
ERROR/3/1/0:      "No Error"
ERROR/3/1/1:      "Internal software error o misfunction"
ERROR/3/1/2:      "Wrong internal command"
ERROR/3/1/3:      "This function is not implemented"
ERROR/3/1/4:      "Invalid resource name for this device "
ERROR/3/1/5:      "Unable to read resource from database"
ERROR/3/1/6:      "Command not accepted in current state"
ERROR/3/1/7:      "Function not compatible with hardware"
ERROR/3/1/8:      "Bad velocity value"
ERROR/3/1/9:      "Bad movement resolution value"
ERROR/3/1/10:     "Wrong movement mode"
ERROR/3/1/11:     "Bad movement limits"
ERROR/3/1/12:     "Wrong reset mode"
ERROR/3/1/13:     "Wrong backup file format"
ERROR/3/1/14:     "Unable to open file"
ERROR/3/1/15:     "Unable to delete file"
ERROR/3/1/16:     "Trying to rewrite an existing file"
ERROR/3/1/17:     "Length uncertainty is too large"
#
ERROR/3/1/30:     "Bad topology"
ERROR/3/1/31:     "Colinear joint points"
ERROR/3/1/32:     "Invalid reference system"
ERROR/3/1/33:     "Unable to calculate lengths"
ERROR/3/1/34:     "Convergency problem"
ERROR/3/1/35:     "Singular matrix"
ERROR/3/1/36:     "Vector modulus too small"
ERROR/3/1/37:     "Vectors not orthogonal"
ERROR/3/1/38:     "Invalid actuator lengths"
ERROR/3/1/39:     "Excesive tilt angle"
ERROR/3/1/40:     "Position is out of limits"
ERROR/3/1/41:     "Null movement"
ERROR/3/1/42:     "Reference system is locked"
#
ERROR/3/1/60:     "Actuator type not implemented"
ERROR/3/1/61:     "Unexpected movement detected"
ERROR/3/1/62:     "Bad actuator parameter"
ERROR/3/1/63:     "Actuator length out of acceptable range"
ERROR/3/1/64:     "Home switch disfunction"
ERROR/3/1/65:     "Limit switch disfunction"
ERROR/3/1/66:     "Limit switch reached"
ERROR/3/1/67:     "Wrong mechanic reference type"
ERROR/3/1/68:     "Device stopped during blind reset"
ERROR/3/1/69:     "Misfunction while hardware reset"
#
ERROR/3/1/100:    "Wrong version of VPAP board"
ERROR/3/1/101:    "VPAP alarm set"
ERROR/3/1/102:    "Failure opening VPAP driver"
ERROR/3/1/103:    "Failure writting to VPAP driver"
ERROR/3/1/104:    "Failure reading from VPAP driver"
ERROR/3/1/105:    "Failure stopping VPAP controller"
ERROR/3/1/106:    "Failure resetting VPAP controller"
#
ERROR/3/1/200:    "ICEPAP: Unable to open connection to hostname"
ERROR/3/1/201:    "ICEPAP: Power can not be switched on"
ERROR/3/1/202:    "ICEPAP: Error reading from device"
ERROR/3/1/203:    "ICEPAP: Error writing to   device"
#
# Class.
# -----
