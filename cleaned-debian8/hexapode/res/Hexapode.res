# $Log: Hexapode.res,v $
# Revision 1.20  2003/02/11 09:05:10  rey
# Version before merging of Linux and OS9
#
# Revision 1.1  1994/01/28  13:20:22  rey
# Initial revision
#
#------------------------------------------------------------------------
#
# Resource file for Hexapode server
#
#------------------------------------------------------------------------
#------------------------------------------------------------------------
#				Devices list
#------------------------------------------------------------------------
Hexapode/test/device:	ID16/HEXAPODE/01\
                        ID16/HEXAPODE/02
#------------------------------------------------------------------------
#				Class resources
#------------------------------------------------------------------------
CLASS/Hexapode/test/FilePath:      /h0/DSERVER/HEXAP_DATA
#
#------------------------------------------------------------------------
#				Objects Resources
#           Device: ID16/HEXAPODE/01
#------------------------------------------------------------------------
ID16/HEXAPODE/01/Description: "Monochromator Support" 
#
#
# Fixed joint coordinates in the FRS. 
#                   (X axis points towards the source,
#                    Y axis points towards the back wall,
#                    Z axis is vertical.)
#
ID16/HEXAPODE/01/Fixed1X:    -353.55
ID16/HEXAPODE/01/Fixed1Y:     353.55
ID16/HEXAPODE/01/Fixed1Z:      45.00
#
ID16/HEXAPODE/01/Fixed2X:    -482.96
ID16/HEXAPODE/01/Fixed2Y:     129.41
ID16/HEXAPODE/01/Fixed2Z:      45.00
#
ID16/HEXAPODE/01/Fixed3X:    -129.41
ID16/HEXAPODE/01/Fixed3Y:    -482.96
ID16/HEXAPODE/01/Fixed3Z:      45.00
#
ID16/HEXAPODE/01/Fixed4X:     129.41  
ID16/HEXAPODE/01/Fixed4Y:    -482.96
ID16/HEXAPODE/01/Fixed4Z:      45.00
#
ID16/HEXAPODE/01/Fixed5X:     482.96
ID16/HEXAPODE/01/Fixed5Y:     129.41 
ID16/HEXAPODE/01/Fixed5Z:      45.00
#
ID16/HEXAPODE/01/Fixed6X:     353.55
ID16/HEXAPODE/01/Fixed6Y:     353.55
ID16/HEXAPODE/01/Fixed6Z:      45.00
#
# Moving joint coordinatesn the MRS.
#
ID16/HEXAPODE/01/Moving1X:    -103.53
ID16/HEXAPODE/01/Moving1Y:     386.37
ID16/HEXAPODE/01/Moving1Z:       0.00
#
ID16/HEXAPODE/01/Moving2X:    -386.37
ID16/HEXAPODE/01/Moving2Y:    -103.52
ID16/HEXAPODE/01/Moving2Z:       0.00
#
ID16/HEXAPODE/01/Moving3X:    -282.84
ID16/HEXAPODE/01/Moving3Y:    -282.84
ID16/HEXAPODE/01/Moving3Z:       0.00
#
ID16/HEXAPODE/01/Moving4X:     282.84
ID16/HEXAPODE/01/Moving4Y:    -282.84
ID16/HEXAPODE/01/Moving4Z:       0.00
#
ID16/HEXAPODE/01/Moving5X:     386.37
ID16/HEXAPODE/01/Moving5Y:    -103.52
ID16/HEXAPODE/01/Moving5Z:       0.00
#
ID16/HEXAPODE/01/Moving6X:     103.53
ID16/HEXAPODE/01/Moving6Y:     386.37
ID16/HEXAPODE/01/Moving6Z:       0.00
#
ID16/HEXAPODE/01/Topology:           SUPPORT
ID16/HEXAPODE/01/MechanicalRef:      LIMIT
ID16/HEXAPODE/01/LengthUncertainty:  0.1
#
# Bonelengths ( for Topology MANIPULATOR )
#
#ID16/HEXAPODE/01/LinkLength1:   0.00       
#ID16/HEXAPODE/01/LinkLength2:   0.00       
#ID16/HEXAPODE/01/LinkLength3:   0.00       
#ID16/HEXAPODE/01/LinkLength4:   0.00       
#ID16/HEXAPODE/01/LinkLength5:   0.00       
#ID16/HEXAPODE/01/LinkLength6:   0.00       
#
#  Geometrical limits.
#
ID16/HEXAPODE/01/MinActuatorLength:   923       
ID16/HEXAPODE/01/MaxActuatorLength:   941       
ID16/HEXAPODE/01/MaxTiltAngle:          5       
ID16/HEXAPODE/01/MaxIncrementX:        20       
ID16/HEXAPODE/01/MaxIncrementY:        20       
ID16/HEXAPODE/01/MaxIncrementZ:        20       
ID16/HEXAPODE/01/MaxIncrementPhi:       5       
ID16/HEXAPODE/01/MaxIncrementTheta:     5       
ID16/HEXAPODE/01/MaxIncrementPsi:       5       
#
ID16/HEXAPODE/01/MaxMovementResolution: 1
#
# Actuator lengths at the nominal position 
#
ID16/HEXAPODE/01/NominalLength1:   931
ID16/HEXAPODE/01/NominalLength2:   931
ID16/HEXAPODE/01/NominalLength3:   931
ID16/HEXAPODE/01/NominalLength4:   931
ID16/HEXAPODE/01/NominalLength5:   931
ID16/HEXAPODE/01/NominalLength6:   931
#
#  Default Reference System ( The origin is shifted to align the X axis
#                             with the incoming beam)
#
ID16/HEXAPODE/01/DefRefSystemX:     0.0       
ID16/HEXAPODE/01/DefRefSystemY:   115.0       
ID16/HEXAPODE/01/DefRefSystemZ:  1400.0       
ID16/HEXAPODE/01/DefRefSystemPhi:   0.0       
ID16/HEXAPODE/01/DefRefSystemTheta: 0.0       
ID16/HEXAPODE/01/DefRefSystemPsi:   0.0       
#
#  Reference System Lock Flag
#
ID16/HEXAPODE/01/ReferenceSystemLock:  False
#
#  Default Reference Position
#
ID16/HEXAPODE/01/DefRefPositionX:     0.       
ID16/HEXAPODE/01/DefRefPositionY:     -0.0032393507       
ID16/HEXAPODE/01/DefRefPositionZ:     941.1996410759       
ID16/HEXAPODE/01/DefRefPositionPhi:   0.       
ID16/HEXAPODE/01/DefRefPositionTheta: 0.       
ID16/HEXAPODE/01/DefRefPositionPsi:   0.       
#
# Actuator lengths at the reference switches 
#
ID16/HEXAPODE/01/HomeLength1:   922
ID16/HEXAPODE/01/HomeLength2:   922
ID16/HEXAPODE/01/HomeLength3:   922
ID16/HEXAPODE/01/HomeLength4:   922
ID16/HEXAPODE/01/HomeLength5:   922
ID16/HEXAPODE/01/HomeLength6:   922
#
# General parameters.
#
#
ID16/HEXAPODE/01/MovementMode:       SIMULATION
ID16/HEXAPODE/01/MotorType:          VPAP
ID16/HEXAPODE/01/Backlash:           0.1
#
# VPAP global parameters.
#
ID16/HEXAPODE/01/VPAP_StepsPerMM:      4800
ID16/HEXAPODE/01/VPAP_MaxSpeed:        2000
ID16/HEXAPODE/01/VPAP_InitialSpeed:     100
ID16/HEXAPODE/01/VPAP_HomeSearchSpeed:  500
ID16/HEXAPODE/01/VPAP_HomeHysteresis:   400
ID16/HEXAPODE/01/VPAP_SlopeRate:        200
#
# Unit and channel for each VPAP actuator.
#
ID16/HEXAPODE/01/VPAPmotor1Unit:    0
ID16/HEXAPODE/01/VPAPmotor1Chan:    2
ID16/HEXAPODE/01/VPAPmotor2Unit:    0
ID16/HEXAPODE/01/VPAPmotor2Chan:    1
ID16/HEXAPODE/01/VPAPmotor3Unit:    0
ID16/HEXAPODE/01/VPAPmotor3Chan:    6
ID16/HEXAPODE/01/VPAPmotor4Unit:    0
ID16/HEXAPODE/01/VPAPmotor4Chan:    5
ID16/HEXAPODE/01/VPAPmotor5Unit:    0
ID16/HEXAPODE/01/VPAPmotor5Chan:    4
ID16/HEXAPODE/01/VPAPmotor6Unit:    0
ID16/HEXAPODE/01/VPAPmotor6Chan:    3
#
#
#------------------------------------------------------------------------
#				Objects Resources
#           Device: ID16/HEXAPODE/02
#------------------------------------------------------------------------
ID16/HEXAPODE/02/Description: "Test Hexapode" 
#
#
# Fixed joint coordinates in the FRS. 
#                   (X axis points towards the source,
#                    Y axis points towards the back wall,
#                    Z axis is vertical.)
#
ID16/HEXAPODE/02/Fixed1X:    -353.55
ID16/HEXAPODE/02/Fixed1Y:     353.55
ID16/HEXAPODE/02/Fixed1Z:      45.00
#
ID16/HEXAPODE/02/Fixed2X:    -482.96
ID16/HEXAPODE/02/Fixed2Y:     129.41
ID16/HEXAPODE/02/Fixed2Z:      45.00
#
ID16/HEXAPODE/02/Fixed3X:    -129.41
ID16/HEXAPODE/02/Fixed3Y:    -482.96
ID16/HEXAPODE/02/Fixed3Z:      45.00
#
ID16/HEXAPODE/02/Fixed4X:     129.41  
ID16/HEXAPODE/02/Fixed4Y:    -482.96
ID16/HEXAPODE/02/Fixed4Z:      45.00
#
ID16/HEXAPODE/02/Fixed5X:     482.96
ID16/HEXAPODE/02/Fixed5Y:     129.41 
ID16/HEXAPODE/02/Fixed5Z:      45.00
#
ID16/HEXAPODE/02/Fixed6X:     353.55
ID16/HEXAPODE/02/Fixed6Y:     353.55
ID16/HEXAPODE/02/Fixed6Z:      45.00
#
# Moving joint coordinatesn the MRS.
#
ID16/HEXAPODE/02/Moving1X:    -103.53
ID16/HEXAPODE/02/Moving1Y:     386.37
ID16/HEXAPODE/02/Moving1Z:       0.00
#
ID16/HEXAPODE/02/Moving2X:    -386.37
ID16/HEXAPODE/02/Moving2Y:    -103.52
ID16/HEXAPODE/02/Moving2Z:       0.00
#
ID16/HEXAPODE/02/Moving3X:    -282.84
ID16/HEXAPODE/02/Moving3Y:    -282.84
ID16/HEXAPODE/02/Moving3Z:       0.00
#
ID16/HEXAPODE/02/Moving4X:     282.84
ID16/HEXAPODE/02/Moving4Y:    -282.84
ID16/HEXAPODE/02/Moving4Z:       0.00
#
ID16/HEXAPODE/02/Moving5X:     386.37
ID16/HEXAPODE/02/Moving5Y:    -103.52
ID16/HEXAPODE/02/Moving5Z:       0.00
#
ID16/HEXAPODE/02/Moving6X:     103.53
ID16/HEXAPODE/02/Moving6Y:     386.37
ID16/HEXAPODE/02/Moving6Z:       0.00
#
ID16/HEXAPODE/02/Topology:           SUPPORT
ID16/HEXAPODE/02/MechanicalRef:      LIMIT
ID16/HEXAPODE/02/LengthUncertainty:  0.1
#
# Bonelengths ( for Topology MANIPULATOR )
#
#ID16/HEXAPODE/02/LinkLength1:   0.00       
#ID16/HEXAPODE/02/LinkLength2:   0.00       
#ID16/HEXAPODE/02/LinkLength3:   0.00       
#ID16/HEXAPODE/02/LinkLength4:   0.00       
#ID16/HEXAPODE/02/LinkLength5:   0.00       
#ID16/HEXAPODE/02/LinkLength6:   0.00       
#
#  Geometrical limits.
#
ID16/HEXAPODE/02/MinActuatorLength:   923       
ID16/HEXAPODE/02/MaxActuatorLength:   941       
ID16/HEXAPODE/02/MaxTiltAngle:          5       
ID16/HEXAPODE/02/MaxIncrementX:        20       
ID16/HEXAPODE/02/MaxIncrementY:        20       
ID16/HEXAPODE/02/MaxIncrementZ:        20       
ID16/HEXAPODE/02/MaxIncrementPhi:       5       
ID16/HEXAPODE/02/MaxIncrementTheta:     5       
ID16/HEXAPODE/02/MaxIncrementPsi:       5       
#
ID16/HEXAPODE/02/MaxMovementResolution: 1
#
# Actuator lengths at the nominal position 
#
ID16/HEXAPODE/02/NominalLength1:   931
ID16/HEXAPODE/02/NominalLength2:   931
ID16/HEXAPODE/02/NominalLength3:   931
ID16/HEXAPODE/02/NominalLength4:   931
ID16/HEXAPODE/02/NominalLength5:   931
ID16/HEXAPODE/02/NominalLength6:   931
#
#  Default Reference System ( The origin is shifted to align the X axis
#                             with the incoming beam)
#
ID16/HEXAPODE/02/DefRefSystemX:     0.0       
ID16/HEXAPODE/02/DefRefSystemY:   115.0       
ID16/HEXAPODE/02/DefRefSystemZ:  1400.0       
ID16/HEXAPODE/02/DefRefSystemPhi:   0.0       
ID16/HEXAPODE/02/DefRefSystemTheta: 0.0       
ID16/HEXAPODE/02/DefRefSystemPsi:   0.0       
#
#  Reference System Lock Flag
#
ID16/HEXAPODE/02/ReferenceSystemLock:  False
#
#  Default Reference Position
#
ID16/HEXAPODE/02/DefRefPositionX:     0.       
ID16/HEXAPODE/02/DefRefPositionY:     -0.0032393507       
ID16/HEXAPODE/02/DefRefPositionZ:     941.1996410759       
ID16/HEXAPODE/02/DefRefPositionPhi:   0.       
ID16/HEXAPODE/02/DefRefPositionTheta: 0.       
ID16/HEXAPODE/02/DefRefPositionPsi:   0.       
#
# Actuator lengths at the reference switches 
#
ID16/HEXAPODE/02/HomeLength1:   922
ID16/HEXAPODE/02/HomeLength2:   922
ID16/HEXAPODE/02/HomeLength3:   922
ID16/HEXAPODE/02/HomeLength4:   922
ID16/HEXAPODE/02/HomeLength5:   922
ID16/HEXAPODE/02/HomeLength6:   922
#
# General parameters.
#
#
ID16/HEXAPODE/02/MovementMode:       SIMULATION
ID16/HEXAPODE/02/MotorType:          VPAP
ID16/HEXAPODE/02/Backlash:           0.1
#
# VPAP global parameters.
#
ID16/HEXAPODE/02/VPAP_StepsPerMM:      4800
ID16/HEXAPODE/02/VPAP_MaxSpeed:        2000
ID16/HEXAPODE/02/VPAP_InitialSpeed:     100
ID16/HEXAPODE/02/VPAP_HomeSearchSpeed:  500
ID16/HEXAPODE/02/VPAP_HomeHysteresis:   400
ID16/HEXAPODE/02/VPAP_SlopeRate:        200
#
# Unit and channel for each VPAP actuator.
#
ID16/HEXAPODE/02/VPAPmotor1Unit:    2
ID16/HEXAPODE/02/VPAPmotor1Chan:    1
ID16/HEXAPODE/02/VPAPmotor2Unit:    2
ID16/HEXAPODE/02/VPAPmotor2Chan:    2
ID16/HEXAPODE/02/VPAPmotor3Unit:    2
ID16/HEXAPODE/02/VPAPmotor3Chan:    3
ID16/HEXAPODE/02/VPAPmotor4Unit:    2
ID16/HEXAPODE/02/VPAPmotor4Chan:    4
ID16/HEXAPODE/02/VPAPmotor5Unit:    2
ID16/HEXAPODE/02/VPAPmotor5Chan:    5
ID16/HEXAPODE/02/VPAPmotor6Unit:    2
ID16/HEXAPODE/02/VPAPmotor6Chan:    8

# ICEPAP network hostname 
D23/hexapode/01/ICEPAPhostname:    iced231

# ICEPAP debug levels 
D23/hexapode/01/ICEPAPdebug:       1
D23/hexapode/01/ICEPAPlibdebug:    1

# ICEPAP leg resolution
# depends on icepapcms settings of DRIVER steps per turn
D23/hexapode/01/ICEPAPstepsPerMM:  58800

# ICEPAP motor addresses
D23/hexapode/01/ICEPAPmotor1Addr:  11
D23/hexapode/01/ICEPAPmotor2Addr:  12
D23/hexapode/01/ICEPAPmotor3Addr:  13
D23/hexapode/01/ICEPAPmotor4Addr:  14
D23/hexapode/01/ICEPAPmotor5Addr:  15
D23/hexapode/01/ICEPAPmotor6Addr:  16

