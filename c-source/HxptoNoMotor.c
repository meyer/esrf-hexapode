/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/src/RCS/HxptoNoMotor.c,v 2.3 2012/09/24 09:22:53 perez Rel $";*/
/*********************************************************************
 *
 * File:     HxptoSimul.c
 *
 * Project:  Hexapito (Hexapode control program)
 *
 * Description: Provide a function set for NOMOTOR (simulated motor)
 *
 * Author(s):   Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original: October 28 1993
 *
 * (c) 1993 by European Synchrotron Radiation Facility,
 *                     Grenoble, France
 *
 *********************************************************************/

#include <EsrfHexapito.h>

#define DEBUG_LVL1
#define DEBUG_LVL2
#define DEBUG_LVL3

#define SIMUL_RESOLUTION 0.1e-6    /* A lower value could be a source of
                                     convergency problems in the geometry
                                     routines. */

/*
 * Extern.
 */
extern SharedData   *SD;
extern ActuatorData *AD;
extern GeometryData *GD;

/*
 *  Declarations for NOMOTOR
 */
double  SimulLength[6];

long   SimulType;

long   SimulMotorInit();
long   SimulCheckMotors();
long   SimulSetMotors();
long   SimulMoveMotorsTo();
long   SimulStopMotors();
long   SimulSearchHomeSwitch();

void   SimulPrintMotorPrivate();

MotorDefinition SimulMotorDefinition = {
      SimulMotorInit,
      SimulCheckMotors,
      SimulSetMotors,
      SimulMoveMotorsTo,
      SimulStopMotors,
      SimulSearchHomeSwitch,
};

void SimulPrintMotorPrivate()
{
/*
   printf("No private data for type %s\n",
                                    ValueToLabel(SimulType, &motorTypeList));
*/
}

/**************************************************************
 *
 *
 *
 *
 *
 *
 **************************************************************/
long SimulMotorInit(data, parameters)
motorData          *data;
actuatorParameters *parameters;
{
   int i;

#ifdef DEBUG_LVL3
   printf("<ACTUATORS> in SimulMotorInit\n");
#endif

   SimulType = data->MotorType;
   SimulPrintMotorPrivate();

   for(i = 0; i < 6; i++)
      SimulLength[i] = 0;

   parameters->MotorCleverSearch = 1;

   parameters->MotorResolution = SIMUL_RESOLUTION;

   return(OK);
}

long SimulCheckMotors(global_status, status, length)
long  *global_status;
long   status[6];
double length[6];
{
   int i;

#ifdef DEBUG_LVL3
   printf("<ACTUATORS> in SimulCheckMotors\n");
#endif

   for (i = 0; i < 6; i++){
      status[i] = DEV_READY;
      length[i] = SimulLength[i];
   }
   *global_status = DEV_READY;
   return(OK);
}

long SimulSetMotors(Length)
double  Length[6];
{
   int i;

#ifdef DEBUG_LVL3
   printf("<ACTUATORS> in SimulSetMotors\n");
#endif

   for(i = 0; i < 6; i++)
      SimulLength[i] = Length[i];
   return(OK);
}

long SimulMoveMotorsTo(length, relspeed)
double  length[6];
float   relspeed[6];
{
   int i;

#ifdef DEBUG_LVL3
   printf("<ACTUATORS> in SimulMoveMotorsTo\n");
#endif

   for(i = 0; i < 6; i++)
      SimulLength[i] = length[i];
   return(OK);
}

long SimulStopMotors()
{
#ifdef DEBUG_LVL3
   printf("<ACTUATORS> in SimulStopMotors\n");
#endif

   return(OK);
}

long SimulSearchHomeSwitch(searching, type)
short  *searching;
short   type;
{
   int i;

#ifdef DEBUG_LVL3
   printf("<ACTUATORS> in SimulSearchHomeSwitch\n");
#endif

   if (!*searching)
      *searching = 1;
   else {
      *searching = 0;
      for (i = 0; i < 6; i++){
         SimulLength[i] = SD->ActuatorLength[i] = GD->NominalLength[i];
         AD->HomeLengthFound[i] = AD->HomeLength[i];
      }
   }
   return(OK);
}

