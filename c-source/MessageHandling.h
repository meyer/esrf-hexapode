
/*********************************************************************
 *
 * File:     MessageHandling.h
 *
 *
 * Project:  Hexapito (Hexapode control program)
 *
 * Description: Header to Shared memory management code under linux
 *
 * Author(s):  Paolo Mangiagalli
 *
 * Original: February 26 1998
 *
 *
 * Version: $Revision: 2.3 $
 * by:      $Author: perez $
 * date:    $Date: 2012/09/24 09:22:54 $
 *
 *          Log$
 *
 *
 * (c) 1998 by European Synchrotron Radiation Facility,
 *                     Grenoble, France
 *
 *********************************************************************/

#define NO_MESSAGE          0
#define STATUS_READ         1
#define POSITION_WRITE      2
#define STOP_MOTOR          3
#define QUIT                4
#define STATUS_WRITE        5
#define INJECTORS_CONTROL   7
#define LENGTH2DAC          8
#define ADC2LENGTH          9
#define REFERENCE_ADVALUE  10
#define REFERENCE          11
#define MESSAGE_ACK        12

#define INJECTORS_OFF   0
#define INJECTORS_ON    1

struct mymsgbuf {
  long     mtype;
  int      status[6];
  double   value[6];
  int      control;
};
