/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/src/RCS/Auxiliary.c,v 2.3 2012/09/24 09:22:53 perez Rel $";*/
/*********************************************************************
 *
 * File:         Auxiliary.h
 *
 * Project:      Hexapode Device Server
 *
 * Description:  Utility functions and lists definitions.
 *
 * Author(s):    Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:     March 11 1993
 *
 * $Log: Auxiliary.c,v $
 *
 *
 * Copyright(c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 *********************************************************************/
#ifndef Auxiliary_H
#define Auxiliary_H

#include <stdio.h>
#include <EsrfHexapode.h>

typedef struct  {
       long     value;
       char    *label;
} _LabelList;

typedef struct  {
       long           size;
       _LabelList    *list;
} LabelList;



/*
 * Function declarations.
 */
void    printPos(double L[6]);
void    printLengths(double Pos[6]);
char   *ValueToLabel(long vvalue, LabelList *vlist);
long    LabelToValue(char *vstring, LabelList *vlist);

#endif   /*	Auxiliary_H */
