/***************************************************************
#
#	File:	Hxptohyd.c
#
#	Project: Hexapode
#
#	Description: Communication routines for hydraulic hexapode
#
#	Author(s): Paolo Mangiagalli
#
#	Original:  07/1998
#
#
#	Copyright (c) year European Synchrotron Radiation Facility
#		Grenoble, France
#
 * Version: $Revision: 2.3 $
 * by:      $Author: perez $
 * date:    $Date: 2012/09/24 09:22:53 $
 *
 *          $Log: Hxptohyd.c,v $
 *          Revision 2.3  2012/09/24 09:22:53  perez
 *          Add support of MIDPOINT reference for ICEPAP
 *
 *          Revision 2.2  2011/05/23 07:59:15  perez
 *          Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 *          Revision 2.1  2011/03/28 09:04:33  perez
 *          Add ICEPAPvelocity resource
 *
 *          Revision 2.0  2010/09/06 07:44:12  perez
 *          Add ICEPAP motor support
 *
 *          Revision 1.37  2010/06/22 13:04:50  perez
 *          Fix bug of ch numbering + high debug
 *
 *          Revision 1.36  2010/06/17 06:53:01  perez
 *          Better fix bug of channels not as first six ones
 *
 *          Revision 1.35  2009/06/18 11:51:22  perez
 *          Fix bug of channels not as first six ones
 *
 *          Revision 1.34  2009/05/25 12:37:36  perez
 *          Fix bug in VPAP retries to avoid slowing down DS
 *
 *          Revision 1.31  2008/01/17 10:17:25  rey
 *          Fix bug with usleep
 *
 *          Revision 1.30  2006/05/22 14:50:43  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.29  2006/05/22 14:47:26  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.28  2006/05/22 14:41:40  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.27  2006/05/22 14:39:50  rey
 *          Bug when killing hexapode (hexapito not dying) solved
 *
 *          Revision 1.23  2006/03/16 14:16:04  rey
 *          Bug with CheckAll return missing solved
 *
 * Revision 1.22  2004/03/09  16:29:26  16:29:26  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:25  16:36:25  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 *          Revision 1.0  1999/05/12 06:47:48  dserver
 *          Locked by PM
 *
***************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <math.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <signal.h>

#include "EsrfHexapito.h"
#include "MessageHandling.h"




#define DEBUG_LVL1 1
#define DEBUG_LVL2 1
#define DEBUG_LVL3 1


#define OUT_OF_RANGE 0          /* last x out of range */
#define CONTROL_RANGE_1 1       /* last x in control range but everything else out */
#define CONTROL_RANGE_2 2       /* last x in stable range */
#define CONTROL_RANGE_3 3       /* average x in stable range */
#define STABLE_RANGE 5          /* average+sigma in stable range */

#define MSEC 1000 /* 1 msec = 1000 usec */
#define PROTOCOL_SLEEP 100*MSEC
#define FORK_SLEEP_SEC 2 
#define MAX_PROTOCOL_TRY 20 /* try at least MAX_PROTOCOL_TRY to read a message */


#define MOTOR_RESOLUTION 25e-6 

/***************************************************************
*
*    Function declaration
*
***************************************************************/


long HydMotorInit();
long HydCheckMotors();
long HydSetMotors();
long HydMoveMotorsTo();
long HydStopMotors();
long HydSearchHomeSwitch();


/***************************************************************
*
*    Variable declaration
*
***************************************************************/

extern GeometryData *GD;
extern SharedData *SD; 	/* in the real project this is an extern variable */
extern ActuatorData *AD;	/* in the real project this is an extern variable */
hydData     *HD;

int qid;                /* handle to the hxfeedback message queue */
int msgsz = sizeof(struct mymsgbuf) - sizeof(long); /* queue buffer size */
struct mymsgbuf qbuf;   /* queue buffer */


MotorDefinition HydDefinition = {
      HydMotorInit,
      HydCheckMotors,
      HydSetMotors,
      HydMoveMotorsTo,
      HydStopMotors,
      HydSearchHomeSwitch,
};

/***************************************************************
#
#	Function: HydMotorInit
#
#	Description: Init the hexapode motors
#		Called by MotorInit defined in Actuators.c
#
#	Arg(s) In:  
#		    AD->MotorData structure
#		    Actuators.c global actuators parameter stucture		
#
#	Arg(s) Out: Set Actuators.c global actuators parameter structure
#
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/

long HydMotorInit(data,parameters)
motorData *data;
actuatorParameters *parameters;
{
  pid_t childpid;
  key_t key;
  char mod_key_arg[64];
  int i,j;

#ifdef DEBUG_LVL1
  printf("<HXPTOHYD> MotorInit \n");
#endif

  HD = &data->MotorPrivate.hyd;

  if ( (childpid = fork()) == -1) {
    SD->Error = DevErr_DeviceOpen;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Cannot Fork process %s \n",HD->feedback_process_name);
#endif
    return(NOT_OK);
  }
  
  /* Create unique key via call to ftok() */
  key = ftok(".", 'm');
  
  /* Open the queue - create if necessary */
  if((qid = msgget(key, IPC_CREAT|0660)) == -1) {
    SD->Error = DevErr_QueueOpen;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Cannot open queue  %d \n",key);
#endif
    return(NOT_OK);     
  }

  if(childpid == 0) {  /* client side */
    sprintf(mod_key_arg,"%d",key); /* put the message queue key in argv[1] */
    execlp(HD->feedback_process_name,HD->feedback_process_name,mod_key_arg,(char *)0);
  }

  sleep(FORK_SLEEP_SEC);  /* wait that hxfeedback is spawned */

  qbuf.mtype = STATUS_READ;
  if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0 ) == -1 ) {
    SD->Error = DevErr_QueueWrite;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> msgsnd error \n");
#endif
    return(NOT_OK);
  }
  for(i=0;i<MAX_PROTOCOL_TRY;i++) {    
    if ( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , STATUS_WRITE, IPC_NOWAIT) != -1 ) {
      for(j=0;j<6;j++) {
	printf("\t<HXPTOHYD> retrieved status[%i] = %i\n",j+1,qbuf.status[j]);
	printf("\t<HXPTOHYD> retrieved position[%i] = %f\n",j+1,qbuf.value[j]);
	SD->ActuatorLength[j] = qbuf.value[j];
      }
      break;
    }
    else {
      printf("\t<HXPTOHYD> Try to retrieve %i\n",i+1);
      usleep(PROTOCOL_SLEEP);
    }
  }
  if(i==MAX_PROTOCOL_TRY) { /* Hxfeedback didn't respond */
   SD->Error = DevErr_DeviceOpen;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Feedback process doesn't respond  %d \n");
#endif
    kill(childpid,SIGINT);    
    if ( msgctl(qid, IPC_RMID, 0) == -1) {
      SD->Error = DevErr_QueueClose;
#ifdef DEBUG_LVL1
      printf("<HXPTOHYD> Close queue error \n");
#endif
      return(NOT_OK);
    }
    return(NOT_OK);
  }

  HD->feedback_pid = childpid;


  qbuf.mtype = POSITION_WRITE;  /* set as new position the last retrieved */
  if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0 ) == -1 ) {
    SD->Error = DevErr_QueueWrite;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> msgsnd error \n");
#endif
    return(NOT_OK);
  }

  for(i=0;i<MAX_PROTOCOL_TRY;i++) {    
    if ( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , MESSAGE_ACK, IPC_NOWAIT) != -1 ) 
      break;
    else
      usleep(PROTOCOL_SLEEP);
  }
  if ( i == MAX_PROTOCOL_TRY ) {      
    SD->Error = DevErr_QueueRead;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Hxfeedback didn't acknowledge \n");
#endif
    return(NOT_OK);
  }


  /* add code for changing calibration parameters */

  parameters->MotorCleverSearch = 0; /* No HomeSearch Capability */
  parameters->MotorResolution = MOTOR_RESOLUTION;

#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> End of Motor Init \n");
#endif
  return(OK);
}






/***************************************************************
#
#	Function: HydSearchHomeSwitch
#
#	Description: Search the home position for the hexapode
#		Called by SearchHomeSwitch defined in Actuators.c
#
#	Arg(s) In:  Hexapito SearchingHome flag
#		    	Search type = NORMAL_SEARCH | RESET_SEARCH
#		    
#
#	Arg(s) Out: Set Hexapito SearchingHomeFlag
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long HydSearchHomeSwitch(searching,type)
short *searching;
short type;
{

#ifdef DEBUG_LVL1
  printf("<HXPTOHYD> SearchHomeSwitch, why i'm here? \n");
#endif
	return(OK);
}


/***************************************************************
#
#	Function: HydCheckMotors
#
#	Description: Read leg status and leg position
#		Called by CheckMotors defined in Actuators.c
#
#	Arg(s) In:  Hexapito global_status
#		    SD->ActuatorsStatus[]
#		    SD->ActuatorsLenght[]		
#
#	Arg(s) Out: Set Hexapito global_status
#		    SD->ActuatorsStatus[]
#		    SD->ActuatorsLenght[]		
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long HydCheckMotors(global_status, status, length)
long *global_status;
long status[6];
double length[6];
{
    int i;

    qbuf.mtype = STATUS_READ;
    if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0 ) == -1 ) {
      SD->Error = DevErr_QueueWrite;
#ifdef DEBUG_LVL1
      printf("<HXPTOHYD> msgsnd error \n");
#endif
      return(NOT_OK);
    }
    for(i=0;i<MAX_PROTOCOL_TRY;i++) {    
      if ( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , STATUS_WRITE, IPC_NOWAIT) != -1 ) 
	break;
      else
	usleep(PROTOCOL_SLEEP);
    }
    if ( i == MAX_PROTOCOL_TRY ) {      
      SD->Error = DevErr_QueueRead;
#ifdef DEBUG_LVL1
      printf("<HXPTOHYD> Feedback process doesn't respond \n");
#endif
      return(NOT_OK);
    }
    for(i=0;i<6;i++) {
      if(qbuf.status[i] == OUT_OF_RANGE) /* if out of range, hxpd is moving */
	status[i] = DEV_MOVING;
      else if ( SD->State != HXPD_MOVING )/* if is in control range and hexapode READY -> not moving */ 
	status[i] = DEV_READY;
      else if ( qbuf.status[i] < CONTROL_RANGE_3 ) /* if hxpd is moving, wait for CONTROL_RANGE_3 */
	status[i] = DEV_MOVING;
      else
	status[i] = DEV_READY;
    }
    
    *global_status = 0;
    for(i=0;i<6;i++) { /* global status ORing of leg status */
      *global_status |= status[i];	
      length[i] = qbuf.value[i]; /* write lengths */
    }
    

    return(OK);
}	

	



/***************************************************************
#
#	Function: HydStopMotors
#
#	Description: Stop immediatly all motors
#		Called by StopActuators defined in Actuators.c
#
#	Arg(s) In:  
#
#	Arg(s) Out: 
#
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/

long HydStopMotors()

{
  int i;

#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Stop Motors \n");
#endif

  qbuf.mtype = INJECTORS_CONTROL;
  qbuf.control = INJECTORS_OFF;
  if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0 ) == -1 ) {
    SD->Error = DevErr_QueueWrite;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> msgsnd error \n");
#endif
    return(NOT_OK);
    }

  for(i=0;i<MAX_PROTOCOL_TRY;i++) {    
    if ( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , MESSAGE_ACK, IPC_NOWAIT) != -1 ) 
      break;
    else
      usleep(PROTOCOL_SLEEP);
  }
  if ( i == MAX_PROTOCOL_TRY ) {      
    SD->Error = DevErr_QueueRead;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Hxfeedback didn't acknowledge \n");
#endif
    return(NOT_OK);
  }

  return(OK);
}



/***************************************************************
#
#	Function: HydMoveMotorsTo
#
#	Description: Move the actuators to the selected lenghts
#
#	Arg(s) In: new length[] meter, speed
#
#	Arg(s) Out: -
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long HydMoveMotorsTo(length,relspeed)
double length[6];
float relspeed[6];
{
  int i;
#ifdef DEBUG_LVL1
  printf("<HXPTOHYD> Move motors to %.3f %.3f %.3f %.3f %.3f %.3f\n", 
	 length[0], length[1], length[2], \
	 length[3], length[4], length[5] );
#endif
  
  for (i=0;i<6;i++)
     qbuf.value[i]= length[i];

  qbuf.mtype = POSITION_WRITE; /* write new positions */
  if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0 ) == -1 ) {
    SD->Error = DevErr_QueueWrite;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> msgsnd error \n");
#endif
  }
  
  for(i=0;i<MAX_PROTOCOL_TRY;i++) {    
    if ( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , MESSAGE_ACK, IPC_NOWAIT) != -1 ) 
      break;
    else
      usleep(PROTOCOL_SLEEP);
  }
  if ( i == MAX_PROTOCOL_TRY ) {      
    SD->Error = DevErr_QueueRead;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Hxfeedback didn't acknowledge \n");
#endif
    return(NOT_OK);
  }  
  

  qbuf.mtype = INJECTORS_CONTROL; /* unblock injectors if previously blocked */
  qbuf.control = INJECTORS_ON;
  if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0 ) == -1 ) {
    SD->Error = DevErr_QueueWrite;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> msgsnd error \n");
#endif
    return(NOT_OK);
  }


  for(i=0;i<MAX_PROTOCOL_TRY;i++) {    
    if ( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , MESSAGE_ACK, IPC_NOWAIT) != -1 ) 
      break;
    else
      usleep(PROTOCOL_SLEEP);
  }
  if ( i == MAX_PROTOCOL_TRY ) {      
    SD->Error = DevErr_QueueRead;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Hxfeedback didn't acknowledge \n");
#endif
    return(NOT_OK);
  }
  
  return(OK);
 
}


/***************************************************************
#
#	Function: HydSetMotors
#
#	Description: Set target to the selected lenghts,
#			 without moving motors
#
#	Arg(s) In: new length[] meter
#
#	Arg(s) Out: -
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long HydSetMotors(length)
double length[6];
{
  int i;
#ifdef DEBUG_LVL1
  printf("<HXPTOHYD> Set motors \n");
#endif
  
  qbuf.mtype = INJECTORS_CONTROL; /* block injectors */
  qbuf.control = INJECTORS_OFF;
  if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0 ) == -1 ) {
    SD->Error = DevErr_QueueWrite;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> msgsnd error \n");
#endif
    return(NOT_OK);
  }
     
  for(i=0;i<MAX_PROTOCOL_TRY;i++) {    
    if ( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , MESSAGE_ACK, IPC_NOWAIT) != -1 ) 
      break;
    else
      usleep(PROTOCOL_SLEEP);
  }
  if ( i == MAX_PROTOCOL_TRY ) {      
    SD->Error = DevErr_QueueRead;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Hxfeedback didn't acknowledge \n");
#endif
    return(NOT_OK);
  }

  for (i=0;i<6;i++)
     qbuf.value[i]= length[i];

  qbuf.control = POSITION_WRITE; /* write new positions */
  if (msgsnd(qid,(struct msgbuf *)&qbuf ,msgsz , 0 ) == -1 ) {
    SD->Error = DevErr_QueueWrite;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> msgsnd error \n");
#endif
  }


  for(i=0;i<MAX_PROTOCOL_TRY;i++) {    
    if ( msgrcv(qid,(struct msgbuf *)&qbuf ,msgsz , MESSAGE_ACK, IPC_NOWAIT) != -1 ) 
      break;
    else
      usleep(PROTOCOL_SLEEP);
  }
  if ( i == MAX_PROTOCOL_TRY ) {      
    SD->Error = DevErr_QueueRead;
#ifdef DEBUG_LVL1
    printf("<HXPTOHYD> Hxfeedback didn't acknowledge \n");
#endif
    return(NOT_OK);
  }

  return(OK);
}













