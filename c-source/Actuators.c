/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/src/RCS/Actuators.c,v 2.3 2012/09/24 09:22:53 perez Rel $";*/
/*********************************************************************
 *
 * File:     Actuators.c
 *
 * Project:  Hexapito (Hexapode control program)
 *
 * Description: Code to implement calls to the actuators. 
 *
 * Author(s):   Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original: March 11 1993
 *
 * $Log: Actuators.c,v $
 * Revision 2.3  2012/09/24 09:22:53  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:15  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:12  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.34  2009/05/25 12:37:36  perez
 * Fix bug in VPAP retries to avoid slowing down DS
 *
 * Revision 1.31  2008/01/17 10:17:25  rey
 * Fix bug with usleep
 *
 * Revision 1.30  2006/05/22 14:50:43  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.29  2006/05/22 14:47:26  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.28  2006/05/22 14:41:40  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.27  2006/05/22 14:39:50  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.26  2006/05/22 14:37:55  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.25  2006/05/22 14:37:02  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.23  2006/03/16 14:11:24  rey
 * CheckAll no return. bug solved
 *
 * Revision 1.22  2004/03/09  16:29:28  16:29:28  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:29  16:36:29  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:12  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.13  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.12  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.11  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.10  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.9  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.8  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.7  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.6  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.5  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.2  1993/10/16  20:12:41  rey
 * A domingo 16
 *
 * Revision 1.1  93/10/11  17:46:55  17:46:55  rey (Vincente Rey-Bakaikoa)
 * Initial revision
 * 
 * Revision 1.2  1993/10/03  14:48:41  rey
 * Antes de empezar a marranear
 *
 * Revision 1.1  1993/09/30  12:59:20  rey
 * Initial revision
 *
 *
 * (c) 1993 by European Synchrotron Radiation Facility,
 *                     Grenoble, France
 *
 *********************************************************************/

/*
 * Includes.
 */


#include <math.h>
#include <Actuators.h>

#define DEBUG_LVL1 
#define DEBUG_LVL2 
#define DEBUG_LVL3
/*
#define DEBUG_LVL1 
#define DEBUG_LVL2 
#define DEBUG_LVL3
 */

/*
 * Declarations.
 */
long CheckActuators();
long StopActuators();
long SetLengths();


MotorDefinition    *MT;
actuatorParameters  Ap;

/*
 * Extern. 
 */
extern ActuatorData *AD;
extern SharedData   *SD;
extern GeometryData *GD;


/*****************************************************************************
 *  Function:      CheckActuators()
 *
 *  Description:   Reads the individual states and positions of the
 *                 and calculates a global state.
 *
 *         In:    
 *
 *  Error code(s): 
 *
 *****************************************************************************/
long CheckActuators(status)
long *status;
{
   int  i;

#ifdef DEBUG_LVL1
     printf("<ACTUATORS> in CheckActuators\n");
#endif

   if (MT->CheckMotors(status, SD->ActuatorStatus, SD->ActuatorLength) != OK)
      return(NOT_OK);

   if (AD->MechanicalRef == ABSOLUTE){
      /*
       * Read absolute encoders and verify that they match the motor
       * readings within MechanicalResolution.
       *  (to be implemented)
       */
       SD->Error = DevErr_FunctionNotImplemented;
       *status = DEV_FAULT;
       return(NOT_OK);
   }

   /*
    * Decides the global status for the manipulator mechanics.
    *        if one actuator is DEV_FAULT ------> status = DEV_FAULT
    *   else if one actuator is DEV_LIMIT ------> status = DEV_LIMIT
    *   else if one actuator is DEV_MOVING
    *                        or DEV_SEARCHING --> status = DEV_MOVING
    *   else -----------------------------------> status = DEV_READY
    */

   if (*status & DEV_FAULT)
      *status = DEV_FAULT;
   else if (*status & DEV_LIMIT)
      *status = DEV_LIMIT;
   else if (*status & DEV_MOVING || *status & DEV_SEARCHING)
      *status = DEV_MOVING;
   else
      *status = DEV_READY;

   return(OK);
}


/*****************************************************************************
 *  Function:      UpdateActuatorLengths()
 *
 *  Description:   Load the reading from the absolute encoders in the
 *                 motor controllers. 
 *
 *         In:    
 *
 *  Error code(s): 
 *
 *****************************************************************************/
long UpdateActuatorLengths()
{
  int  i;

#ifdef DEBUG_LVL1
     printf("<ACTUATORS> in UpdateActuatorLengths\n");
#endif

  /*
   * If not absolute encoders or simulation mode -> error.
   */
   if (AD->MechanicalRef != ABSOLUTE) {
      SD->Error = DevErr_InternalBug;
      return(NOT_OK);
   }

   /*
    * Read absolute encoders and store lengths in SD->ActuatorLength
    *  (to be implemented)
    */

    SD->Error = DevErr_FunctionNotImplemented;
    return(NOT_OK);

   /*
    * Load motor positions
    */
   return(SetLengths(SD->ActuatorLength));
}

/*****************************************************************************
 *  Function:      SearchHomeSwitch()
 *
 *  Description:   
 *
 *         In:    
 *
 *  Error code(s):
 *
 *****************************************************************************/
long SearchHomeSwitch(searching, type)
short  *searching;
short   type;
{
   int  i;

#ifdef DEBUG_LVL3
     printf("<ACTUATORS> in SearchHomeSwitch\n");
#endif

   if (AD->MechanicalRef == NONE || AD->MechanicalRef == ABSOLUTE){
      SD->Error = DevErr_InternalBug;
      return(NOT_OK);
   } 

   return(MT->SearchHomeSwitch(searching, type));
}

/*****************************************************************************
 *  Function:      SetLengths()
 *
 *  Description:   Set the lengths of the actuators at a certain value.
 *
 *         In:     New values.
 *
 *  Error code(s): DevErr_DeviceWrite
 *
 *****************************************************************************/
long SetLengths(Length)
double  Length[6];
{
   int    i;

#ifdef DEBUG_LVL3
     printf("<ACTUATORS> in SetLengths\n");
#endif

   if(MT->SetMotors(Length) != OK)
       return(NOT_OK);

  /*
   * If absolute encoders set the encoder offsets.
   */
   if (AD->MechanicalRef == ABSOLUTE){
      /*
       *   To be implemented
       */ 
      SD->Error = DevErr_FunctionNotImplemented;
      return(NOT_OK);
   }

   return(OK);
}

/*****************************************************************************
 *  Function:      StopActuators()
 *
 *  Description:   Inmediately stops the movement of the six actuators.
 *
 *         In:     none.
 *
 *        Out:     none.
 *
 *  Error code(s): DevErr_CommandFailed
 *
 *****************************************************************************/
long StopActuators()
{
#ifdef DEBUG_LVL3
     printf("<ACTUATORS> in StopActuators\n");
#endif

   return(MT->StopMotors());
}

/*****************************************************************************
 *  Function:      MoveActuatorsTo()
 *
 *  Description:   Move the legs to a certain set of positions.
 *
 *         In:     New lengths in meters.
 *
 *        Out:     none.
 *
 *  Error code(s): DevErr_DeviceIllegalParameter, DevErr_DeviceWrite
 *
 *****************************************************************************/
long MoveActuatorsTo(length)
double  length[6];
{
   float speed[6];
   float increment;
   float max_increment = 0;
   int   i;

#ifdef DEBUG_LVL3
     printf("<ACTUATORS> in MoveActuatorsTo\n");
#endif

  /*
   * Calculate relative velocities.
   */
   for(i = 0; i < 6; i++){
      speed[i] = increment = fabs(length[i] - SD->ActuatorLength[i]);
      if (increment > max_increment)
         max_increment = increment;
   }

   for(i = 0; i < 6; i++)
      if (max_increment != 0.0 )
         speed[i] *=  AD->Velocity / max_increment;

  /*
   * Move motors.
   */
   if (MT->MoveMotorsTo(length, speed) != OK){
      StopActuators();
      return(NOT_OK);
   } else
      return(OK);
}

/*****************************************************************************
 *  Function:      ActuatorsInit()
 *
 *  Description:   Initializes actuator hardware.
 *
 *         In:     none.
 *
 *        Out:     none.
 *
 *  Error code(s): DevErr_DeviceOpen, DevErr_DeviceWrite
 *
 *****************************************************************************/
long ActuatorsInit(mode, resolution)
long    mode;
double *resolution;
{
   int  i;
   int  motor_type;

#ifdef DEBUG_LVL3
     printf("<ACTUATORS> in ActuatorsInit\n");
#endif

   if (mode == SIMULATION_MODE )
		motor_type = NOMOTOR;
   else
		motor_type = AD->MotorData.MotorType;

   MT = NULL;
   for (i = 0; i < MotorListSize; i++)
      if (MotorList[i].MotorType == motor_type){
         MT = MotorList[i].Definition;
         break;
      }
    
   if (MT == NULL){
      SD->Error = DevErr_ActuatorTypeNotImplemented;
      return(NOT_OK);
   }

/*
#ifdef DEBUG_LVL1
   printf("<ACTUATORS> Motor Data, Type: %s\n", 
                ValueToLabel(AD->MotorData.MotorType, &motorTypeList));
#endif
*/

   if (MT->MotorInit(&AD->MotorData, &Ap) != OK)
       return(NOT_OK);
    
   if (AD->MechanicalRef == ABSOLUTE){
      /*
       * Initialises the encoder hardware.
       *  (to be implemented)
       */
       SD->Error = DevErr_FunctionNotImplemented;
       return(NOT_OK);

      if (Ap.MotorResolution > Ap.EncoderResolution)
          Ap.MechanicalResolution = Ap.MotorResolution;
      else
          Ap.MechanicalResolution = Ap.EncoderResolution;

      AD->CleverHomeSearch = 1;

   }else {
      Ap.MechanicalResolution = Ap.MotorResolution;

      AD->CleverHomeSearch = Ap.MotorCleverSearch;
   }
    
   if (Ap.MechanicalResolution <= 0){
      SD->Error = DevErr_InternalBug;
      return(NOT_OK);
   }

   *resolution = Ap.MechanicalResolution;
#ifdef DEBUG_LVL3
   printf("<ACTUATORS> leaving ActuatorsInit\n");
#endif
   return(OK);
}
