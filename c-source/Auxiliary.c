/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/src/RCS/Auxiliary.c,v 2.3 2012/09/24 09:22:53 perez Rel $";*/
/*********************************************************************
 *
 * File:         Auxiliary.c
 *
 * Project:      Hexapode Device Server
 *
 * Description:  Utility functions and lists definitions.
 *
 * Author(s):    Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:     March 11 1993
 *
 * $Log: Auxiliary.c,v $
 * Revision 2.3  2012/09/24 09:22:53  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:15  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:12  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.34  2009/05/25 12:37:36  perez
 * Fix bug in VPAP retries to avoid slowing down DS
 *
 * Revision 1.31  2008/01/17 10:17:25  rey
 * Fix bug with usleep
 *
 * Revision 1.30  2006/05/22 14:50:43  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.29  2006/05/22 14:47:26  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.28  2006/05/22 14:41:40  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.27  2006/05/22 14:39:50  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.26  2006/05/22 14:37:55  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.25  2006/05/22 14:37:02  rey
 * Bug when killing hexapode (hexapito not dying) solved
 *
 * Revision 1.23  2006/03/16 14:11:24  rey
 * CheckAll no return. bug solved
 *
 * Revision 1.22  2004/03/09  16:29:24  16:29:24  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:14  16:36:14  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:12  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.15  1994/01/28  13:18:54  rey
 * What about this one?
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.14  1994/01/28  13:09:40  rey
 * this is the same as before
 *
 * Revision 1.13  1994/01/28  13:05:01  rey
 * Version at 28 Janvier 1994
 *
 * Revision 1.12  1994/01/28  13:00:12  rey
 * Release version. Problem with math. coprocesor detected. Compiled and corrected with new options
 *
 * Revision 1.11  1993/12/21  12:56:51  rey
 * First pre-release version
 *
 * Revision 1.10  1993/12/20  16:28:53  rey
 * I really hope that this will be the last one
 *
 * Revision 1.9  1993/12/20  16:24:57  rey
 * Encore
 *
 * Revision 1.8  1993/12/20  16:19:53  rey
 * Repetimos
 *
 * Revision 1.7  1993/12/20  16:15:22  rey
 * Probably first release version
 *
 * Revision 1.6  1993/11/09  17:37:15  rey
 * Espero que sea uno de los ultimos .
 *
 * Revision 1.5  1993/11/06  08:50:17  rey
 * Almost the firs release version
 *
 * Revision 1.4  1993/10/26  07:53:43  rey
 * Version at 26 October
 *
 * Revision 1.3  1993/10/26  07:51:36  rey
 * Version del 26 Octubre.
 *
 *
 * Copyright(c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 *********************************************************************/
#include <stdio.h>
#include <Auxiliary.h>

/*
 * Hexapode State List.
 */
_LabelList _hexapodeStateList[] =  
{
      {HXPD_READY,  "READY"},
      {HXPD_MOVING, "MOVING"},
      {HXPD_FAULT,  "FAULT"},
};

LabelList hexapodeStateList=
  {sizeof(_hexapodeStateList)/sizeof(_LabelList),_hexapodeStateList};

/*
 * Actuator State List.
 */
_LabelList _actuatorStateList[] =  
{
      {DEV_READY,      "READY"},
      {DEV_MOVING,     "MOVING"},
      {DEV_SEARCHING,  "SEARCHING"},
      {DEV_UP_LIMIT,   "UPPER LIMIT"},
      {DEV_DOWN_LIMIT, "LOWER LIMIT"},
      {DEV_LIMIT,      "LIMIT"},
      {DEV_HOME,       "HOME"},
      {DEV_FAULT,      "FAULT"},
};

LabelList actuatorStateList=
  {sizeof(_actuatorStateList)/sizeof(_LabelList),_actuatorStateList};

/*
 * Reference System List.
 */
_LabelList _referenceSystemList[] = 
{
      {LRS,   "LRS"},
      {IRS,   "IRS"},
      {FRS,   "FRS"},
      {MRS,   "MRS"},
};

LabelList referenceSystemList=
  {sizeof(_referenceSystemList)/sizeof(_LabelList),_referenceSystemList};

/*
 * Operation Mode List.
 */
_LabelList _operationModeList[] =
    {
        {NORMAL_MODE,     "NORMAL"},
        {SIMULATION_MODE, "SIMULATION"},
    };

LabelList operationModeList=
  {sizeof(_operationModeList)/sizeof(_LabelList),_operationModeList};


/*
 * Mechanical Reference List.
 */
_LabelList  _mechRefList[] = 
{
     {NONE,        "NONE"},
     {ABSOLUTE,    "ABSOLUTE"},
     {MIDPOINT,    "MIDPOINT"},
     {LIMIT,       "LIMIT"},
};

LabelList mechRefList=
  {sizeof(_mechRefList)/sizeof(_LabelList),_mechRefList};

/*
 * Topology List.
 */
_LabelList _topologyList[] = 
{
      {SUPPORT,     "SUPPORT"},
      {MANIPULATOR, "MANIPULATOR"},
};

LabelList topologyList=
  {sizeof(_topologyList)/sizeof(_LabelList),_topologyList};

/*
 * Actuator Type List.
 */
_LabelList _motorTypeList[] = 
{
      {NOMOTOR,          "NOMOTOR"},
      {VPAP,             "VPAP"},
      {VPAP_CY545,       "VPAP_545"},
      {VPAP_CY550,       "VPAP_550"},
      {PHYSIKINSTRUMENT, "PI"},
      {HYDRAULIC,        "HYDRAULIC"},
      {ICEPAP,           "ICEPAP"},
};

LabelList motorTypeList=
  {sizeof(_motorTypeList)/sizeof(_LabelList),_motorTypeList};

/*
 * Reset Mode List.
 */
_LabelList _resetModeList[] = 
{
      {HARD_RESET, "HARD"},
      {SOFT_RESET, "SOFT"},
};

LabelList resetModeList=
  {sizeof(_resetModeList)/sizeof(_LabelList),_resetModeList};




/*
 * PrintLengths:  prints a Length vector.
 ***********************************************/
void printLengths(double L[6])
	//double L[6];
{
	int	i;

	for (i =0; i< 6; i++)
		printf ("l(%d) = %12.8f\n", i, L[i]);
	printf ("\n");

}

/*
 * PrintPos:  prints a Position vector.
 ***********************************************/
void printPos(double Pos[6])
	//double Pos[6];
{
	printf ("x     = %12.8f\n", Pos[x]);
	printf ("y     = %12.8f\n", Pos[y]);
	printf ("z     = %12.8f\n", Pos[z]);
	printf ("phi   = %12.8f\n", Pos[phi]);
	printf ("theta = %12.8f\n", Pos[theta]);
	printf ("psi   = %12.8f\n", Pos[psi]);
	printf ("\n");
}


/************************************************************************
 *
 * Function   :  long ValueToLabel()
 * 
 * Description:  This routine 
 * 
 *        In  :  value and list
 * 
 *        Out :  none
 * 
 * Return(s)  :  corresponding string for that value in that list
 *               NULL if not found or other error.
 * 
 ***************************************************************************/
char *ValueToLabel(long vvalue, LabelList *vlist)
//long          vvalue;
//LabelList    *vlist;
{
    long i;

    for ( i = 0; i < vlist->size; i++ ) 
    {
        if (vvalue == vlist->list[i].value)
        return(vlist->list[i].label);
    }
    return((char *)NULL);
}


/************************************************************************
 *
 * Function   :  long LabelToValue()
 * 
 * Description:  This routine 
 * 
 *        In  :  string and list
 * 
 *        Out :  none
 * 
 * Return(s)  :  corresponding numeric value for that string in that list
 *               -1 if not found or other error.
 * 
 ***************************************************************************/
long LabelToValue(char *vstring, LabelList *vlist)
//char         *vstring;
//LabelList    *vlist;
{
    long   i, j;
    int    len;
    char  *cstring;

    len = strlen(vstring);

    for ( i = 0; i < vlist->size; i++ ) 
    {
        cstring = vlist->list[i].label;

        if (len != strlen(cstring))
             continue;

        for (j = 0; j < len; j++)
            if (toupper(vstring[j]) != toupper(cstring[j]))
                break;

        if (j == len)
             return(vlist->list[i].value);
    }
    return(-1);
}
