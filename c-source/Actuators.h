/*static char RcsId[] = "$Header: /segfs/dserver/classes/hexapode/include/RCS/Actuators.h,v 2.3 2012/09/24 09:22:54 perez Rel $ ";*/
/*********************************************************************
 *
 * File:  Actuators.h
 *
 * Project:  Hexapode Device Server
 *
 * Description:  header for the  Hexapode Device Server
 *    
 *
 * Author(s):  Vicente Rey Bakaikoa & Pablo Fajardo
 *
 * Original:  March 11 1993
 *
 * $Log: Actuators.h,v $
 * Revision 2.3  2012/09/24 09:22:54  perez
 * Add support of MIDPOINT reference for ICEPAP
 *
 * Revision 2.2  2011/05/23 07:59:16  perez
 * Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 * Revision 2.1  2011/03/28 09:04:33  perez
 * Add ICEPAPvelocity resource
 *
 * Revision 2.0  2010/09/06 07:44:15  perez
 * Add ICEPAP motor support
 *
 * Revision 1.37  2010/06/22 13:04:50  perez
 * Fix bug of ch numbering + high debug
 *
 * Revision 1.36  2010/06/17 06:53:01  perez
 * Better fix bug of channels not as first six ones
 *
 * Revision 1.35  2009/06/18 11:51:22  perez
 * Fix bug of channels not as first six ones
 *
 * Revision 1.30  2006/05/22 14:50:45  rey
 * Fixing RCS to all have same version number
 *
 * Revision 1.23  2006/05/22 14:49:53  rey
 * *** empty log message ***
 *
 * Revision 1.22  2004/03/09 16:29:35  perez
 * Increase delay in ContinueHardReset()
 *
 * Revision 1.21  2004/01/15  16:41:01  16:41:01  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 * Revision 1.20  2003/02/11 09:02:42  rey
 * Version before merging of Linux and OS9
 *
 * Revision 1.1  1994/01/28  13:20:22  rey
 * Initial revision
 *
 *
 * Copyright(c) 1993 by European Synchrotron Radiation Facility, 
 *                     Grenoble, France
 *
 *********************************************************************/
#include <EsrfHexapito.h>

extern MotorDefinition SimulMotorDefinition;
/* JM
 * extern MotorDefinition VpapDefinition;
 */
extern MotorDefinition PiDefinition;
extern MotorDefinition HydDefinition;
extern MotorDefinition IcepapDefinition;

struct _MotorList{
   long    MotorType;
   MotorDefinition *Definition;
} MotorList[] =
       {
           { NOMOTOR,           &SimulMotorDefinition},
#ifndef arm
/* JM
 *          { VPAP,              &VpapDefinition},
 *          { VPAP_CY545,        &VpapDefinition},
 *          { VPAP_CY550,        &VpapDefinition},
 */
           { ICEPAP,            &IcepapDefinition},
#ifdef linux
           { PHYSIKINSTRUMENT,  &PiDefinition},
           { HYDRAULIC,         &HydDefinition},
#endif
#endif
       };

int  MotorListSize = sizeof(MotorList)/sizeof(struct _MotorList);
