#ifndef EsrfHexapito_H
#define EsrfHexapitpo_H

#include <EsrfHexapode.h>
#include <EsrfHexapodeErrors.h>
#include <SharedMemory.h>



/*
 * Lists.
 */
/*
extern LabelList operationModeList;
extern LabelList mechRefList;
extern LabelList topologyList;
extern LabelList motorTypeList;
extern LabelList resetModeList;
extern LabelList hexapodeStateList;
extern LabelList actuatorStateList;
extern LabelList referenceSystemList;
*/

/*
 * Utility functions.
 */
extern long  LabelToValue();
extern char *ValueToLabel();


typedef struct {
   long       (*MotorInit)();
   long       (*CheckMotors)();
   long       (*SetMotors)();
   long       (*MoveMotorsTo)();
   long       (*StopMotors)();
   long       (*SearchHomeSwitch)();
} MotorDefinition;

#endif /* EsrfHexapitpo_H */
