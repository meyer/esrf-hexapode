
/***************************************************************
#
#	File:	Hxptopi.c
#
#	Project: Hxptopi
#
#	Description: Communication Routines for Pi Hexapode
#
#	Author(s): Paolo Mangiagalli
#
#	Original : 01/1998
#
#	Revision:
#
#	Copyright (c) year European Synchrotron Radiation Facility
#		Grenoble, France
#
 * Version: $Revision: 2.3 $
 * by:      $Author: perez $
 * date:    $Date: 2012/09/24 09:22:53 $
 *
 *          $Log: Hxptopi.c,v $
 *          Revision 2.3  2012/09/24 09:22:53  perez
 *          Add support of MIDPOINT reference for ICEPAP
 *
 *          Revision 2.2  2011/05/23 07:59:15  perez
 *          Implement DevHxpdSetDebug + Icepap grouped motion (workaround bug DSP fw 1.22)
 *
 *          Revision 2.1  2011/03/28 09:04:33  perez
 *          Add ICEPAPvelocity resource
 *
 *          Revision 2.0  2010/09/06 07:44:12  perez
 *          Add ICEPAP motor support
 *
 *          Revision 1.37  2010/06/22 13:04:50  perez
 *          Fix bug of ch numbering + high debug
 *
 *          Revision 1.36  2010/06/17 06:53:01  perez
 *          Better fix bug of channels not as first six ones
 *
 *          Revision 1.35  2009/06/18 11:51:22  perez
 *          Fix bug of channels not as first six ones
 *
 *          Revision 1.34  2009/05/25 12:37:36  perez
 *          Fix bug in VPAP retries to avoid slowing down DS
 *
 *          Revision 1.31  2008/01/17 10:17:25  rey
 *          Fix bug with usleep
 *
 *          Revision 1.30  2006/05/22 14:50:43  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.29  2006/05/22 14:47:26  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.28  2006/05/22 14:41:40  rey
 *          Fixing RCS to all have same version number
 *
 *          Revision 1.27  2006/05/22 14:39:50  rey
 *          Bug when killing hexapode (hexapito not dying) solved
 *
 *          Revision 1.23  2006/03/16 14:16:04  rey
 *          Bug with CheckAll return missing solved
 *
 * Revision 1.22  2004/03/09  16:29:25  16:29:25  perez (Manuel.Perez)
 * Increase delay in ContinueHardReset()
 * 
 * Revision 1.21  2004/01/15  16:36:16  16:36:16  rey (Vicente Rey-Bakaikoa)
 * Unified Linux/os9 version but Linux still unstable
 * 
 *          Revision 1.0  1999/05/12 06:47:48  dserver
 *          Locked by PM
 *
***************************************************************/
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <math.h>

#include <EsrfHexapito.h>


#define MSEC 1000 /* 1 msec = 1000 usec */
#define READ_SLEEP_1 250*MSEC  /* used by VE */
#define READ_SLEEP_2 100*MSEC  /* used by TP, TS */
#define READ_SLEEP_3 20*MSEC   /* used by LS, MR, SQ */

#define BOARD_123 0
#define BOARD_456 1
#define LIMIT_SWITCH_BACKSTEP 2000
#define MOTOR_RESOLUTION 5e-6     /*  DC Mike STEP_PER_METER  = 1.692047e7 */
#define BACKUP_LENGTH_ERROR 5e-5  /* max error accepted between actual read lengths and last saved ones */
#define HOME_TORQUE 127          /* Homing max. torque  0 < n < 127 */
#define NORMAL_TORQUE 127       /* Normal displacement torque      */
#define MAX_SPEED 8000          /* Max free running speed in encoder_counts/s */
#define MIN_SPEED 7000
#define MECHANICAL_TOLLERANCE 1E-3 
#define CONTROLLER_NAME " PI Hexapode Controller "


#define DEBUG_LVL1
#define DEBUG_LVL2
#define DEBUG_LVL3



/***************************************************************
*
*    Function declaration
*
***************************************************************/


long PiMotorInit();
long PiCheckMotors();
long PiSetMotors();
long PiMoveMotorsTo();
long PiStopMotors();
long PiSearchHomeSwitch();


long PiCommInit();
long PiReadVersion();
void PiBoardSelect();
void PiLimitSwitchSet();

/***************************************************************
*
*    Variable declaration
*
***************************************************************/

extern GeometryData *GD;
extern SharedData *SD; 	/* in the real project this is an extern variable */
extern ActuatorData *AD;	/* in the real project this is an extern variable */
piData     *PD;

long MotorType;     /* perhaps not useful, but ......                 */

int Homed;          /* Home flag */

MotorDefinition PiDefinition = {
      PiMotorInit,
      PiCheckMotors,
      PiSetMotors,
      PiMoveMotorsTo,
      PiStopMotors,
      PiSearchHomeSwitch,
};

/***************************************************************
#
#	Function: PiBoardSelect
#
 #	Description: Select the controller board
#
#	Arg(s) In: fd = device file descriptor
		   board = BOARD_123 | BOARD_456 	
#
#	Arg(s) Out: -
#
#	Return: -
#
#***************************************************************/


void PiBoardSelect(fd, board)
int fd;
int board;
{
    
    char select[2]={'\x81','\x82'}; 

	/* char 129 (\x81) select board 1 (128+1) 
	   char 130 (\x81) select board 2 (128+2) */
	
	write(fd,&select[board],1);	
	usleep(READ_SLEEP_3);
	tcflush(fd,TCIFLUSH);
}


/***************************************************************
#
#	Function: PiLimitSwitchSet
#
#	Description: Set the back step which occurs when a limit hit
#					is hitted.
#
#	Arg(s) In: fd = device file descriptor
#		   board = BOARD_123 | BOARD_456 	
#
#	Arg(s) Out: -
#
#	Return: -
#
#***************************************************************/


void PiLimitSwitchSet(fd)
int fd;
{
	char outbuf[256];
	PiBoardSelect(fd,BOARD_123);
	sprintf(outbuf,"LS%i\r\n",LIMIT_SWITCH_BACKSTEP);
	write(fd,outbuf,strlen(outbuf));
	usleep(READ_SLEEP_3);
	PiBoardSelect(fd,BOARD_456);
	sprintf(outbuf,"LS%i\r\n",LIMIT_SWITCH_BACKSTEP);
	write(fd,outbuf,strlen(outbuf));
	usleep(READ_SLEEP_3);		
}



/***************************************************************
#
#	Function: PiCommInit
#
#	Description: Open Serial Port Communication
#
#	Arg(s) In: PiData structure
#
#	Arg(s) Out: Set PD->fd PD->devicename, PD->unitname)
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/
long PiCommInit (data)
piData	*data;
{
	
	int fd;

	struct termios oldtio, newtio;
	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> PiCommInit on dev %s \n",data->devicename);
#endif	

	

	
	/* open device for r/w and without crtl-c management */
	fd = open(data->devicename, O_RDWR | O_NOCTTY );
	if(fd < 0) { 
		SD->Error = DevErr_OpenSerialDevice;
	        return(NOT_OK);
	}
	/* store the shared data */
	data->fd = fd;


	/* clear newtio structure */
	bzero((void *)&newtio,sizeof(newtio));
	
	/* set control flags
		BAUDRATE 9600 baud
		CS8 8 bit word, 1 stop, no parity
		CLOCAL local connection
		CREAD  enable reading
	*/ 
	newtio.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
	
	/* set input processing flags
		IGNPAR ignore bits with parity error 	
	*/
	newtio.c_iflag = IGNPAR;

	/* set output processing flag to raw output */
	newtio.c_oflag = 0;

	/* set local mode flags non-canonical 
	 */
	newtio.c_lflag = 0;

	/* initialise control characters nothing ! */
	newtio.c_cc[VINTR] = 0;
	newtio.c_cc[VQUIT] = 0;
	newtio.c_cc[VERASE] = 0;
	newtio.c_cc[VKILL] = 0;
	newtio.c_cc[VEOF] = 0;
	newtio.c_cc[VTIME] = 1; /* timeout value if MIN = 0  is VTIME * 0.1 s*/
	newtio.c_cc[VMIN] = 0;  /* don`t block if no characters at input */
	newtio.c_cc[VSWTC] = 0;
	newtio.c_cc[VSTART] = 0;
	newtio.c_cc[VSTOP] = 0;
	newtio.c_cc[VSUSP] = 0;
	newtio.c_cc[VEOL] = 0;
	newtio.c_cc[VREPRINT] = 0;
	newtio.c_cc[VDISCARD] = 0;
	newtio.c_cc[VWERASE] = 0;
	newtio.c_cc[VLNEXT] = 0;
	newtio.c_cc[VEOL2] = 0;
	
	/* write the new port parameters */
	tcflush(fd,TCIFLUSH);
	if(tcsetattr(fd, TCSANOW, &newtio)) { /* add error management */
		return(NOT_OK);
	}

	return(OK);
}
		

/***************************************************************
#
#	Function: PiReadVersion
#
#	Description: Read software version from controllers
#
#	Arg(s) In: PiData structure
#
#	Arg(s) Out: Set PiData.version[]
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/

long PiReadVersion(data)
piData *data;
{
	char outbuf[256],
	     inpbuf[256];
	int fd = data->fd,
		res;

	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> Read Version \n");
#endif

	/* select board 456 */
	PiBoardSelect(fd,BOARD_456);	

	/* ask for version */
	sprintf(outbuf,"VE\r\n");
	write(fd,outbuf,strlen(outbuf));
	
	usleep(READ_SLEEP_1);
	
	/* read board 456 answer */
	res = read(fd,&inpbuf[0],255);
	inpbuf[res]=0;
	if (!res) { 
		SD->Error = DevErr_ReadSerialDevice;
	        return(NOT_OK);
	}	
	sprintf(&data->version[BOARD_456][0],&inpbuf[0]);

	/* select board 123 */
	PiBoardSelect(fd,BOARD_123);	

	/* ask for version */
	sprintf(outbuf,"VE\r\n");
	write(fd,outbuf,strlen(outbuf));
	
	usleep(READ_SLEEP_1);
	
	/* read board 123 answer */
	res = read(fd,&inpbuf[0],255);
	inpbuf[res]=0;
	if (!res) {
	        SD->Error = DevErr_ReadSerialDevice;
		return(NOT_OK);
	}	
	sprintf(data->version[BOARD_123],inpbuf);	
	return(OK);
}


/***************************************************************
#
#	Function: PiReadState
#
#	Description: Read State for the six motors from controllers
#
#	Arg(s) In: PD->fd, SD->ActuatorStatus[]
#
#	Arg(s) Out: Set SD->ActuatorsStatus[]
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/

long PiReadState(fd,status)
int fd;
long status[6];
{
	char outbuf[256],
	     inpbuf[256];
	
	long temp_status;
	int 	res,
		i;

	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> ReadState\n");
#endif
    
	/* select board 123 */
	PiBoardSelect(fd,BOARD_123);	
	
	/* ask for state  */
	sprintf(outbuf,"TS\r\n");
	write(fd,outbuf,strlen(outbuf));
	
	usleep(READ_SLEEP_2);
	
	/* read PI answer */
	res = read(fd,inpbuf,255);
	if (!res) {
	        SD->Error = DevErr_ReadSerialDevice;
		return(NOT_OK);
	}	
	inpbuf[res]=0;
	for(i=0;i<3;i++)
		sscanf(&inpbuf[3+i*9],"%d",&status[i]); 

	/* select board 456 */
	PiBoardSelect(fd,BOARD_456);	
	
	/* ask for status  */
	sprintf(outbuf,"TS\r\n");
	write(fd,outbuf,strlen(outbuf));
	
	usleep(READ_SLEEP_2);
	
	/* read PI answer */
	res = read(fd,inpbuf,255);
	if (!res) { 
	        SD->Error = DevErr_ReadSerialDevice; 
		return(NOT_OK);
	}
	inpbuf[res]=0;
	for(i=3;i<6;i++)
		sscanf(&inpbuf[3+(i-3)*9],"%d",&status[i]); 	
	
	/* write status to Hex. Sh. Data Structure 
		bit 0 -> On target
		bit 1 -> Limit Hit
		bit 2 -> Motor Off if set Motor On if clear
		bit 3 -> PushBoard or Keyboard control if setted
		bit 4 -> unused
		bit 5 -> Slave
		bit 6 -> Find Edge mode if setted
		bit 7 -> WS: Set if motor stops more than 128 ms
	*/
	
	for(i=0;i<6;i++) {
		temp_status = status[i];
		status[i] = 0;

		if (temp_status & 2) status[i] |= DEV_LIMIT;
		if (temp_status & 4) status[i] |= DEV_STOP;	
		if (temp_status & 128) 
			 status[i] |= DEV_READY;
		else 
			 status[i] |= DEV_MOVING;
	}
	return(OK);
}


/***************************************************`************
#
#	Function: PiReadLenght
#
#	Description: read the encoder position and converts to meter
#
#	Arg(s) In: PD->fd PD->ActuatorLength[]
#
#	Arg(s) Out: Set PD->ActuatorLenght[]
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long PiReadLength(fd,length)
int fd;
double length[6];
{
	char outbuf[256],
	     inpbuf[256];
	int i,j,res;
	int ok_read;
    long position[6];


	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> ReadLength \n");
#endif
    j = 0;
    do{
      
      ok_read = 0;

      /* select board 123 */
      PiBoardSelect(fd,BOARD_123);	
      
      /* ask for position  */
      sprintf(outbuf,"TP\r\n");
      write(fd,outbuf,strlen(outbuf));
      
      usleep(READ_SLEEP_2);
      
      /* read PI answer */
      res = read(fd,inpbuf,255);
      if (!res) { 
	SD->Error = DevErr_ReadSerialDevice;
	return(NOT_OK);
      }	
      for(i=0;i<3;i++)
	sscanf(&inpbuf[3+16*i],"%d",&position[i]);
      
      /* select board 456 */
      PiBoardSelect(fd,BOARD_456);	
      
      /* ask for position  */
      sprintf(outbuf,"TP\r\n");
      write(fd,outbuf,strlen(outbuf));
      
      usleep(READ_SLEEP_2);
      
      /* read PI answer */
      res = read(fd,inpbuf,255);
      if (!res) { 
	SD->Error = DevErr_ReadSerialDevice;
	return(NOT_OK);
      }
	
      for(i=0;i<3;i++)
	sscanf(&inpbuf[3+16*i],"%d",&position[3+i]); 
      
      for(i=0;i<6;i++) {
	length[i] = position[i] / PD->StepsPerMeter + AD->HomeLengthFound[i];
	
	/*if the length is greater than the double of max length, 
	  or it is lower than half of the min length return ok_read = -1,
	  added for strange readings of 63 meters! */ 
	if ( ( j < 3 ) && ( ( length[i] > 2*GD->MaxActuatorLength )
	               || ( length[i] < GD->MinActuatorLength/2 )) ){
#ifdef DEBUG_LVL1 
	  printf("<HXPTOPI> ReadLength ****WARNING****  wrong leg %d length. Read Again\n",i);
#endif		
	  j++;
	  ok_read = -1;
	  break;
	}
	
	/* Insert of a mechanical tollerance on the length */
	if (length[i] > GD->MaxActuatorLength && 
	    length[i] < GD->MaxActuatorLength + MECHANICAL_TOLLERANCE){
#ifdef DEBUG_LVL1
	  printf("<HXPTOPI> ReadLength ****WARNING**** Leg %d out of length %f\n",i,length[i]);
#endif		
	  length[i]=GD->MaxActuatorLength;
	  continue;
	}
	
	if (length[i] < GD->MinActuatorLength && 
	    length[i] > GD->MinActuatorLength - MECHANICAL_TOLLERANCE){
#ifdef DEBUG_LVL1
	  printf("<HXPTOPI> ReadLength ****WARNING**** Leg %d out of length %f\n",i,length[i]);
#endif		
	  length[i]=GD->MinActuatorLength;
	}
      

      }
    }while(ok_read);

    return(OK);
}


/***************************************************************
#
#	Function: PiSearchHomeSwitch
#
#	Description: Search the home position for the hexapode
#		Called by SearchHomeSwitch defined in Actuators.c
#
#	Arg(s) In:  Hexapito SearchingHome flag
#		    	Search type = NORMAL_SEARCH | RESET_SEARCH
#		    
#
#	Arg(s) Out: Set Hexapito SearchingHomeFlag
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long PiSearchHomeSwitch(searching,type)
short *searching;
short type;
{
	static short phase;
	int i;
	char inpbuf[256],
		 outbuf[256];



	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> SearchHomeSwitch \n");
#endif

	if(!*searching) { /* start the search */
		phase = 0;
		*searching = 1;
		
		if (type != NORMAL_SEARCH && type != RESET_SEARCH) {
			SD->Error = DevErr_InternalBug;
			return(NOT_OK);
		}
		if (AD->MechanicalRef != LIMIT) {	
			SD->Error = DevErr_InternalBug;
			return(NOT_OK);
		}
	}
	switch(phase) {
		case 0: /* all is OK, start the search */
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> Start search \n");
#endif			
			PiBoardSelect(PD->fd,BOARD_123);   /* turn all motors on */
			sprintf(outbuf,"MN\r\n");
			write(PD->fd,outbuf,strlen(outbuf)); 
			usleep(READ_SLEEP_3);
                        sprintf(outbuf,"SQ%d\r\n",HOME_TORQUE);
			write(PD->fd,outbuf,strlen(outbuf)); 
			usleep(READ_SLEEP_3);				

			PiBoardSelect(PD->fd,BOARD_456);
			sprintf(outbuf,"MN\r\n");
			write(PD->fd,outbuf,strlen(outbuf)); 
			usleep(READ_SLEEP_3);
			sprintf(outbuf,"SQ%d\r\n",HOME_TORQUE);
			write(PD->fd,outbuf,strlen(outbuf)); 
			usleep(READ_SLEEP_3);		
			
			PiBoardSelect(PD->fd,BOARD_123);
			for(i=0;i<3;i++) {
				sprintf(outbuf,"%iMR900000\r\n",i+1);
				write(PD->fd,outbuf,strlen(outbuf)); 
				usleep(READ_SLEEP_3);
			}
			PiBoardSelect(PD->fd,BOARD_456);
			for(i=0;i<3;i++) {
				sprintf(outbuf,"%iMR900000\r\n",i+1);
				write(PD->fd,outbuf,strlen(outbuf)); 
				usleep(READ_SLEEP_3);
			}
			phase = 1;
			break;
		
		case 1: /* search has been started, if all legs hit the limit go to phase 2 */ 
        	phase = 2;
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> Search in progress..... \n");
#endif				
			for(i=0;i<6;i++) {
				if(!(SD->ActuatorStatus[i] & DEV_LIMIT)) {
					phase = 1;
				    break;
				}
			}
			break;	

		case 2: /* end the search */
			phase = 0;
			*searching = 0;
		       
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> Search ended \n");
#endif
                        /* define home and reset torque */
			PiBoardSelect(PD->fd,BOARD_123);
			sprintf(outbuf,"DH\r\n");
			write(PD->fd,outbuf,strlen(outbuf)); 
			usleep(READ_SLEEP_3);
			sprintf(outbuf,"SQ%d\r\n",NORMAL_TORQUE);
			write(PD->fd,outbuf,strlen(outbuf)); 
			usleep(READ_SLEEP_3);	


			
			PiBoardSelect(PD->fd,BOARD_456);
			sprintf(outbuf,"DH\r\n");
			write(PD->fd,outbuf,strlen(outbuf)); 
			usleep(READ_SLEEP_3);
			sprintf(outbuf,"SQ%d\r\n",NORMAL_TORQUE);
			write(PD->fd,outbuf,strlen(outbuf)); 
			usleep(READ_SLEEP_3);	
			
			for(i=0;i<6;i++) 
				AD->HomeLengthFound[i] = AD->HomeLength[i];
									
			/*********/
			PiReadLength(PD->fd,SD->ActuatorLength);
			/*********/
			
			Homed = 1;
			
			if (type==RESET_SEARCH); /* add action !!!! */
						
			break;

	}
	return(OK);
}


/***************************************************************
#
#	Function: PiCheckMotors
#
#	Description: Read leg status and leg position
#		Called by CheckMotors defined in Actuators.c
#
#	Arg(s) In:  Hexapito global_status
#		    SD->ActuatorsStatus[]
#		    SD->ActuatorsLenght[]		
#
#	Arg(s) Out: Set Hexapito global_status
#		    SD->ActuatorsStatus[]
#		    SD->ActuatorsLenght[]		
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long PiCheckMotors(global_status, status, length)
long *global_status;
long status[6];
double length[6];
{
	int fd=PD->fd;
	int i;

	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> CheckMotors \n");
#endif

        *global_status = 0;

	if ( PiReadState(fd,status) != OK) {
		*global_status = DEV_FAULT;
		return(NOT_OK);
	}
	
	for(i=0;i<6;i++) 
	  *global_status |= status[i];

	if ( PiReadLength(fd,length) != OK) {
		*global_status = DEV_FAULT;
		return(NOT_OK);
	}
	return(OK);
}	

	
/***************************************************************
#
#	Function: PiMotorInit
#
#	Description: Init the hexapode motors
#		Called by MotorInit defined in Actuators.c
#
#	Arg(s) In:  
#		    AD->MotorData structure
#		    Actuators.c global actuators parameter stucture		
#
#	Arg(s) Out: Set Actuators.c global actuators parameter structure
#
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/

long PiMotorInit(data,parameters)
motorData *data;
actuatorParameters *parameters;
{
        int i,j;
	double backup_lengths[6];
	double actual_lengths[6];
        long   encoder_count;
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> MotorInit \n");
#endif

	MotorType = data->MotorType;
	PD = &data->MotorPrivate.pi;
	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> PD->devicename = %s  \n",PD->devicename);
#endif
	
	if(PiCommInit(PD) != OK)
		return(NOT_OK);

	parameters->MotorCleverSearch = 1; /* HomeSearch Capability */

	if(PD->StepsPerMeter <= 0) {
		SD->Error = DevErr_BadActuatorParameter;
		return(NOT_OK);
	}
	parameters->MotorResolution = MOTOR_RESOLUTION; /* != from encoder resolution ! */

        
	/* read the last saved leg lenghts */

	if(ReadLengthFile(SD->BackupFile,backup_lengths) != OK) {
	  return(NOT_OK);
	}

        /* read positions from encoders assuming nominal home lengths */
        

        for(i=0;i<6;i++) 
	  AD->HomeLengthFound[i] = AD->HomeLength[i];

        if(PiReadLength(PD->fd,actual_lengths) != OK) {
	  return(NOT_OK);
	}
#ifdef DEBUG_LVL1
   for(i=0;i<6;i++) printf("Actual  Length[%d] = %f\n",i+1,actual_lengths[i]);
#endif	
       
   /* if actual and last saved reading differs unset Homed flag,
      this will cause an error in attempting to move Hexapode */
        
        Homed = 1;
        for(i=0;i<6;i++){
	  if (fabs( actual_lengths[i] - backup_lengths[i] ) >= BACKUP_LENGTH_ERROR ) {
	    Homed = 0;
	    break;
	  }
	}
	if (!Homed) { /* modify HomeLengthFound so that PiReadPosition give the backup position */
	  for(i=0;i<6;i++) {
	    encoder_count = (long) (( actual_lengths[i] - AD->HomeLength[i] ) * PD->StepsPerMeter);
	    AD->HomeLengthFound[i] = backup_lengths[i] - encoder_count / PD->StepsPerMeter;
	  }
	}
	return(OK);
}


/***************************************************************
#
#	Function: PiStopMotors
#
#	Description: Stop immediatly all motors
#		Called by StopActuators defined in Actuators.c
#
#	Arg(s) In:  
#
#	Arg(s) Out: 
#
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/

long PiStopMotors()

{
	
	char outbuf[256];


	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> StopMotors \n");
#endif

	PiBoardSelect(PD->fd,BOARD_123);
	sprintf(outbuf,"AB\r\n");
	write(PD->fd,outbuf,strlen(outbuf)); 
	usleep(READ_SLEEP_3);

	PiBoardSelect(PD->fd,BOARD_456);
	sprintf(outbuf,"AB\r\n");
	write(PD->fd,outbuf,strlen(outbuf)); 
	usleep(READ_SLEEP_3);

	return(OK);
}



/***************************************************************
#
#	Function: PiMoveMotorsTo
#
#	Description: Move the actuators to the selected lenghts
#
#	Arg(s) In: new length[] meter, speed
#
#	Arg(s) Out: -
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long PiMoveMotorsTo(length,relspeed)
double length[6];
float relspeed[6];
{
	char outbuf[256],
	     inpbuf[256];
	int i,res;
	
	long position;
	
	
#ifdef DEBUG_LVL1
    printf("<HXPTOPI> MoveMotorsTo \n");
#endif

        if(!Homed){
          SD->Error = DevErr_NotHomed;
	  return(NOT_OK);
	}

        for(i=0;i<6;i++) { 
	  if (AD->HomeLengthFound[i] <= 0){
	    SD->Error = DevErr_HomeLengthError;
			return(NOT_OK);
	  }
	}

	
	PiBoardSelect(PD->fd,BOARD_123);   /* turn all motors on */
	sprintf(outbuf,"MN\r\n");
	write(PD->fd,outbuf,strlen(outbuf)); 
	usleep(READ_SLEEP_3);
	
	PiBoardSelect(PD->fd,BOARD_456);
	sprintf(outbuf,"MN\r\n");
	write(PD->fd,outbuf,strlen(outbuf)); 
	usleep(READ_SLEEP_3);
	

    	/* select board 123 */ 
	PiBoardSelect(PD->fd,BOARD_123);
		
	for(i=0;i<3;i++) {
		position = (long) ((length[i] - AD->HomeLengthFound[i]) * PD->StepsPerMeter);
		
		sprintf(outbuf,"%iSV%d\r\n",i+1,(int)(MIN_SPEED + relspeed[i] * (MAX_SPEED - MIN_SPEED)));
		write(PD->fd,outbuf,strlen(outbuf)); 
	        usleep(READ_SLEEP_3);
		sprintf(outbuf,"%iMA%d\r\n",i+1,position);
		write(PD->fd,outbuf,strlen(outbuf)); 
		usleep(READ_SLEEP_3);
	}		
	/* select board 456 */
	PiBoardSelect(PD->fd,BOARD_456);

	for(i=3;i<6;i++) {		
		position = (long) ((length[i] - AD->HomeLengthFound[i]) * PD->StepsPerMeter);
		sprintf(outbuf,"%iSV%d\r\n",i-2,(int)(MIN_SPEED + relspeed[i] * (MAX_SPEED - MIN_SPEED)));
		write(PD->fd,outbuf,strlen(outbuf)); 
	        usleep(READ_SLEEP_3);
		sprintf(outbuf,"%iMA%d\r\n",i-2,position);
		write(PD->fd,outbuf,strlen(outbuf));
		usleep(READ_SLEEP_3);
	}
	return(OK);
}


/***************************************************************
#
#	Function: PiSetMotors
#
#	Description: Set target to the selected lenghts,
#			 without moving motors
#
#	Arg(s) In: new length[] meter
#
#	Arg(s) Out: -
#
#	Return: OK if succesful else NOT_OK
#
#***************************************************************/


long PiSetMotors(length)
double length[6];
{
	char outbuf[256],
	     inpbuf[256];
	int i,res;
	
	long position;

#ifdef DEBUG_LVL1
    printf("<HXPTOPI> SetMotorsTo\n");
#endif
	
	for(i=0;i<6;i++){ /* check if Home is corrctly setted */
	  if (AD->HomeLengthFound[i] <= 0) {
	    SD->Error = DevErr_HomeLengthError;
	    return(NOT_OK);
	  }
	}
	
	PiBoardSelect(PD->fd,BOARD_123);   /* turn all motors off */
	sprintf(outbuf,"MF\r\n");
	write(PD->fd,outbuf,strlen(outbuf)); 
	usleep(READ_SLEEP_3);
	PiBoardSelect(PD->fd,BOARD_456);
	sprintf(outbuf,"MF\r\n");
	write(PD->fd,outbuf,strlen(outbuf)); 
	usleep(READ_SLEEP_3);
	

    	/* select board 123 */ 
	PiBoardSelect(PD->fd,BOARD_123);
		
	for(i=0;i<3;i++) {
		position = (long) ((length[i] - AD->HomeLengthFound[i]) * PD->StepsPerMeter);
		sprintf(outbuf,"%iMA%d\r\n",i+1,position);
		write(PD->fd,outbuf,strlen(outbuf)); 
		usleep(READ_SLEEP_3);
	}		
	/* select board 456 */
	PiBoardSelect(PD->fd,BOARD_456);

	for(i=3;i<6;i++) {		
		position = (long) ((length[i] - AD->HomeLengthFound[i]) * PD->StepsPerMeter);
		sprintf(outbuf,"%iMA%d\r\n",i-2,position);
		write(PD->fd,outbuf,strlen(outbuf));
		usleep(READ_SLEEP_3);
	}
	return(OK);
}













