
#ifndef EsrfHexapode_H
#define EsrfHexapode_H

#include <signal.h>
#include <math.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libdeep.h>
#include <SharedMemory.h>



/*
 * Convenience definition.
 */
#define  NOT_OK                    0
#define  OK                        1

#ifndef PI
#define PI 3.141592653590
#endif

/*
 * Conversion Macros.
 */
#define FromMMtoM(a)  ((a) / 1000.)             /* mm to meters  conversion */
#define FromMtoMM(a)  (1000. * (a))             /* meters to mm  conversion */
#define FromDEGtoRAD(a)  ((a) * PI / 180.)    /* Deg to Radian conversion */
#define FromRADtoDEG(a)  ((a) * 180./ PI )    /* Radian to Deg conversion */

/*
 * Reset modes.
 */
#define  HARD_RESET         0
#define  SOFT_RESET         1

/*
 * Operation modes.
 */
#define  NORMAL_MODE         0
#define  SIMULATION_MODE     1

/*
 * Possible mechanical reference types
 */
#define NONE            0
#define ABSOLUTE        1
#define MIDPOINT        2
#define LIMIT           3

/*
 * Different topologies.
 */
#define  SUPPORT          0
#define  MANIPULATOR      1

/*
 * Different types of motor controllers.
 */
#define  NOMOTOR          0
#define  VPAP             1
#define  VPAP_CY545       2
#define  VPAP_CY550       3
#define  PHYSIKINSTRUMENT 4
#define  HYDRAULIC        5
#define  ICEPAP           6

/*
 * Possible Hexapode states.
 */
#define HXPD_READY      0
#define HXPD_MOVING     1
#define HXPD_FAULT      2

/*
 * Possible Actuator status.
 */
#define DEV_READY           0x01
#define DEV_MOVING          0x02
#define DEV_UP_LIMIT        0x04
#define DEV_DOWN_LIMIT      0x08
#define DEV_LIMIT           0x0C /* (DEV_UP_LIMIT || DEV_DOWN_LIMIT) */
#define DEV_HOME            0x10
#define DEV_SEARCHING       0x20
#define DEV_OTHER           0x40
#define DEV_FAULT           0x80
#define DEV_STOP            0x100  /* added for PI controller */

/*
 * Reference systems.
 */
#define  LRS              0
#define  IRS              1
#define  FRS              2
#define  MRS              3

/*
 * Coordinates.
 */
#define  x                0
#define  y                1
#define  z                2
#define  refs             3
#define  phi              3
#define  theta            4
#define  psi              5

/*
 * Miscelaneous.
 */
#define  MIN_VELOCITY     0.001              /* Minimum relative velocity */


/*
 * Hexapito Flag values.
 */
#define HEXAP_RUNNING       1992
#define HEXAP_NOT_RUNNING   1993

/*
#define COMMAND_REQUEST    SIGUSR1
#define COMMAND_ACCEPTED   SIGUSR2
#define COMMAND_EXECUTE    SIGXCPU
#define COMMAND_SUCCESFUL  SIGURG
#define COMMAND_ERROR      SIGUNUSED
#define COMMAND_COMPLETED  SIGXFSZ
#define COMMAND_END_ACKN   SIGVTALRM
#define NO_COMMAND         SIGPROF
*/
#define COMMAND_REQUEST    41
#define COMMAND_ACCEPTED   42
#define COMMAND_EXECUTE    43
#define COMMAND_SUCCESFUL  44
#define COMMAND_ERROR      45
#define COMMAND_COMPLETED  46
#define COMMAND_END_ACKN   47
#define NO_COMMAND         48


/*
 * Hexapito commands.
 */
#define DEV_GET_INFO          0
#define DEV_SEARCH_HOME       2
#define DEV_SET_REF_SYSTEM    3
#define DEV_SET_REF_POSITION  4
#define DEV_SET_REF_ORIGIN    5
#define DEV_SET_REF_AXIS      6
#define DEV_DEF_REFERENCE     7
#define DEV_CHECK_POSITION    8
#define DEV_MOVE_ACTUATORS    9
#define DEV_MOVE_TABLE       10 
#define DEV_ROTATE_TABLE     11
#define DEV_TRANSLATE_TABLE  12 
#define DEV_ABORT_MOVEMENT   13 
#define DEV_SET_ACT_LENGTH   14 
#define DEV_SET_MODE         15
#define DEV_SET_VELOCITY     16
#define DEV_SET_RESOLUTION   17
#define DEV_RESET            18 
#define DEV_QUIT             19

#define NORMAL_SEARCH        47
#define RESET_SEARCH        123




typedef struct
{
      long      MotorCleverSearch;
      double    MotorResolution;
      double    EncoderResolution;
      double    MechanicalResolution;
} actuatorParameters;

typedef struct 
{
      short         cratenum;
      struct 
      {
          short         unitnum;
          short         channel;
          int           filepath;
      }             motor[6];
      float         StepsPerMeter;
      float         MaxSpeed;
      float         InitialSpeed;
      float         HomeSearchSpeed;
      long          HomeHysteresis;
      short         SlopeRate;
} vpapData;

#define ICEPAP_HOSTNAME_MAX 50
typedef struct 
{
      char          hostname[ICEPAP_HOSTNAME_MAX];
      int           debug;
      int           libdebug;
      int           initialized;
      deephandle_t  dev;
      /*
      float         StepsPerMeter;
      */
      double        StepsPerMeter;
      int           Velocity;
      struct 
      {
          int         addr;
      }             motor[6];
} icepapData;


typedef struct              /* data structure for PI controller (PM) */
{
   char    version[2][256]; /* firmware version of the two controllers */
   char    devicename[25];  /* filepath for the pysical device */     
   int     fd;              /* file descriptor for communication */   
   float   StepsPerMeter;
} piData;

typedef struct              /* data structure for Hydraulic controller (PM) */
{
  char    feedback_process_name[64];
#ifdef linux
  pid_t   feedback_pid;
#endif
} hydData;

typedef struct 
{
      long          MotorType;
      union
      {
          vpapData       vpap;
          icepapData     icepap;
          piData         pi;
          hydData        hyd;
      }             MotorPrivate;
} motorData; 

typedef struct _GeometryData {
                                       /* Hexapode definition */
      long          Topology;
      double        FixedPoint[6][3];
      double        MovingPoint[6][3];
      double        LinkLength[6];
      double        MinActuatorLength;
      double        MaxActuatorLength;
      double        MinLength[6];
      double        MaxLength[6];
      double        MaxTiltAngle;
      double        MaxIncrement[6];
      double        NominalLength[6];
      double        NominalPosition[6];
                                       /* Default Reference System */  
      double        DefaultReferenceSystem[6];
      double        DefaultReferencePosition[6];
                                       /* Current Reference System */
      double        ReferenceSystem[6];
      double        ReferencePosition[6];
                                       /* Mechanical Resolution (m)*/ 
      double        MechResolution;
                                       /* Movement Resolution (< 1)*/ 
      double        MovementResolution;
      char          ReferenceSystemLock;
} GeometryData;


typedef struct _ActuatorData {
      float     Velocity;
      double    HomeLength[6];
      double    HomeLengthFound[6];
      long      MechanicalRef;
      double    Backlash;
      double    ActuatorBacklash;
      long      CleverHomeSearch;
      motorData MotorData; 
} ActuatorData;


#define MAX_FILENAME_LENGTH 160

typedef struct _SharedData 
{
                                    /* Position filename  */
      char           BackupFile[MAX_FILENAME_LENGTH];
      char           TmpBackupFile[MAX_FILENAME_LENGTH];
      char           Description[MAX_FILENAME_LENGTH];
      double		 LengthUncertainty;
                                    /* Dataport Variables */
      //unsigned short pid;
      //unsigned short serverpid;
      pid_t         pid;
      pid_t         serverpid;
      unsigned long  time;
                                    /* Geometry and Actuators Data */
      GeometryData   GData;
      ActuatorData   AData;
                                    /* Current Parameters */
      double         Position[6];
      double         ActuatorLength[6];
      long           ActuatorStatus[6];
                                    /* Absolute movement definitions */
      double         FinalPosition[6];
      double         FinalActuatorLength[6];
                                    /* Rotation and Translation definitions */
      double         RotationAngle;
      double         RotationOrigin[4];
      double         RotationAxis[4];
      double         TranslationVector[4];
                                    /* Reference Position */
      double         OldPosition[6];
      double         NewPosition[6];
                                    /* Reference System Orientation */
      double         NewRefSystem[6];
      double         NewReferenceOrigin[4];
      double         OldDirection[4];
      double         NewDirection[4];
      double         NewActuatorLength[6];
                                     /* Movement Mode */
      float          NewVelocity;
      long           NewMovementMode;
      long           ResetMode;
      double         NewMovementResolution;
                                     /* Movement Mode */
      long           MovementMode;
      long           LastResetMode;
      double         MaxMovementResolution;
                                     /* Interprocess  */        
      long           Command;
      long           State;
      long           Error;
      long           LastError;
} SharedData;


#endif   /*	EsrfHexapode_H */
